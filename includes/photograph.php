<?php
// If it's going to need the database, then it's 
// probably smart to require it before we start.
require_once(LIB_PATH.DS.'database.php');
require_once(LIB_PATH.DS.'loginInvestor.php');
//require_once(LIB_PATH.DS.'user.php');
//require_once('../includes/initialize.php');



class Photograph extends DatabaseObject {
	
	protected static $table_name="accounts";
	protected static $db_fields=array('id' , 'filename' , 'filenamethumb');//, 'type', 'size', 'caption');
	protected static $db_query_part = "id, filename, filenamethumb";

	public $id;
	public $filename;
	public $filenamethumb;
	
	//public $displayPath;
	private 	$temp_path;
	private 	$newName;
  	protected 	$upload_dir="images";
 	public 		$errors=array();
  
  	protected 	$upload_errors = array(
		// http://www.php.net/manual/en/features.file-upload.errors.php
	  UPLOAD_ERR_OK 	    => "No errors.",
	  UPLOAD_ERR_INI_SIZE  	=> "Larger than upload_max_filesize.",
	  UPLOAD_ERR_FORM_SIZE 	=> "Larger than form MAX_FILE_SIZE.",
	  UPLOAD_ERR_PARTIAL 	=> "Partial upload.",
	  UPLOAD_ERR_NO_FILE 	=> "No file.",
	  UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
	  UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
	  UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension."
	);

	// Pass in $_FILE(['uploaded_file']) as an argument
  public function attach_file($file) {
		// Perform error checking on the form parameters
		if(!$file || empty($file) || !is_array($file)) {
		  // error: nothing uploaded or wrong argument usage
		  $this->errors[] = "No file was uploaded.";
		  return false;
		} else if($file['error'] != 0) {
		  // error: report what PHP says went wrong
		  $this->errors[] = $this->upload_errors[$file['error']];
		  return false;
		} else {
		  // Set object attributes to the form parameters.
		  $this->temp_path  = $file['tmp_name'];
		  $this->filename   = basename($file['name']); 
		  // Don't worry about saving anything to the database yet.
		  return true;

		}
	}
  
	public function save() {
		// A new record won't have an id yet.
		if(isset($this->id)) {
		// Make sure there are no errors
		// Can't save if there are pre-existing errors
		if(!empty($this->errors)) { return false; }
		// Can't save without filename and temp location
		if(empty($this->filename) || empty($this->temp_path)) {
		    $this->errors[] = "The file location was not available.";
		    return false;
		}
		// Determine the target_path
		$loginInvestor = LoginInvestor::find_by_id($this->id);
	  	$target_path = SITE_ROOT .DS. 'public' .DS. $this->upload_dir .DS. $loginInvestor->username .DS. $this->filename;
	  	if (!file_exists(SITE_ROOT .DS. 'public' .DS. $this->upload_dir .DS.$loginInvestor->username )) {
	  	mkdir(SITE_ROOT .DS. 'public' .DS. $this->upload_dir .DS. $loginInvestor->username, 0777, true);
	  	}
		// Make sure a file doesn't already exist in the target location
	    if(file_exists($target_path)) {
	    $this->errors[] = "The file {$this->filename} already exists.";
	    return false;
	   	}
		// Attempt to move the file 
		if(move_uploaded_file($this->temp_path, $target_path)) {
	  	// Success
	  	$path_parts = pathinfo('..'.DS.$this->upload_dir.DS.$loginInvestor->username.DS.$this->filename, PATHINFO_EXTENSION);
		$newName = $loginInvestor->username."_".rand();
		rename('..'.DS.'..'.DS.$this->upload_dir.DS.$loginInvestor->username.DS.$this->filename,
			   '..'.DS.'..'.DS.$this->upload_dir.DS.$loginInvestor->username.DS.$newName.".".$path_parts);
		$this->filename = $newName.".".$path_parts;
		//resize the picture
		$src  = SITE_ROOT .DS. 'public' .DS. $this->upload_dir .DS. $loginInvestor->username .DS. $this->filename;
		if ($path_parts == "jpg") {
		$this->resize_jpg($path_parts, $src, $src, 400);
		} else if ($path_parts == "png") {
		$this->resize_png($path_parts, $src, $src, 400);
		}

		// Creation of thumb picture 
		$destThumb = SITE_ROOT .DS. 'public' .DS. $this->upload_dir .DS. $loginInvestor->username .DS. $newName . "_thumb". "." .$path_parts;

		if ($path_parts == "jpg") {
		$this->resize_jpg($path_parts, $src, $destThumb, 100);
		} else if ($path_parts == "png") {
		$this->resize_png($path_parts, $src, $destThumb, 100);
		}

		$this->filenamethumb = $newName . "_thumb". "." .$path_parts;
		// Save a corresponding entry to the database
		if($this->update()) {	 
		// We are done with temp_path, the file isn't there anymore
		unset($this->temp_path);
		



		return true;
		} 
		} else {
			// File was not moved.
	    $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
	    return false;
		}
		} else {
		$this->errors[] = "Account not found";
		return false;
		}
	}

    public function destroy() {
		// First remove the database entry
		if($this->delete()) {
		// then remove the file
		// Note that even though the database entry is gone, this object 
		// is still around (which lets us use $this->image_path()).
		$target_path 	   = SITE_ROOT.DS.'public'.DS.$this->image_path();
		$target_thumb_path = SITE_ROOT.DS.'public'.DS.$this->image_thumb_path();

		return unlink($target_path) ? (unlink($target_thumb_path) ? true : false) : false;
		} else {
		// database delete failed
		return false;
		}
	}

	public function image_path() {
	  $loginInvestor = LoginInvestor::find_by_id($this->id);
	  return $this->upload_dir.DS.$loginInvestor->username .DS. $this->filename;
	}

	public function image_thumb_path() {
	  $loginInvestor = LoginInvestor::find_by_id($this->id);
	  return $this->upload_dir.DS.$loginInvestor->username .DS. $this->filenamethumb;
	}
	
	// Common Database Methods
	public static function find_all() {
		return self::find_by_sql("SELECT id, filename FROM ".self::$table_name);
    }
  
    public static function find_by_id($id=0) {
	global $database;
    $result_array = self::find_by_sql("SELECT ".self::$db_query_part." FROM ".self::$table_name." WHERE id=".$database->escape_value($id)." LIMIT 1");
	return !empty($result_array) ? array_shift($result_array) : false;
    }
  
    public static function find_by_sql($sql="") {
    global $database;
    $result_set = $database->query($sql);
    $object_array = array();
    while ($row = $database->fetch_array($result_set)) {
      $object_array[] = self::instantiate($row);
    }
    return $object_array;
    }

	private static function instantiate($record) {
		// Could check that $record exists and is an array
    $object = new self;
		// Simple, long-form approach:
		// $object->id 				= $record['id'];
		// $object->username 	= $record['username'];
		// $object->password 	= $record['password'];
		// $object->first_name = $record['first_name'];
		// $object->last_name 	= $record['last_name'];
		
		// More dynamic, short-form approach:
		foreach($record as $attribute=>$value){
		  if($object->has_attribute($attribute)) {
		    $object->$attribute = $value;
		  }
		}
		return $object;
	}
	
	private function has_attribute($attribute) {
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $this->attributes());
	}

	protected function attributes() { 
		// return an array of attribute names and their values
	  $attributes = array();
	  foreach(self::$db_fields as $field) {
	    if(property_exists($this, $field)) {
	      $attributes[$field] = $this->$field;
	    }
	  }
	  return $attributes;
	}
	
	protected function sanitized_attributes() {
	  global $database;
	  $clean_attributes = array();
	  // sanitize the values before submitting
	  // Note: does not alter the actual value of each attribute
	  foreach($this->attributes() as $key => $value){
	    $clean_attributes[$key] = $database->escape_value($value);
	  }
	  return $clean_attributes;
	}
	
	// replaced with a custom save()
	// public function save() {
	//   // A new record won't have an id yet.
	//   return isset($this->id) ? $this->update() : $this->create();
	// }
	
	/*public function create() {
		global $database;
		// Don't forget your SQL syntax and good habits:
		// - INSERT INTO table (key, key) VALUES ('value', 'value')
		// - single-quotes around all values
		// - escape all values to prevent SQL injection
		$attributes = $this->sanitized_attributes();
	  $sql = "INSERT INTO ".self::$table_name." (";
		$sql .= join(", ", array_keys($attributes));
	  $sql .= ") VALUES ('";
		$sql .= join("', '", array_values($attributes));
		$sql .= "')";
	  if($database->query($sql)) {
	    $this->id = $database->insert_id();
	    return true;
	  } else {
	    return false;
	  }
	}*/

	public function update() {
	  global $database;
		// Don't forget your SQL syntax and good habits:
		// - UPDATE table SET key='value', key='value' WHERE condition
		// - single-quotes around all values
		// - escape all values to prevent SQL injection
		$attributes = $this->sanitized_attributes();
		$attribute_pairs = array();
		foreach($attributes as $key => $value) {
		  $attribute_pairs[] = "{$key}='{$value}'";
		}
		$sql = "UPDATE ".self::$table_name." SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE id=". $database->escape_value($this->id);
	  $database->query($sql);
	  return ($database->affected_rows() == 1) ? true : false;
	}

	public function delete() {
		global $database;
		// Don't forget your SQL syntax and good habits:
		// - DELETE FROM table WHERE condition LIMIT 1
		// - escape all values to prevent SQL injection
		// - use LIMIT 1
	  // $sql = "DELETE FROM ".self::$table_name;
	  // $sql .= " WHERE id=". $database->escape_value($this->id);
	  // $sql .= " LIMIT 1";
	  
	  $sql = "UPDATE ".self::$table_name." SET filename=''";
	  $sql .= " WHERE id=". $database->escape_value($this->id);
	  
	  $database->query($sql);
	  return ($database->affected_rows() == 1) ? true : false;
	
		// NB: After deleting, the instance of User still 
		// exists, even though the database entry does not.
		// This can be useful, as in:
		//   echo $user->first_name . " was deleted";
		// but, for example, we can't call $user->update() 
		// after calling $user->delete().
	}

	protected function resize_png($ext, $src, $dest, $desired_width) {
	/* read the source image */

	if ($ext = "png") {
	$source_image = imagecreatefrompng($src);	
	}

	$width  = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	imagepng($virtual_image, $dest);

	return true;
	}

	protected function resize_jpg($ext, $src, $dest, $desired_width) {
	/* read the source image */
	
	if ($ext = "jpg") {
	$source_image = imagecreatefromjpeg($src);	
	}

	$width  = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	imagejpeg($virtual_image, $dest);

	return true;
	}

	/*function resample($jpgFile, $thumbFile, $width, $orientation) {
    // Get new dimensions
    list($width_orig, $height_orig) = getimagesize($jpgFile);
    $height = (int) (($width / $width_orig) * $height_orig);
    // Resample
    $image_p = imagecreatetruecolor($width, $height);
    $image   = imagecreatefromjpeg($jpgFile);
    imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
    // Fix Orientation
    switch($orientation) {
        case 3:
            $image_p = imagerotate($image_p, 180, 0);
            break;
        case 6:
            $image_p = imagerotate($image_p, -90, 0);
            break;
        case 8:
            $image_p = imagerotate($image_p, 90, 0);
            break;
    }
    // Output
    imagejpeg($image_p, $thumbFile, 90);
}*/


}

?>