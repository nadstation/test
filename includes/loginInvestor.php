<?php
require_once(LIB_PATH.DS.'database.php');
require_once(LIB_PATH.DS.'currency.php');
require_once(LIB_PATH.DS.'lots.php');

class LoginInvestor extends DatabaseObject {
	
protected static $table_name="accounts";

protected static $db_fields = array('id','type', 'username', 'active','email', 'hashed_password' , 'hash');

protected static $db_query_part = "id,'type', username, active, email, hashed_password, hash";

public $id;
public $type;
public $username;
public $active;
public $email;
public $hashed_password;
public $hash;

public function defaultConfig($id){

$currency  = new Currency();
$currencies = $currency->find_all();

for ($i=0; $i < count($currencies) ; $i++) { 
$lot = Lots::find_by_account_id_and_currency_id($id, $currencies[$i]->id);
if (!$lot) {
$newLot = new Lots();
$newLot->account_id  = $id;
$newLot->currency_id = $currencies[$i]->id;
$newLot->value 	  = "0.01";
$newLot->save();
} else {
$lot->save();	
}
}

}


public static function authenticate($email="",$password=""){
	global $database;
	$email 	  = $database->escape_value($email);
	$password = $database->escape_value($password);
	
	$sql = "SELECT * FROM " .self::$table_name. " ";
	$sql .= "WHERE email = '{$email}' ";
	$sql .= "AND hashed_password = '".sha1($password)."' ";
	$sql .= "LIMIT 1";
	$result_array = self::find_by_sql($sql);
	return !empty($result_array) ? array_shift($result_array) : false ;
	
}

public static function authenticateAdmin($email="",$password=""){
	global $database;
	$email 	  = $database->escape_value($email);
	$password = $database->escape_value($password);
	
	$sql = "SELECT * FROM " .self::$table_name. " ";
	$sql .= "WHERE email = '{$email}' ";
	$sql .= "AND hashed_password = '".sha1($password)."' ";
	$sql .= "AND admin = 1 ";
	$sql .= "LIMIT 1";
	$result_array = self::find_by_sql($sql);
	return !empty($result_array) ? array_shift($result_array) : false ;
	
}

public static function find_all(){  //not wrong to do with instance but better doing it with static video 35
global $database;
$result_set = self::find_by_sql("SELECT " . self::$db_query_part . "FROM " . self::$table_name);
return $result_set;
}

public static function find_by_id($id=0){
global $database;
$result_array = self::find_by_sql("SELECT " . self::$db_query_part . " FROM ".self::$table_name." WHERE id = {$id} LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_by_email($email=""){
global $database;
$result_array = self::find_by_sql("SELECT " . self::$db_query_part . " FROM ".self::$table_name." WHERE email = '{$email}' LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_by_user($user=""){
global $database;
$result_array = self::find_by_sql("SELECT " .self::$db_query_part. " FROM ".self::$table_name." WHERE username = '{$user}' LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_by_sql($sql=""){
global $database;
$result_set = $database->query($sql);
$object_array = array();
while($row = $database->fetch_array($result_set)){
	$object_array[] = self::instantiate($row);
}
return $object_array;
}

public static function count_all() {
global $database;
$sql = "SELECT COUNT(*) FROM " .self::$table_name;
$result_set = $database->query($sql);
$row = $database->fetch_array($result_set);
return array_shift($row);
}



private static function instantiate($record) {

//could check if that $record exists and is an array
//Simple, long-form approach:
//an object should know how to build itself


 $object = new self;
// $object->id  		= $record['id'];
// $object->username 	= $record['username'];
// $object->password 	= $record['password'];
// $object->first_name = $record['first_name'];
// $object->last_name 	= $record['last_name'];

//More dynamic , short-form approach:

foreach ($record as $attribute=> $value){
if($object->has_attribute($attribute)){
	$object->$attribute = $value;
}
}

return $object;

}

private function has_attribute($attribute){
	//get_object_vars returns an associative array withh all attributes
	//(incl. private ones!) as the keys and their current values as the value
	$object_vars = get_object_vars($this);
	
	return array_key_exists($attribute, $object_vars);
	
}

protected function attributes() {
// return an array of attributes key and their values
	//return get_object_vars($this);
	$attributes = array();
	foreach (self::$db_fields as $field) {
	if (property_exists($this, $field))
	$attributes[$field] = $this->$field;
	}
	
	return $attributes;
	
	
}

protected function sanitized_attributes() {
	global $database;
	$clean_attributes = array();
	// sanitize the value before submitting
	// does not alter the actual value  of each attribute
	foreach($this->attributes() as $key => $value){
	$clean_attributes[$key]	 = $database->escape_value($value);
	}
	
	return $clean_attributes;
}

public function save() {
//A new record won't have an id yet.
return isset($this->id) ? $this->update() : $this->create();


}
//protected is better, we force the user to call save()
public function create() {
global $database;
//Don't forget your SQL syntax and good habits:
// - INSERT INTO table (key, key)  VALUES ('value','value')
// - single-quotes around all values
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();

$sql = "INSERT INTO ".self::$table_name." (";
$sql .= join(", ", array_keys($attributes));
$sql .= ") VALUES ('";
$sql .= join("', '",array_values($attributes));
$sql .= "')";

if($database->query($sql)) {
	$this->id = $database->insert_id();
	return true;
} else {
	return false;
}


}
//protected is better, we force the user to call save()
public function update() {
	
global $database;
//don't forget your SQL syntax and good habits:
// - UPDATE table SET Key = 'value', key='value' WHERE condition
// - single-quotes around all value
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();
$attributes_pairs = array();

foreach ($attributes as $key => $value) {
	$attributes_pairs[] = "{$key}='{$value}'";
}

$sql = "UPDATE ". self::$table_name ." SET ";
$sql .= join(", ", $attributes_pairs);
$sql .= " WHERE id=". $database->escape_value($this->id);
return ($database->query($sql)) ? true : false;
}






}
?>