	<?php
	
	$errors = array();
	
	// * presence
	// use trim() so empty spaces don't count
	// use === to avoid false positives
	// empty() would consider "0" to be  empty
	//$value = trim("0");
	//if(!isset($value) || empty($value) && !is_numeric($value)) { // false || true = true ... "00"  false || false = false  ... "1"  false || true && false
	
	function has_presence($value) {
		return isset($value) && $value !== "";
	}
	
	function validate_presences($required_fields) {
	global $errors;
	foreach($required_fields as $field)	{
		$value = trim($_POST[$field]); 
		if (!has_presence($value)){
			$errors[$field."Presence"] = $field . " can't be blank";
			} 
		}
	}
	
	// * string length 
	// max length
	
	function is_inferior_max_length($value, $max) {
		return strlen($value) <= $max ;
	}
	
	
	
	function validate_max_lengths($fields_with_max_length){
	global $errors;	
		// Using an an assoc. array
	//$fields_with_max_lengths = array("username" => 30, "password" => 8);
	foreach ($fields_with_max_length as $field => $max) {
	$value = trim($_POST[$field]);
	if (!is_inferior_max_length($value,$max)) {
		$errors[$field] = ($field) . " is too long";
	}	
	}
	}
	
	function is_superior_min_length($value, $min) {
		return strlen($value) >= $min ;
	}
	
	function validate_min_lengths($fields_with_min_length){
	global $errors;	
		// Using an an assoc. array
	//$fields_with_max_lengths = array("username" => 30, "password" => 8);
	foreach ($fields_with_min_length as $field => $min) {
	$value = trim($_POST[$field]);
	if (!is_superior_min_length($value,$min)) {
		$errors[$field] = ($field) . " is too short";
	}	
	}
	}
	
	// * inclusion of set
	function has_inclusion_in($value,$set) {
		return in_array($value,$set);
	}
	
	function password_match($password, $againPassword){
	global $errors;
	if ($password != $againPassword) {
		$errors["passwordMatch"] = "Password does not Match";
	} else {
		return true;
	}	
	}
	
	function passwordExpressionCheck($password = ""){
	global $errors;
	$regex = "/^$|[a-z0-9_-]{6,18}$/";
	if (preg_match($regex, $password,$match )) {
    // Indeed, the expression matches the string
	return true;
	} else {
    // If preg_match() returns false, then the regex does not
    // match the string
    $errors["password"] = "The password is not Valid";
	}		
	}
	
	function emailExpressionCheck($email = ""){
	global $errors;
	$regex = '/^$|([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/';
	if (preg_match($regex, $email, $match )) {
	} else {
	$errors["email"] = "You email is not valid.";	
	}
	}
	
	function usernameExpressionCheck($username = ""){
	global $errors;
	$regex = '/^$|[a-z0-9_-]{6,16}$/';
	if (preg_match($regex, $username, $match )) {
	} else {
	$errors["username"] = "Your username must contain between 6 and 16 characters.";	
	}
	}
	
	
	
	function lots_validation($instrument,$sizeLot) {
	global $errors;
	if(doubleval($sizeLot)){
	$sizeLot = number_format($sizeLot, 2);
	if (0.01 <= $sizeLot && $sizeLot <= 1.0) {		
	} else {
	$errors["rangeSize"][] = $instrument. " :please insert a Value between 0.01 and 1.0 Lot(s)";
	}
	} else {
	$errors["formatLot"][] = $instrument. " :the format is incorrect!";
	}
	}
	
	
	
		
	?>