<?php
//If it's going to need the database, then it is probably smart to require it before we start
//require_once('functions.php');
//require_once('database.php');
//require_once("../../includes/initialize.php");
require_once(LIB_PATH.DS.'database.php');



class Traderinfo extends DatabaseObject {
	
protected static $table_name="traderaccounts";

protected static $db_fields = array('id', 'companyName', 'accountName', 'serverAccount', 'accountType', 'tradeAllowed','leverage','accountBalance','accountEquity','accountCurrency');

protected static $db_query_part = "id, companyName, accountName, serverAccount, accountType, tradeAllowed, leverage, accountBalance, accountEquity, accountCurrency";


public $id;
public $companyName;
public $accountName;
public $serverAccount;
public $accountType;
public $tradeAllowed;
public $leverage; 
public $accountBalance;  
public $accountEquity;  
public $accountCurrency;


public static function find_all(){  //not wrong to do with instance but better doing it with static video 35
global $database;
$result_set = self::find_by_sql("SELECT " . self::$db_query_part . " FROM " . self::$table_name);
return $result_set;
}

public static function find_by_id($id=0){
global $database;
//$result_set = $database->query("SELECT * FROM users WHERE id = {$id} LIMIT 1");
//$result_set = self::find_by_sql("SELECT * FROM users WHERE id = {$id} LIMIT 1");
$result_array = self::find_by_sql("SELECT " . self::$db_query_part . " FROM ".self::$table_name." WHERE id = {$id} LIMIT 1");
//$found = $database->fetch_array($result_set);
//return $found;
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function amount_Following_by_trader_account_id($traderaccount_id=0){
global $database;

$sql = "SELECT " . self::$db_query_part . " FROM ".self::$table_name." WHERE following = {$traderaccount_id}";
$result_set = $database->query($sql);
$totalAmountFollowing = 0;

while($row = $database->fetch_array($result_set))
{
$totalAmountFollowing += intval($row["accountBalance"]);
}


$result = $totalAmountFollowing;
return $result;

}


//AND statut = \'C\' AND flagOpen = \'Y\' AND flagClose = \'Y\'
public static function find_by_sql($sql=""){
global $database;
$result_set = $database->query($sql);
//return $result_set;
$object_array = array();
while($row = $database->fetch_array($result_set)){
	$object_array[] = self::instantiate($row);
}
return $object_array;
}



private static function instantiate($record) {

//could check if that $record exists and is an array

//Simple, long-form approach:

//an object should know how to build itself


 $object = new self;
// $object->id  		= $record['id'];
// $object->username 	= $record['username'];
// $object->password 	= $record['password'];
// $object->first_name = $record['first_name'];
// $object->last_name 	= $record['last_name'];

//More dynamic , short-form approach:

foreach ($record as $attribute=> $value){
if($object->has_attribute($attribute)){
	$object->$attribute = $value;
}
}

return $object;

}

private function has_attribute($attribute){
	//get_object_vars returns an associative array withh all attributes
	//(incl. private ones!) as the keys and their current values as the value
	$object_vars = get_object_vars($this);
	
	return array_key_exists($attribute, $object_vars);
	
}

protected function attributes() {
// return an array of attributes key and their values
	//return get_object_vars($this);
	$attributes = array();
	foreach (self::$db_fields as $field) {
	if (property_exists($this, $field))
	$attributes[$field] = $this->$field;
	}
	
	return $attributes;
	
	
}

protected function sanitized_attributes() {
	global $database;
	$clean_attributes = array();
	// sanitize the value before submitting
	// does not alter the actual value  of each attribute
	foreach($this->attributes() as $key => $value){
	$clean_attributes[$key]	 = $database->escape_value($value);
	}
	
	return $clean_attributes;
}

public function save() {
//A new record won't have an id yet.
return isset($this->id) ? $this->update() : $this->create();


}
//protected is better, we force the user to call save()
public function create() {
global $database;
//Don't forget your SQL syntax and good habits:
// - INSERT INTO table (key, key)  VALUES ('value','value')
// - single-quotes around all values
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();

$sql = "INSERT INTO ".self::$table_name." (";
$sql .= join(", ", array_keys($attributes));
$sql .= ") VALUES ('";
$sql .= join("', '",array_values($attributes));
$sql .= "')";

if($database->query($sql)) {
	$this->id = $database->insert_id();
	return true;
} else {
	return false;
}


}
//protected is better, we force the user to call save()
public function update() {
	
global $database;
//don't forget your SQL syntax and good habits:
// - UPDATE table SET Key = 'value', key='value' WHERE condition
// - single-quotes around all value
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();
$attributes_pairs = array();

foreach ($attributes as $key => $value) {
	$attributes_pairs[] = "{$key}='{$value}'";
}

$sql = "UPDATE ". self::$table_name ." SET ";
$sql .= join(", ", $attribute_pairs);
$sql .= " WHERE id=". $database->escape_value($this->id);
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;
	
}

public function delete() {
global $database;
// Don't forget your SQL syntax and good habits:
// - DELETE FROM table WHERE condition LIMIT 1
// - escape all values to prevent SQL injection
// - use LIMIT 1
$sql = "DELETE FROM ". self::$table_name ." ";
$sql .= "WHERE id=". $database->escape_value($this->id);
$sql .= " LIMIT 1";
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;

}



}
?>