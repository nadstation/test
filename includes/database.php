<?php
    //require_once("config.php");
	require_once(LIB_PATH.DS."config.php");
	
	
	
	class MySQLDatabase {
		
		private $connection; //otherwise wont be accessible by other function inside the class
		
		function __construct() {
		$this->open_connexion();
		}
		
		public function open_connexion(){
		// 1. Create a database connection
		$this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);
		//Test if connection occured.
		if(mysqli_connect_errno()) {
		die("Database connection failed: " . mysqli_connect_error() . " (" . mysqli_connect_errno() . ")" );
		}
		}
		
		public function close_connection() {
		if(isset($this->connection)){
		mysqli_close($this->connection);
		unset($this->connexion);
		} 
			
		}
		
		public function query($sql) {
		$result = mysqli_query($this->connection, $sql);
		if (!$result) {
		die(mysqli_error($this->connection));
		}
		$this->confirm_query($result);
		return $result;
		}
	
		private function confirm_query($result) {
		if(!$result) {
		die("Database query failed: " . mysqli_error() );
		}
		}
		
		public function escape_value($string) {
		//global $connection;
		$escaped_string = mysqli_real_escape_string($this->connection, $string);
		return $escaped_string;
		}
		
		//"database neutral" functions
		
		public function fetch_array($result_set) {
		return mysqli_fetch_array($result_set);
		}
		
		public function num_rows($result_set){
		return mysqli_num_rows($result_set);
		}
		
		public function insert_id() {
		//get the last id inserted over the current db connection
		return mysqli_insert_id($this->connection);
		}
		
		public function affected_rows() {
		//how many rows that was afffected by the last connection
		return mysqli_affected_rows($this->connection);
		}
		
		
	
	}
	
	$database = new MySQLDatabase();
	$db =& $database;
	//$database->open_connection();
?>