<?php
//If it's going to need the database, then it is probably smart to require it before we start
//require_once('functions.php');
//require_once('database.php');
//require_once("../../includes/initialize.php");
require_once(LIB_PATH.DS.'database.php');



class Browser extends DatabaseObject {
	
protected static $table_name="accounts";

protected static $db_fields = array('id','browsername' );

protected static $db_query_part = "id, browsername";

public $id;
public $browsername;




public static function find_by_id($id=0){
global $database;
$result_array = self::find_by_sql("SELECT " . self::$db_query_part . " FROM ".self::$table_name." WHERE id = {$id} LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}



public static function find_by_sql($sql=""){
global $database;
$result_set = $database->query($sql);
$object_array = array();
while($row = $database->fetch_array($result_set)){
	$object_array[] = self::instantiate($row);
}
return $object_array;
}


private static function instantiate($record) {

//could check if that $record exists and is an array

//Simple, long-form approach:

//an object should know how to build itself


 $object = new self;
// $object->id  		= $record['id'];
// $object->username 	= $record['username'];
// $object->password 	= $record['password'];
// $object->first_name = $record['first_name'];
// $object->last_name 	= $record['last_name'];

//More dynamic , short-form approach:

foreach ($record as $attribute=> $value){
if($object->has_attribute($attribute)){
	$object->$attribute = $value;
}
}

return $object;

}

private function has_attribute($attribute){
	//get_object_vars returns an associative array withh all attributes
	//(incl. private ones!) as the keys and their current values as the value
	$object_vars = get_object_vars($this);
	
	return array_key_exists($attribute, $object_vars);
	
}

protected function attributes() {
// return an array of attributes key and their values
	//return get_object_vars($this);
	$attributes = array();
	foreach (self::$db_fields as $field) {
	if (property_exists($this, $field))
	$attributes[$field] = $this->$field;
	}
	
	return $attributes;
	
	
}

protected function sanitized_attributes() {
	global $database;
	$clean_attributes = array();
	// sanitize the value before submitting
	// does not alter the actual value  of each attribute
	foreach($this->attributes() as $key => $value){
	$clean_attributes[$key]	 = $database->escape_value($value);
	}
	
	return $clean_attributes;
}

public function save() {
//A new record won't have an id yet.
return $this->update();
}


//protected is better, we force the user to call save()
public function update() {
	
global $database;
//don't forget your SQL syntax and good habits:
// - UPDATE table SET Key = 'value', key='value' WHERE condition
// - single-quotes around all value
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();
$attributes_pairs = array();

foreach ($attributes as $key => $value) {
	$attributes_pairs[] = "{$key}='{$value}'";
}

$sql = "UPDATE ". self::$table_name ." SET ";
$sql .= join(", ", $attributes_pairs);
$sql .= " WHERE id=". $database->escape_value($this->id);
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;
	
}

public function get_browser_name($user_agent)
{
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
    
    return 'Other';
}



}
?>