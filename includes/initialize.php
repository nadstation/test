<?php


	//Define the core paths
	//Define them as absolute paths to make sure that require_once works as expected
	
	//DIRECTORY_SEPARATOR is a pre-defined constant
	//(\for windows, /for Unix)
	
	defined('DS') ? NULL : define('DS', DIRECTORY_SEPARATOR);
	defined('SITE_ROOT') ? NULL : define('SITE_ROOT',DS.'Inetpub'.DS.'vhosts'.DS.'tradengo.co'.DS.'httpdocs'.DS);
	//C:\Inetpub\vhosts\jumpinvestor.com\httpdocs
	defined('LIB_PATH') ? NULL : define('LIB_PATH',SITE_ROOT.DS.'includes');
	
	
	//load config file first
	require_once(LIB_PATH.DS.'config.php');
	
	//load basic functions next so that everything can use them
    require_once(LIB_PATH.DS."functions.php");

	//load core objects
	require_once(LIB_PATH.DS."session.php");
	require_once(LIB_PATH.DS."database.php");
	require_once(LIB_PATH.DS."databaseobject.php");
	require_once(LIB_PATH.DS."pagination.php");
	
	//load database-related classes
	require_once(LIB_PATH.DS."user.php");
	require_once(LIB_PATH.DS."traderaccount.php");
	require_once(LIB_PATH.DS."investorpositions.php");
	require_once(LIB_PATH.DS."investorhistory.php");
	require_once(LIB_PATH.DS."traderhistory.php");
	require_once(LIB_PATH.DS."traderpositions.php");
	require_once(LIB_PATH.DS."userlink.php");
	require_once(LIB_PATH.DS."info.php");	
	require_once(LIB_PATH.DS."traderinfo.php");
	require_once(LIB_PATH.DS."photograph.php");
	require_once(LIB_PATH.DS."photographTrader.php");
	require_once(LIB_PATH.DS."notificationObj.php");
	require_once(LIB_PATH.DS."notificationTrader.php");
	require_once(LIB_PATH.DS."browser.php");
	require_once(LIB_PATH.DS."loginTwitter.php");
	require_once(LIB_PATH.DS."loginFacebook.php");
	require_once(LIB_PATH.DS."loginInvestor.php");
    require_once(LIB_PATH.DS."loginTrader.php");
	require_once(LIB_PATH.DS."traderCopy.php");
	require_once(LIB_PATH.DS."traderFollow.php");
	require_once(LIB_PATH.DS."currency.php");
	require_once(LIB_PATH.DS."lots.php");




	
	
?>