<?php
require_once(LIB_PATH.DS.'database.php');

class Investorpositions extends DatabaseObject {
	
protected static $table_name="investorpositions";

protected static $db_fields = array('id', 'account_id', 'traderaccount_id','masterOrderNumber', 'orderNumber', 'openTime', 'timeNow', 'openType', 'orderSize', 'orderSymbol', 'openPrice', 
'stopLoss', 'takeProfit','swap','profit','profitPips','statut','flagOpen','flagClose','flagManualClose','errorOpen','errorClose','pushNotif','pushData','expired');


public $id;
public $account_id;
public $traderaccount_id;
public $masterOrderNumber;
public $orderNumber;
public $openTime;
public $timeNow;
public $openType;
public $orderSize;
public $orderSymbol;
public $openPrice;
public $stopLoss;
public $takeProfit;
public $swap;
public $profit;
public $profitPips;
public $statut;
public $flagOpen;
public $flagClose;
public $flagManualClose;
public $errorOpen;
public $errorClose;
public $pushNotif;
public $pushData;
public $expired;




public static function find_all(){  //not wrong to do with instance but better doing it with static video 35
global $database;
$result_set = self::find_by_sql("SELECT * FROM " . self::$table_name);
return $result_set;
}


public static function find_all_open_position_by_account_id($account_id=0){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND statut='1' AND flagOpen='1' AND flagClose='0' ORDER BY openTime DESC");
return $result_set;
}

public static function find_all_open_position_by_account_id_and_traderaccount_id($account_id=0, $traderaccount_id=0 ){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND statut='1' AND flagOpen='1' AND flagClose='0' ORDER BY openTime DESC");
return $result_set;
}

public static function find_all_open_position_by_account_id_by_traderaccount_id($account_id=0, $traderaccount_id=0){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND traderaccount_id = {$traderaccount_id} AND statut='1' AND flagOpen='1' AND flagClose='0' ORDER BY openTime DESC");
return $result_set;
}

public static function find_open_position_by_account_id_and_masterordernumber($account_id=0, $masterOrderNumber=0){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND masterOrderNumber = {$masterOrderNumber} AND flagOpen='1' AND flagClose='0' LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_position_by_account_id_and_masterordernumber($account_id=0, $masterOrderNumber=0){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND masterOrderNumber = {$masterOrderNumber} LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_all_not_open_yet_position_by_account_id_and_traderaccount_id($account_id=0,$traderaccount_id=0){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND traderaccount_id = {$traderaccount_id} AND statut='1' AND flagOpen='0'");
return $result_set;
}


public static function find_the_last_open_position_by_account_id($account_id=0){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND statut='1' AND flagOpen='1' AND flagClose='0' ORDER BY openTime DESC LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}


public static function find_by_sql($sql=""){
global $database;
$result_set = $database->query($sql);
//return $result_set;
$object_array = array();
while($row = $database->fetch_array($result_set)){
	$object_array[] = self::instantiate($row);
}
return $object_array;
}

public static function count_expired_open($account_id, $traderaccount_id) {
global $database;
$sql = "SELECT COUNT(*) FROM " .self::$table_name. " WHERE account_id = " .$account_id. " AND traderaccount_id = ". $traderaccount_id ." AND expired='1'";
$result_set = $database->query($sql);
$row = $database->fetch_array($result_set);
return array_shift($row);
}

public static function count_all($account_id) {
global $database;
$sql = "SELECT COUNT(*) FROM " .self::$table_name. " WHERE account_id =" . $account_id;
$result_set = $database->query($sql);
$row = $database->fetch_array($result_set);
return array_shift($row);
}



private static function instantiate($record) {

//could check if that $record exists and is an array

//Simple, long-form approach:

//an object should know how to build itself


 $object = new self;
// $object->id  		= $record['id'];
// $object->username 	= $record['username'];
// $object->password 	= $record['password'];
// $object->first_name = $record['first_name'];
// $object->last_name 	= $record['last_name'];

//More dynamic , short-form approach:

foreach ($record as $attribute=> $value){
if($object->has_attribute($attribute)){
	$object->$attribute = $value;
}
}

return $object;

}

private function has_attribute($attribute){
	//get_object_vars returns an associative array withh all attributes
	//(incl. private ones!) as the keys and their current values as the value
	$object_vars = get_object_vars($this);
	
	return array_key_exists($attribute, $object_vars);
	
}

protected function attributes() {
// return an array of attributes key and their values
	//return get_object_vars($this);
	$attributes = array();
	foreach (self::$db_fields as $field) {
	if (property_exists($this, $field))
	$attributes[$field] = $this->$field;
	}
	
	return $attributes;
	
	
}

protected function sanitized_attributes() {
	global $database;
	$clean_attributes = array();
	// sanitize the value before submitting
	// does not alter the actual value  of each attribute
	foreach($this->attributes() as $key => $value){
	$clean_attributes[$key]	 = $database->escape_value($value);
	}
	
	return $clean_attributes;
}

public function save() {
//A new record won't have an id yet.
return isset($this->id) ? $this->update() : $this->create();


}
//protected is better, we force the user to call save()
public function create() {
global $database;
//Don't forget your SQL syntax and good habits:
// - INSERT INTO table (key, key)  VALUES ('value','value')
// - single-quotes around all values
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();

$sql = "INSERT INTO ".self::$table_name." (";
$sql .= join(", ", array_keys($attributes));
$sql .= ") VALUES ('";
$sql .= join("', '",array_values($attributes));
$sql .= "')";

if($database->query($sql)) {
	$this->id = $database->insert_id();
	return true;
} else {
	return false;
}


}

//protected is better, we force the user to call save()
public function update() {
	
global $database;
//don't forget your SQL syntax and good habits:
// - UPDATE table SET Key = 'value', key='value' WHERE condition
// - single-quotes around all value
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();
$attributes_pairs = array();

foreach ($attributes as $key => $value) {
	$attributes_pairs[] = "{$key}='{$value}'";
}

$sql = "UPDATE ". self::$table_name ." SET ";
$sql .= join(", ", $attributes_pairs);
$sql .= " WHERE id= '". $database->escape_value($this->id) . "'";
//return $sql;
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;
	
}


public function delete() {
global $database;
// Don't forget your SQL syntax and good habits:
// - DELETE FROM table WHERE condition LIMIT 1
// - escape all values to prevent SQL injection
// - use LIMIT 1
$sql = "DELETE FROM ". self::$table_name ." ";
$sql .= "WHERE id=". $database->escape_value($this->id);
$sql .= " LIMIT 1";
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;

}

/*public function deleteAllOpenYetPositions($traderaccount_id=0, $flagOpen=0, $flagClose=0) {
global $database;
// Don't forget your SQL syntax and good habits:
// - DELETE FROM table WHERE condition LIMIT 1
// - escape all values to prevent SQL injection
// - use LIMIT 1
$sql = "DELETE FROM ". self::$table_name ." ";
$sql .= "WHERE traderaccount_id= ". $traderaccount_id . " AND flagOpen= " . $flagOpen . " AND flagClose=" . $flagClose;
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;

}*/



}
?>