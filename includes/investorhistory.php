<?php
require_once(LIB_PATH.DS.'database.php');



class Investorhistory extends DatabaseObject {
	
protected static $table_name="investorhistory";

protected static $db_fields = array('id', 'account_id', 'traderaccount_id','masterOrderNumber', 'orderNumber', 'openTime', 'openType', 'orderSize', 'orderSymbol', 'openPrice', 
'stopLoss', 'takeProfit','closeTime','closePrice','swap','profit','profitPips','flagOpen','flagClose','flagManualClose','errorOpen','errorClose','expired','pushNotif','pushData');


public $id;
public $account_id;
public $traderaccount_id;
public $masterOrderNumber;
public $orderNumber;
public $openTime;
public $openType;
public $orderSize;
public $orderSymbol;
public $openPrice;
public $stopLoss;
public $takeProfit;
public $closeTime;
public $closePrice;
public $swap;
public $profit;
public $profitPips;
public $flagOpen;
public $flagClose;
public $flagManualClose;
public $errorOpen;
public $errorClose;
public $expired;
public $pushNotif;
public $pushData;





public static function find_all(){  //not wrong to do with instance but better doing it with static video 35
global $database;
$result_set = self::find_by_sql("SELECT * FROM " . self::$table_name);
return $result_set;
}

public static function find_all_closed_position(){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE flagClose='1' OR flagManualClose='1' ORDER BY closeTime DESC");
return $result_set;
}

public static function find_all_the_last_30_closed_position($account_id=0){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE flagOpen  ='1' AND (flagClose = '1' OR flagManualClose = '1') ORDER BY closeTime DESC LIMIT 30");
return $result_set;
}

public static function find_all_closed_position_by_account_id($account_id=0){
global $database;
$result_set = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND (flagClose='1' OR flagManualClose='1') ORDER BY closeTime DESC");
return $result_set;
}


public static function find_the_last_closed_position_by_account_id($account_id=0){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND flagOpen='1' AND flagClose='1' ORDER BY closeTime DESC LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_closed_position_by_account_id_and_masterordernumber($account_id=0, $masterordernumber=0 ){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND masterOrderNumber = {$masterordernumber} AND flagOpen='1' AND flagClose='1' LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}

public static function find_expired_position_by_account_id_and_masterordernumber($account_id=0, $masterordernumber=0){
global $database;
$result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." 
WHERE account_id = {$account_id} AND masterOrderNumber = {$masterordernumber} AND expired='1' LIMIT 1");
return !empty($result_array) ? array_shift($result_array) : false ;
}


public static function find_by_sql($sql=""){
global $database;
$result_set = $database->query($sql);
//return $result_set;
$object_array = array();
while($row = $database->fetch_array($result_set)){
	$object_array[] = self::instantiate($row);
}
return $object_array;
}

public static function count_all($account_id) {
global $database;
$sql = "SELECT COUNT(*) FROM " .self::$table_name. " WHERE account_id =" . $account_id;
$result_set = $database->query($sql);
$row = $database->fetch_array($result_set);
return array_shift($row);
}



private static function instantiate($record) {

//could check if that $record exists and is an array

//Simple, long-form approach:

//an object should know how to build itself


 $object = new self;
// $object->id  		= $record['id'];
// $object->username 	= $record['username'];
// $object->password 	= $record['password'];
// $object->first_name = $record['first_name'];
// $object->last_name 	= $record['last_name'];

//More dynamic , short-form approach:

foreach ($record as $attribute=> $value){
if($object->has_attribute($attribute)){
	$object->$attribute = $value;
}
}

return $object;

}

private function has_attribute($attribute){
	//get_object_vars returns an associative array withh all attributes
	//(incl. private ones!) as the keys and their current values as the value
	$object_vars = get_object_vars($this);
	
	return array_key_exists($attribute, $object_vars);
	
}

protected function attributes() {
// return an array of attributes key and their values
	//return get_object_vars($this);
	$attributes = array();
	foreach (self::$db_fields as $field) {
	if (property_exists($this, $field))
	$attributes[$field] = $this->$field;
	}
	
	return $attributes;
	
	
}

protected function sanitized_attributes() {
	global $database;
	$clean_attributes = array();
	// sanitize the value before submitting
	// does not alter the actual value  of each attribute
	foreach($this->attributes() as $key => $value){
	$clean_attributes[$key]	 = $database->escape_value($value);
	}
	
	return $clean_attributes;
}

public function save() {
//A new record won't have an id yet.
return isset($this->id) ? $this->update() : $this->create();


}
//protected is better, we force the user to call save()
public function create() {
global $database;
//Don't forget your SQL syntax and good habits:
// - INSERT INTO table (key, key)  VALUES ('value','value')
// - single-quotes around all values
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();

$sql = "INSERT INTO ".self::$table_name." (";
$sql .= join(", ", array_keys($attributes));
$sql .= ") VALUES ('";
$sql .= join("', '",array_values($attributes));
$sql .= "')";

if($database->query($sql)) {
	$this->id = $database->insert_id();
	return true;
} else {
	return false;
}


}

//protected is better, we force the user to call save()
public function update() {
	
global $database;
//don't forget your SQL syntax and good habits:
// - UPDATE table SET Key = 'value', key='value' WHERE condition
// - single-quotes around all value
// - escape all values to prevent SQL injection
$attributes = $this->sanitized_attributes();
$attributes_pairs = array();

foreach ($attributes as $key => $value) {
	$attributes_pairs[] = "{$key}='{$value}'";
}

$sql = "UPDATE ". self::$table_name ." SET ";
$sql .= join(", ", $attributes_pairs);
$sql .= " WHERE id= '". $database->escape_value($this->id) . "'";
//return $sql;
$database->query($sql);
return ($database->affected_rows() == 1) ? true : false;
	
}



}
?>