<?php require_once('../includes/mobileDetect/Mobile_Detect.php'); 
      require_once('../includes/functions.php'); 
      require_once('../includes/database.php');
 
      $session = new Session(); 

 $detect = new Mobile_Detect;
 
// Any mobile device (phones or tablets).
if ( $detect->isMobile() )  {
 echo "Mobile Version available soon!";
 redirect_to("http://m.TradeNgo.co");
} else {

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tradengo.co Beta</title>
    <link rel="manifest" href="manifest.json">

    <!-- Material Design Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Material Design Bootstrap -->
    <link href="assets/css/mdb.min.css" rel="stylesheet">

    <!-- Template styles -->
    <link href="assets/css/plugins/homeStyle.css" rel="stylesheet">

</head>

<body>

    <!--Navigation-->
    <nav class="navbar navbar-fixed-top z-depth-1" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <div class="navbar-brand" style="margin-left: 50px; "><img alt="TradeNgo" src="assets/images/logo.png" style="width: 50px;"></div>
            <div class="navbar-brand" style="padding-left: 0px;padding-top: 18px"><span >TradeNgo.co Beta</span></div>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav smooth-scroll">
                    <li><a href="#sec-home"          class="waves-effect waves-light">Home<i class="fa fa-home left"></i></a></li>
                    <li><a href="#sec-best-features" class="waves-effect waves-light">About</a></li>
                    <li><a href="#sec-details"       class="waves-effect waves-light">Details</a></li>
                    <li><a href="#sec-testimonials"  class="waves-effect waves-light">Testimonials</a></li>
                    <li><a href="#sec-pricing"       class="waves-effect waves-light">Pricing</a></li>
                    <li><a href="#sec-contact"       class="waves-effect waves-light">Contact</a></li>
                    <li><a href="#sec-footer"        class="waves-effect waves-light">Social</a></li>
                </ul>

                <!-- Navbar Icons -->
            <ul class="double-navbar list-inline pull-right text-center">
                <li><div class="waves-effect waves-light" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-sign-in" style="color: white; padding-top: 2px"></i><br><span style="color: white">Sign In</span></div></li>
                <?php if ($session->is_logged_in()) { ?>
                <li><a href="logout.php" ><div class="waves-effect waves-light"><i class="fa fa-sign-out" style="color: white; padding-top: 2px"></i><br><span style="color: white">Logout</span></div></a></li>

                <?php } else { } ?>

            </ul>
            <!--/. Navbar Icons -->

                

            </div>
        </div>
    </nav>
    <!--/.Navigation-->

    <!-- Modal -->
    <div class="modal fade" id="modalLogin" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header text-center">
                <h4><i class="fa fa-user"></i> Login with:</h4>
                <a class="btn-floating btn-large tw-bg waves-effect waves-light" href="https://www.tradengo.co/fbapp/signin.php"><i class="fa fa-facebook"></i></a>
                <a class="btn-floating btn-large tw-bg waves-effect waves-light" href="https://www.tradengo.co/twapp/index.php?signin=1"><i class="fa fa-twitter"></i></a>
                </div>
                <div class="modal-header text-center">
                    <h4><i class="fa fa-user"></i> Or:</h4>
                </div>
                <div class="modal-body" style="padding:40px 50px;">
                    <div class="row">
                        <form id="form_login" method="post" class="col-md-12">

                        <div id="resultLogin" style="text-align:center"></div>

                            <div class="row">
                                <div class="input-field">
                                    <i class="material-icons prefix">email</i>
                                    <input id="icon_email" type="text" class="validate" name="email">
                                    <label for="icon_email">Your email</label>
                                    <span name="email"></span>
                                </div>

                                <div class="input-field">
                                    <i class="material-icons prefix">lock</i>
                                    <input id="password" type="password" class="validate" name="password">
                                    <label for="password">Password</label>
                                    <span name="password"></span>
                                </div>
                                <div class="text-center">
                                    <button type="submit" name="submit" class="btn btn-primary waves-effect waves-light">Login</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal">X</button>
                    <div class="options">
                    <!--<p>Not a member? <a href="#">Sign Up</a></p>-->
                    <p>Forgot <a href="#">Password?</a></p>
                    </div>
                </div>
                <!--/.Footer-->
            </div>
            <!-- /.Modal content-->
        </div>
    </div>
    <!-- Modal -->

    <!-- Carousel -->
        <div id="main-carousel" class="carousel slide carousel-fade carousel-bg" data-interval="false" style="height: 800px;">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <!-- First slide -->
                <div class="item active">
                    <!--Video-->
                    <div style="height: 400px;">
                        <video autoplay loop class="fillWidth" style="width: 1440px">
                            <source src="assets/videos/test.mov" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                        </video>
                    </div>
                    <!--/.Video-->

                     <!--Alignment-->
            <div class="verticalcenter">
                <div class="container">
                    <div class="row">

                        <!--Content-->
                        <div class="col-md-6 white-text">
                            <h2 style="color: #ff9800">TradeNgo.co</h2>
                            <p>With more than 7 million MT4 plateform users over internet, we chose this technology because it is a very flexible plateform that give us the opportunity to extend our service efficiently</p>
                            <a href="#sec-best-features" class="btn btn-warning btn-lg waves-effect waves-light">Learn more</a>
                        </div>
                        <!--/.Content-->

                        <!--Form-->
                        <div class="col-md-6">
                            <div class="card-panel">
                                <!--Header-->
                                <div class="modal-header text-center">
                                    <h4><i class="fa fa-user"></i> Register with:</h4>
                                    <a class="btn-floating btn-large fb-bg waves-effect waves-light" href="https://www.tradengo.co/fbapp/signup.php"><i class="fa fa-facebook"> </i></a>
                                    <a class="btn-floating btn-large tw-bg waves-effect waves-light" href="https://www.tradengo.co/twapp/index.php?signup=1"><i class="fa fa-twitter"></i></a>
                                    <!--<a class="btn-floating btn-large gplus-bg waves-effect waves-light"><i class="fa fa-google-plus"> </i></a>
                                    <a class="btn-floating btn-large li-bg waves-effect waves-light"><i class="fa fa-linkedin"> </i></a>
                                    <a class="btn-floating btn-large git-bg waves-effect waves-light"><i class="fa fa-github"> </i></a>-->
                                </div>
                                <!--/.Header-->

                                <!--Body-->
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <!--Form-->
                                            <form id="form_signup" method="post" >
                                                <h4 class="text-center">or:</h4>

                                                <div id="result" style="text-align:center"></div>

                                                <div class="input-field">
                                                    <i class="material-icons prefix">account_circle</i>
                                                    <input id="icon_username" type="text" class="validate" name="username">
                                                    <label for="icon_username">Your name</label>
                                                    <span name="username"></span>
                                                </div>

                                                <div class="input-field">
                                                    <i class="material-icons prefix">email</i>
                                                    <input id="icon_email" type="text" class="validate" name="email">
                                                    <label for="icon_email">Your email</label>
                                                    <span name="email"></span>
                                                </div>

                                                <div class="input-field">
                                                    <i class="material-icons prefix">vpn_key</i>
                                                    <input id="icon_password" type="password" class="validate" name="password">
                                                    <label for="icon_password">Your password</label>
                                                    <span name="password"></span>
                                                </div>

                                                <div class="text-center">
                                                    <button type="submit" name="submit" class="btn btn-warning btn-lg waves-effect waves-light">Sign up</button>
                                                    <div class="subscription-checkbox">
                                                        <!--<form action="#">
                                                            <input type="checkbox" id="test6" />
                                                            <label for="test6">
                                                                <p>Subscribe me to the Newsletter</p>
                                                            </label>
                                                        </form>-->
                                                    </div>
                                                </div>
                                            </form>
                                            <!--/.Form-->
                                        </div>
                                    </div>
                                </div>
                                <!--/.Body-->
                            </div>
                        </div>
                        <!--/.Form-->

                    </div>
            </div>
            </div>
            <!--/.Alignment-->
                </div>
                <!-- /.First slide -->

                

                
            </div>
            <!-- /.carousel-inner -->

            
        </div>
        <!-- /.Carousel -->

    <!--Main container-->
    <div class="container">

        <!--Section: Best Features-->
        <section id="sec-best-features">
            <div class="divider-new">Best Features</div>

            <div class="row">

                <!--First column-->
                <div class="col-md-3">
                    <div class="elegant-card z-depth-1 hoverable wow slideInUp">
                        <!-- Image wrapper -->
                        <div class="view overlay hm-blue-slight">
                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(2).jpg" class="img-responsive" alt="">-->
                            <div class="mask waves-effect"></div>
                        </div>
                        <div class="card-content">
                            <h5><i style="margin-left:85px" class="fa fa-star" aria-hidden="true"></i></h5>
                            <p>Follow, Copy and AutoCopy™ top traders</p>
                        </div>
                    </div>
                </div>
                <!--/.First column-->

                <!--Second column-->
                <div class="col-md-3">
                    <div class="elegant-card z-depth-1 hoverable wow slideInUp" data-wow-delay="0.2s">
                        <!-- Image wrapper -->
                        <div class="view overlay hm-blue-slight">
                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(8).jpg" class="img-responsive" alt="">-->
                            <div class="mask waves-effect"></div>
                        </div>
                        <div class="card-content">
                            <h5><i style="margin-left:85px" class="fa fa-bar-chart fa-lg" aria-hidden="true"></i></h5>
                            <p>Analyze your performance with charts and graphs</p>
                        </div>
                    </div>
                </div>
                <!--/.Second column-->

                <!--Third column-->
                <div class="col-md-3">
                    <div class="elegant-card z-depth-1 hoverable wow slideInUp" data-wow-delay="0.4s">
                        <!-- Image wrapper -->
                        <div class="view overlay hm-blue-slight">
                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(10).jpg" class="img-responsive" alt="">-->
                            <div class="mask waves-effect"></div>
                        </div>
                        <div class="card-content">
                            <h5><i style="margin-left:85px" class="fa fa-comment-o" aria-hidden="true"></i></h5>
                            <p>Discuss market events and share trade ideas</p>
                        </div>
                    </div>
                </div>
                <!--/.Third column-->

                <!--Fourth column-->
                <div class="col-md-3">
                    <div class="elegant-card z-depth-1 hoverable wow slideInUp" data-wow-delay="0.5s">
                        <!-- Image wrapper -->
                        <div class="view overlay hm-blue-slight">
                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(13).jpg" class="img-responsive" alt="">-->
                            <div class="mask waves-effect"></div>
                        </div>
                        <div class="card-content">
                            <h5><i style="margin-left:85px" class="fa fa-link" aria-hidden="true"></i></h5>
                            <p>Keep your current broker, simply link your MT4 or account!</p>
                        </div>
                    </div>
                </div>
                <!--/.Fourth column-->

            </div>

        </section>
        <!--/.Section: Best Features-->

        <!--Section: Detailed info-->
        <div class="divider-new">What is TradeNgo?<!--<span class="hidden-xs">/Examples of use</span>--></div>
        <section id="sec-details">
            <!--First fow-->
            <div class="row">

                <!--First column-->
                <div class="col-md-5 wow slideInUp" data-wow-delay="0.2s">
                    <h4>Why is it so great?</h4>
                    <hr>
                    <p>Tradengo is an online social and trading network that enables its members to create a profile, copy and follow other top performing traders from around the world to simply and easily communicate, share trading strategies and market commentary. It gives the ability to link Forex trading accounts from any broker utilizing the MetaTrader 4 and MetaTrader 5 platforms.</p>
                    <br>
                    <p>Members can then view and analyze trading performance in real-time, AutoCopy trading signals and much more.</p>
                </div>
                <!--/.First column-->

                <!--Second column-->
                <div class="col-md-7">

                    <!-- Carousel -->
                    <div id="min-car2" class="carousel slide multiitem-car carousel-fade wow slideInUp" data-wow-delay="0.4s">
                        <div class="text-center">
                            <a class="btn-floating btn-small waves-effect waves-light" href="#min-car2" role="button" data-slide="prev"><i class="material-icons">keyboard_arrow_left</i></a>
                            <a class="btn-floating btn-small waves-effect waves-light" href="#min-car2" role="button" data-slide="next"><i class="material-icons">keyboard_arrow_right</i></a>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#min-car2" data-slide-to="0" class="active">
                            </li>
                            <li data-target="#min-car2" data-slide-to="1"></li>
                            <li data-target="#min-car2" data-slide-to="2"></li>
                            <li data-target="#min-car2" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">


                            <!-- First slide -->
                            <div class="item active">

                                <div class="col-md-12">

                                    <div class="elegant-card z-depth-1 hoverable">
                                        <!-- Image wrapper -->
                                        <div class="view overlay hm-blue-slight">
                                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(18).jpg" class="img-responsive" alt="">-->
                                            <div class="mask waves-effect"></div>
                                        </div>
                                        <div class="card-content">
                                            <h5>Available for Everyone</h5>
                                            <p>TradeNgo is free to join and use*. No matter if you are new to trading or a seasoned professional, you can meet thousands of people like you and benefit from the wisdom of the crowd.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- /.First slide -->

                            <!-- Second slide -->
                            <div class="item">

                                <div class="col-md-12">
                                    <div class="elegant-card z-depth-1 hoverable">
                                        <!-- Image wrapper -->
                                        <div class="view overlay hm-blue-slight">
                                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(19).jpg" class="img-responsive" alt="">-->
                                            <div class="mask waves-effect"></div>
                                        </div>
                                        <div class="card-content">
                                            <h5>No Conflicts of Interest</h5>
                                            <p>Unlike other “social” trading networks, TradeNgo is not a broker, asset manager or introducing broker. You are free to link your own brokerage account giving you full control over your trading environment.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.Second slide -->

                            <!-- Third slide -->
                            <div class="item">
                                <div class="col-md-12">
                                    <div class="elegant-card z-depth-1 hoverable">
                                        <!-- Image wrapper -->
                                        <div class="view overlay hm-blue-slight">
                                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(2).jpg" class="img-responsive" alt="">-->
                                            <div class="mask waves-effect"></div>
                                        </div>
                                        <div class="card-content">
                                            <h5>Earn Money Sharing your Signals</h5>
                                            <p>If you are a successful trader, why not share your performance and earn additional revenue. Choose your own fee model, prove your success and get attracted by thousands of investors.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.Third slide -->

                            <!-- Fourth slide -->
                            <div class="item">
                                <div class="col-md-12">
                                    <div class="elegant-card z-depth-1 hoverable">
                                        <!-- Image wrapper -->
                                        <div class="view overlay hm-blue-slight">
                                            <!--<img src="http://mdbootstrap.com/images/regular/work/img%20(1).jpg" class="img-responsive" alt="">-->
                                            <div class="mask waves-effect"></div>
                                        </div>
                                        <div class="card-content">
                                            <h5>Create your Dream Team</h5>
                                            <p>Why invest with a single asset manager when you can create a portfolio of traders and stay in control? Analyze performance of thousands of traders based on your selection criteria and AutoCopy their signals on your account.</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.Fourth slide -->
                        </div>
                        <!-- /.carousel-inner -->
                    </div>

                </div>
                <!--/.Second column-->

            </div>
            <!--/.First fow-->
        </section>
        <!--/.Section: Detailed info-->


        <!--Section: Testimonials-->
        <div class="divider-new">Testimonials</div>
        <section id="sec-testimonials">
            <!--Testimonial carousel-->
            <!-- Carousel -->
            <div id="carousel-multi-item" class="carousel slide multiitem-car">
                <div class="text-center">
                    <a class="btn-floating btn-small waves-effect waves-light" href="#carousel-multi-item" role="button" data-slide="prev"><i class="material-icons">keyboard_arrow_left</i></a>
                    <a class="btn-floating btn-small waves-effect waves-light" href="#carousel-multi-item" role="button" data-slide="next"><i class="material-icons">keyboard_arrow_right</i></a>
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-multi-item" data-slide-to="0" class="active">
                    </li>
                    <li data-target="#carousel-multi-item" data-slide-to="1"></li>
                    <li data-target="#carousel-multi-item" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">


                    <!-- First slide -->
                    <div class="item active">

                        <!-- Row -->
                        <div class="row text-center">
                            <!-- Card -->
                            <div class="col-sm-4 item-card wow fadeInUp">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up indigo">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/avatar-1.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Ted Govin</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs wow fadeInUp" data-wow-delay="0.2s">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up indigo darken-2">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/avatar-3.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Joanna Falles</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs wow fadeInUp" data-wow-delay="0.4s">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up indigo darken-4">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/avatar-2.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Victoria Vic</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->
                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.item -->

                    <!-- Second slide -->
                    <div class="item">
                        <!-- Row -->
                        <div class="row text-center">
                            <!-- Card -->
                            <div class="col-sm-4 item-card">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up red">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/team-avatar-1.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Jimmy Resky</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up red darken-2">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/team-avatar-2.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Hillary Cess</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up red darken-4">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/team-avatar-3.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Bill Grey</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.item -->

                    <!-- Third slide -->
                    <div class="item">
                        <!-- Row -->
                        <div class="row text-center">
                            <!-- Card -->
                            <div class="col-sm-4 item-card">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up green">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/team-avatar-3.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>John Tes</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up green darken-2">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/avatar-2.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Anna Maria</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->

                            <!-- Card -->
                            <div class="col-sm-4 item-card  hidden-xs">
                                <div class="testimonial-card z-depth-1 hoverable">
                                    <div class="card-up green darken-4">
                                    </div>
                                    <div class="avatar"><!--<img src="http://mdbootstrap.com/wp-content/uploads/2015/10/team-avatar-2.jpg" class="img-circle img-responsive">-->
                                    </div>
                                    <div class="card-content">
                                        <h5>Pawel Cosver</h5>
                                        <p><i class="fa fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, adipisci, voluptatum placeat ducimus vero commodi provident culpa accusamus nostrum dolor, labore ratione eius.</p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.Card -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.item -->

                </div>
                <!-- /.carousel-inner -->

            </div>
            <!--/.Testimonial carousel-->
        </section>
        <!--Section: Testimonials-->


        <!--Section: Pricing Plans-->
        <div class="divider-new">Pricing Plans</div>
        <section id="sec-pricing">
            <!--Pricing Cards-->
            <div class="row">

                <div class="col-md-4 wow fadeInUp">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="blue darken-2 z-depth-1">
                            <p class="no-margin white-text extra-padding-05">Basic</p>
                        </div>
                        <!--Price-->
                        <div class="card-up blue">
                            <h1 class="price">0</h1>
                        </div>
                        <!--Content-->
                        <div class="card-content">
                            <ul class="features-list">
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-times red-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-times red-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-times red-text"></i> Feature name</p>
                                </li>
                            </ul>
                            <hr>
                            <!--CTA-->
                            <a class="btn btn-primary btn-ptc waves-effect waves-light">Buy now</a>
                        </div>
                    </div>
                    <!--/.Pricing card-->

                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="indigo darken-2 z-depth-1">
                            <p class="no-margin white-text extra-padding-05">Pro</p>
                        </div>
                        <!--Price-->
                        <div class="card-up indigo">
                            <h1 class="price">0</h1>
                        </div>
                        <!--Content-->
                        <div class="card-content">
                            <ul class="features-list">
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-times red-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-times red-text"></i> Feature name</p>
                                </li>
                            </ul>
                            <hr>
                            <!--CTA-->
                            <a class="btn btn-danger btn-ptc waves-effect waves-light">Buy now</a>
                        </div>
                    </div>
                    <!--/.Pricing card-->

                </div>

                <div class="col-md-4 wow fadeInUp" data-wow-delay="0.4s">
                    <!--Pricing card-->
                    <div class="pricing-card text-center z-depth-1 hoverable">
                        <!--Label-->
                        <div class="deep-purple darken-2 z-depth-1">
                            <p class="no-margin white-text extra-padding-05">enterprise</p>
                        </div>
                        <!--Price-->
                        <div class="card-up deep-purple">
                            <h1 class="price">0</h1>
                        </div>
                        <!--Content-->
                        <div class="card-content">
                            <ul class="features-list">
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                                <li>
                                    <p><i class="fa fa-check green-text"></i> Feature name</p>
                                </li>
                            </ul>
                            <hr>
                            <!--CTA-->
                            <a class="btn btn-danger btn-ptc waves-effect waves-light">Buy now</a>
                        </div>
                    </div>
                    <!--/.Pricing card-->

                </div>

            </div>
            <!--/.Pricing Card-->
        </section>
        <!--Section: Testimonials-->


        <!--Section: Contact-->
        <div class="divider-new">Contact Us</div>
        <section id="sec-contact">

            <!--Contact Form-->
            <!-- Main forms row -->
            <div class="row wow fadeIn">
                <div class="col-md-8">
                    <div id="map-container" class="card-panel hoverable wow fadeInUp" style="height: 300px"></div>
                </div>
                <!-- Contact details -->
                <div class="col-md-4 text-center">


                    <ul class="contact-data">
                        <li class="wow fadeInUp" data-wow-delay="0.2s"><a class="btn-floating btn-small waves-effect waves-light blue-grey"><i class="material-icons">pin_drop</i></a>
                            <p>New York, NY 10012, USA</p>
                        </li>

                        <li class="wow fadeInUp" data-wow-delay="0.3s"><a class="btn-floating btn-small waves-effect waves-light blue-grey" data-toggle="modal" data-target="#contact-form"><i class="material-icons">phone</i></a>
                            <p>+ 01 234 567 89</p>
                        </li>

                        <li class="wow fadeInUp" data-wow-delay="0.4s"><a class="btn-floating btn-small waves-effect waves-light blue-grey" data-toggle="modal" data-target="#contact-form"><i class="material-icons">email</i></a>
                            <p>contact@mdbootstrap.com</p>
                        </li>
                    </ul>
                </div>
            </div>
            <!--/. Main row -->

        </section>
        <!--Section: Contact-->

    </div>
    <!--/.Main container-->

    <!-- Modal -->
    <div class="modal fade" id="infoNotif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">TradeNgo Notification</h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                <?php if ( $_GET["signup"] == "1" ) {
                echo "This account has already suscribed!" ;
                } else if ( $_GET["signin"] == "1" ) {
                echo "This account hasn't been found" ;
                }
                ?>
                
                </div>
                <!--Footer-->
                <div class="">
                    <!--<button type="button" class="btn btn-secondary" data-dismiss="modal" >Previous one</button>
                    <button type="button" class="btn btn-primary" id="resendNotification" >This one</button>-->
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- /.Live preview-->


    <!--Section: Footer-->
    <footer class="page-footer pt-color" id="sec-footer">
        <!--Footer Content-->
        <div class="container-fluid footer-content">
            <div class="row text-center wow fadeIn">
                <h4 class="white-text wow fadeIn">Find me on social media</h4>
                <a href="https://www.facebook.com/mdbootstrap" class="btn-sm-full fb-bg rectangle waves-effect waves-light wow fadeInUp"><i class="fa fa-facebook"> </i> <span>Facebook</span> </a>
                <a href="https://twitter.com/MDBootstrap" class="btn-sm-full tw-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.1s"><i class="fa fa-twitter"> </i> <span>Twitter</span></a>
                <a href="https://plus.google.com/+Mdbootstrap" class="btn-sm-full gplus-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.2s"><i class="fa fa-google-plus"> </i> <span>Google +</span></a>
                <a class="btn-sm-full li-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.3s"><i class="fa fa-linkedin"> </i> <span>LinkedIn</span></a>
                <a class="btn-sm-full ins-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.4s"><i class="fa fa-instagram"> </i> <span>Instagram</span></a>
                <a href="https://www.pinterest.com/mdbootstrap/" class="btn-sm-full pin-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.5s"><i class="fa fa-pinterest"> </i> <span>Pinterest</span></a>
                <a class="btn-sm-full yt-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.6s"><i class="fa fa-youtube"> </i> <span>Youtube</span></a>
                <a class="btn-sm-full git-bg rectangle waves-effect waves-light wow fadeInUp" data-wow-delay="0.7s"><i class="fa fa-github"> </i> <span>Github</span></a>
            </div>
        </div>
        <!--/.Footer Content-->

        <!--Coprytights-->
        <div class="footer-copyright text-center rgba-black-light wow fadeIn">
            <div class="container-fluid">
                © 2015 Copyright: <a href="https://www.tradengo.co"> tradeNgo.co </a>
            </div>
        </div>
        <!--/.Coprytights-->
    </footer>
    <!--/.Section: Footer-->
    <!-- SCRIPTS -->

    <script type="text/javascript" src="assets/js/main.js"></script>
    

    <!-- JQuery -->
    <script type="text/javascript" src="assets/js/plugins/jquery.min.js"></script>

    <script type="text/javascript" src="assets/js/plugins/jquery.validate.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/jquery.form.min.js"></script> 


    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="assets/js/plugins/bootstrap.min.js"></script>

    <!-- Material Design Bootstrap -->
    <script type="text/javascript" src="assets/js/plugins/mdb.min.js"></script>

    
    <script type="text/javascript" src="assets/js/forms/signUp.js"></script>

    <script type="text/javascript" src="assets/js/forms/login.js"></script>

    <script>
        jQuery(document).ready(function() {
            signUpForm.initSignUpForm();
            loginForm.initLoginForm();
        });
    </script>            


    <!--Google Maps-->
    <!--<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script>
        function init_map() {
            var var_location = new google.maps.LatLng(40.725118, -73.997699);

            var var_mapoptions = {
                center: var_location,
                zoom: 14
            };

            var var_marker = new google.maps.Marker({
                position: var_location,
                map: var_map,
                title: "New York"
            });

            var var_map = new google.maps.Map(document.getElementById("map-container"),
                var_mapoptions);

            var_marker.setMap(var_map);
        }

        google.maps.event.addDomListener(window, 'load', init_map);
    </script>-->

    <!--WOW effect initialization-->
    <script>
        new WOW().init();
    </script>

     <?php if ( $_GET["signup"] == "1" || $_GET["signin"] == "1"  ) { ?>
    <script>
    console.log("modal show");
    $('#infoNotif').modal('show');
    </script>
    <?php } ?>    

</body>

</html>
<?php } ?>