<?php
require_once('../../includes/initialize.php'); 
require_once("../../includes/functions.php"); 
session_start();
require 'autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

if($session->is_logged_in()){ /******notification*******/ // We need to be logged.
$id 			= $session->user_id;
if ( isset( $_REQUEST['oauth_verifier'], $_REQUEST['oauth_token'] ) && $_REQUEST['oauth_token'] == $_SESSION['oauth_token_notification'] ) {

$request_token = [];
$request_token['oauth_token'] 		 = $_SESSION['oauth_token_notification'];
$request_token['oauth_token_secret'] = $_SESSION['oauth_token_notification_secret'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
$_SESSION['access_token_notification'] = $access_token;

redirect_to('index.php?notification='.$id);

} else if (isset($_GET['denied'])) {
redirect_to('https://www.tradengo.co/admin.php');
}


} 

/*******signup*****/

if ( isset( $_REQUEST['oauth_verifier'], $_REQUEST['oauth_token'] ) && $_REQUEST['oauth_token'] == $_SESSION['oauth_token_signup'] ) {

$request_token = [];
$request_token['oauth_token'] 		 = $_SESSION['oauth_token_signup'];
$request_token['oauth_token_secret'] = $_SESSION['oauth_token_signup_secret'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
$_SESSION['access_token_signup'] = $access_token;
$value = "1";
redirect_to('index.php?signup='.$value);

} else if (isset($_GET['denied'])) {
redirect_to('https://www.tradengo.co/index.php');
}

/*******signin*****/

if ( isset( $_REQUEST['oauth_verifier'], $_REQUEST['oauth_token'] ) && $_REQUEST['oauth_token'] == $_SESSION['oauth_token_signin'] ) {

$request_token = [];
$request_token['oauth_token'] 		 = $_SESSION['oauth_token_signin'];
$request_token['oauth_token_secret'] = $_SESSION['oauth_token_signin_secret'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $request_token['oauth_token'], $request_token['oauth_token_secret']);
$access_token = $connection->oauth("oauth/access_token", array("oauth_verifier" => $_REQUEST['oauth_verifier']));
$_SESSION['access_token_signin'] = $access_token;
$value = "1";
redirect_to('index.php?signin='.$value);

} else if (isset($_GET['denied'])) {
redirect_to('https://www.tradengo.co/index.php');
}







