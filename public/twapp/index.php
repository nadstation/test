<?php

require_once("../../includes/initialize.php"); 
require_once("../../includes/functions.php");
session_start();
require 'autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$session = new Session(); 


if(isset($_GET["notification"])) { /*****notification******/

if(!isset($_SESSION['access_token_notification'])) {

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
$_SESSION['oauth_token_notification'] 		   = $request_token['oauth_token'];
$_SESSION['oauth_token_notification_secret']   = $request_token['oauth_token_secret'];
$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
redirect_to($url);

} else {

$id 			= $session->user_id;

$access_token = $_SESSION['access_token_notification'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");

$loginTwitter     				  = LoginTwitter::find_by_id($id);
$loginTwitter->notificationTw 	  = 1;

if ($user->type != 2 ) {
$loginTwitter->twLogUsername	  = $user->screen_name;
$loginTwitter->twLogToken 	  	  = $access_token['oauth_token'];
$loginTwitter->twLogTokenSecret   = $access_token['oauth_token_secret'];

} 

$loginTwitter->update();

unset($_SESSION['oauth_token_notification']);
unset($_SESSION['oauth_token_notification_secret']);


//if ($twitterApi->update()) {
redirect_to('https://www.tradengo.co/admin.php');
//}


}

} 


if (isset($_GET["signup"])) { /****signup****/

if(!isset($_SESSION['access_token_signup'])) {

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
$_SESSION['oauth_token_signup'] 						= $request_token['oauth_token'];
$_SESSION['oauth_token_signup_secret'] 					= $request_token['oauth_token_secret'];
$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
redirect_to($url);

} else {

$access_token = $_SESSION['access_token_signup'];

$isAccountExist = LoginTwitter::find_by_oauth_token($access_token['oauth_token']);

if ($isAccountExist) {
//Account alreaddy exist
redirect_to('https://www.tradengo.co/index.php?signup=1');	
} else {

/****signup****/
$user = new User;
if ($user->create()) {
$id = $user->id;
$userCreated = User::find_by_id($id);
$userCreated->type = "2";
$userCreated->update();

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$userTwitter = $connection->get("account/verify_credentials");

$loginTwitter = LoginTwitter::find_by_id($id);
$loginTwitter->twLogUsername 			= $userTwitter->screen_name;
$loginTwitter->twProfilePicture			= $userTwitter->profile_image_url_https;
$loginTwitter->twLogToken 		 	  	= $access_token['oauth_token'];
$loginTwitter->twLogTokenSecret   		= $access_token['oauth_token_secret'];
$loginTwitter->twLogName 				= $userTwitter->name;
$loginTwitter->update();

$loginTwitter->defaultConfig($loginTwitter->id);

unset($_SESSION['oauth_token_signup']);
unset($_SESSION['oauth_token_signup_secret']);

$found_account 		 = User::find_by_id($id);

if ($found_account) {

	$session->logout();
	//Success
	$session->login($found_account);
	redirect_to('https://www.tradengo.co/admin.php');
}

}

}

}

}

if(isset($_GET["signin"])) { /****login****/

if(!isset($_SESSION['access_token_signin'])) {
//redirect_to('http://www.jumpinvestor.com/index.php');

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
$request_token = $connection->oauth('oauth/request_token', array('oauth_callback' => OAUTH_CALLBACK));
$_SESSION['oauth_token_signin'] 						= $request_token['oauth_token'];
$_SESSION['oauth_token_signin_secret'] 					= $request_token['oauth_token_secret'];
$url = $connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
//$url .= "&force_login=false";
//$url = $connection->url('oauth/authenticate', array('force_login' => true));
redirect_to($url);


} else {

$access_token = $_SESSION['access_token_signin'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");


unset($_SESSION['oauth_token_signin']);
unset($_SESSION['oauth_token_signin_secret']);

if ($loginTwitter = LoginTwitter::find_by_oauth_token($access_token['oauth_token'])) {
$user = $connection->get("account/verify_credentials");


/**** Update username if different ****/
if ( $user->screen_name != $loginTwitter->twLogUsername ) {
$loginTwitter->twLogUsername = $user->screen_name;
$loginTwitter->update();
}

/**** Update name if different ****/
if ( $user->name != $loginTwitter->twLogName ) {
$loginTwitter->twLogName = $user->name;
$loginTwitter->update();
}
/**************************************/
$session->logout();

$session->login($loginTwitter);
redirect_to('https://www.tradengo.co/admin.php');

} else {
redirect_to('https://www.tradengo.co/index.php?signin=1');	
}


}

} 






?>