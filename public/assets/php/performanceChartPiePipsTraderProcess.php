<?php require_once("../../../includes/initialize.php");?>
<?php 

 
 if(isset($_POST["id"])){
 	
 $id = $db->escape_value($_POST["id"]);
 $result_set_closed_position = $db->query("SELECT orderSymbol FROM traderhistory WHERE traderaccount_id = {$id} AND flagClose='1' ORDER BY closeTime ASC");
 header('Content-type: application/json');
 $dat = array();
 $distinct = array();
 $obj = array();
 $objv = array();
 $arrayCountValues = array();

 while ($traderpositions = $db->fetch_array($result_set_closed_position)){
 $dat[] = $traderpositions["orderSymbol"]; 
 }
 
 $totalPositions = count($dat); //total positions

 $arrayCountValues = array_count_values($dat);
 foreach ($arrayCountValues as $key => $value) {
 $objv[] = $key;
 $objv[] = (($value/$totalPositions) * 100);
 
 $obj[]  = $objv;
 
 unset($objv);
 $objv = array();
 }
 
 

 
 echo json_encode($obj);
 
 
 }
?>