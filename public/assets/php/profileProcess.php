<?php
require_once("../../../includes/initialize.php"); 
require_once("../../../includes/functions.php"); 
require_once("../../../includes/validation_functions.php"); 

require '../../twapp/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$id = $session->user_id; 
?>
<?php

if(isset($_POST['profile'])){

		 $photo 		= Photograph::find_by_id($id);
		 $user  		= User::find_by_id($id);
		 $loginTwitter 	= LoginTwitter::find_by_id($id);
		 $loginInvestor = LoginInvestor::find_by_id($id);

		 $obj 			= array();	
	
		 $gender 		 = $db->escape_value($_POST["inlineRadioOptions"]);
		 $day 			 = $db->escape_value($_POST["day"]);
		 $month 		 = $db->escape_value($_POST['month']);
		 $year           = $db->escape_value($_POST['year']);
		 $dateStart		 = $db->escape_value($_POST['dateStart']);
		 $quote			 = $db->escape_value($_POST['quote']);
		 
		 $fields_with_max_lengths = array("quote" => 200);
		 validate_max_lengths($fields_with_max_lengths);
		 
		 if (!empty($errors)) {
		 $obj['responseText']= "failed";
		 $obj['list']= $errors; 
				
		 } else if (($gender && $day && $month && $year && $dateStart ) != null) {
		 			 
		 $birthDate = $year. "-" .$month. "-" .$day;

		 $user->gender 	  = $gender;
		 $user->age       = $birthDate;
		 $user->dateStart = $dateStart;
		 $user->quote     = $quote;

		 $user->update() ? $obj['responseText'] = "success" : $obj['responseText'] = "failed";

		 
		 /*if ($user->update()) {
		 if ($database->affected_rows() == 1) {
		 $obj['responseText']= "success";
		 } else {
		 $obj['responseText']= "warning";
		 }
		 } else {
		 $obj['responseText']= "error";	
		 }*/

		 }
		 	
		if(isset($_FILES['file_upload'])){

		if ($user->type == "1"){

		$picture  = $photo->filename;
		$username = $loginInvestor->username;
		
		
		$file = "../../images/".$username.'/'.$picture;
		if(file_exists($file)){
		$photo->destroy();	
		}
		
	    if ($photo->attach_file($_FILES['file_upload'])) {
		if($photo->save()) {
		
		$newPicture  	  = $photo->filename;
		$newPictureThumb  = $photo->filenamethumb;

		//Success
		$obj['responsePicture'] = "success";
        $obj['link']	 		= "../../images/".$username."/". $newPicture;
        $obj['linkThumb']		= "../../images/".$username."/". $newPictureThumb;
        $obj['type']= "1";

    	} else {
    	//Failed
    	$obj['responsePicture'] = "failed";
		$obj['pictureList'] = $photo->errors;	
		}
    	} else {
    	//Failed
    	$obj['responsePicture'] = "failed";
		$obj['pictureList'] = $photo->errors;
    	}

    	} else if ($user->type == "2") {

    	$type = $_FILES['file_upload']['type'];
    	$image = $_FILES['file_upload']['tmp_name'];

    	$ext = array("png", "jpeg");
    	
    	foreach ($ext as $value) {
    	if (strstr($image, $value)){
    	$format = $value;	
    	}
    	}

    	$base64 = base64_encode_image ($image,$format);

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $loginTwitter->twLogToken , $loginTwitter->twLogTokenSecret );
    	$connection->post('account/update_profile_image', array('image' => $base64.';type='.$type.';filename='.$image));

    	$twitterAccount = $connection->get("account/verify_credentials");

    	$loginTwitter->twProfilePicture = $twitterAccount->profile_image_url_https;

		if ($loginTwitter->update()) {

		$obj['responsePicture'] = "success";
		$obj['link']= $twitterAccount->profile_image_url_https;
		$obj['type']= "2";



		} else {
		$obj['responsePicture'] = "failed";
		}
		
    	}

		} else {

		// Failed
        $obj['responsePicture'] = "failed";
		$obj['pictureList'] = "There is no picture attached";

		}
		
		header('Content-type: application/json');
		echo json_encode($obj);
	    
		}


		
	
?>



	
  
  


		
