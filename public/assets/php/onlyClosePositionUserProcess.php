<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if(isset($_POST["idTrader"]) && isset($_POST["idUser"]) && isset($_POST["masterordernumber"])){
$obj = array();
$idTrader    		= $db->escape_value($_POST["idTrader"]);
$idUser      		= $db->escape_value($_POST["idUser"]);
$masterOrderNumber  = $db->escape_value($_POST["masterordernumber"]);

$investorpositions  	= Investorpositions::find_open_position_by_account_id_and_masterordernumber($idUser, $masterOrderNumber);
$investorOpenPositions  = Investorpositions::find_all_open_position_by_account_id_by_traderaccount_id($idUser, $idTrader);
$obj['count'] = count($investorOpenPositions);

if ($investorpositions) {
$investorpositions->statut   = 0;
$investorpositions->flagOpen = 1;
if ($investorpositions->update()) {
$result = 1;
} else {
$result = 0;	
}

} else {
$result = 0;	
}

header('Content-type: application/json');

if ($result){

$obj['response'] = "success";
} else {

$obj['response'] = "failed";
}

echo json_encode($obj);

 
}