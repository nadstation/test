<?php require_once("../../../includes/initialize.php");?>
<?php require_once("../../../includes/functions.php"); ?>
<?php 

 
 if(isset($_POST["id"])){
 	
 $id = $db->escape_value($_POST["id"]);

 $result_last_closed_position = $db->query("SELECT * FROM investorhistory 
 WHERE flagClose='1' OR flagManualClose='1' ORDER BY closeTime DESC LIMIT 30");

 $obj  = array();
 $objv = array();

 while($userpositions = $db->fetch_array($result_last_closed_position)){
 //$obj = array();

 $obj['orderNumber'] = $userpositions["orderNumber"];
 $obj['orderSymbol'] = $userpositions["orderSymbol"];
 $obj['closePrice']  = $userpositions["closePrice"];
 $obj['profitPips']  = $userpositions["profitPips"];
 $obj['account_id']  = $userpositions["account_id"];
 $photo 			 = Photograph::find_by_id($obj['account_id']);
 
 $user  			 = User::find_by_id($obj['account_id']);

 if ($user->type == "1") {
 $loginInvestor    = loginInvestor::find_by_id($obj['account_id']);
 $username = $loginInvestor->username;
 } else if ($user->type == "2") {
 $loginTwitter     = LoginTwitter::find_by_id($obj['account_id']);
 $username = $loginTwitter->username;
 }

 $obj['filename']	 = $photo->filename;
 $obj['username']	 = $username;
 $obj['result'] = 'default';
 if ( $userpositions["profitPips"] > 0) {
 $obj['result'] = 'Winning';
 } else if ($userpositions["profitPips"] < 0) {
 $obj['result'] = 'Losing'; 
 }



 $objv[] = $obj;

 //unset($obj);
 
 }
 

 header('Content-type: application/json');
 echo json_encode($objv);



 
 
 }
?>