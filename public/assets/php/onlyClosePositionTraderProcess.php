<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if(isset($_POST["id"]) && isset($_POST["idUser"])) {
	
$traderaccount_id     = $db->escape_value($_POST["id"]);
$account_id 		  = $db->escape_value($_POST["idUser"]);
$notClosePosition     = array();
$user              	  = User::find_by_id($account_id);	
$offset 		   	  = offsetTime($user->timezone);

$notClosePosition   = Investorpositions::find_all_open_position_by_account_id_and_traderaccount_id($account_id, $traderaccount_id);

for ($c=0; $c < count($notClosePosition)   ; $c++) {

$timestamp = strtotime($notClosePosition[$c]->openTime);

if ($user->isAutoTimezone) {

$newTimestamp 				   = $timestamp + ($user->detectOffsetTimezone * 3600);
$newTime 					   = date('Y-m-d H:i:s', $newTimestamp);
$notClosePosition[$c]->openTime = $newTime;

} else {

$newTimestamp   			   = $timestamp + $offset;
$newTime        			   = date('Y-m-d H:i:s', $newTimestamp);
$notClosePosition[$c]->openTime = $newTime;

}

}

header('Content-type: application/json');
$obj = array();
$obj['data']     = $notClosePosition;

echo json_encode($obj);

}