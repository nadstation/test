<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if ( isset($_POST["idTrader"]) && isset($_POST["idUser"]) && isset($_POST["ordernumber"]) ){

$obj = array();

$idTrader    = $db->escape_value($_POST["idTrader"]);
$idUser      = $db->escape_value($_POST["idUser"]);
$orderNumber = $db->escape_value($_POST["ordernumber"]);

$traderpositions   	= Traderpositions::find_by_traderaccount_id_and_ordernumber($idTrader, $orderNumber);


$result = 1;

if ($traderpositions) {

$investorPositions	 = InvestorPositions::find_position_by_account_id_and_masterordernumber($idUser, $orderNumber);

$historyPositions	 = Investorhistory::find_expired_position_by_account_id_and_masterordernumber($idUser, $orderNumber);


if (!$investorPositions && $historyPositions) {


//$historyPositions	 = new Investorhistory;
$openPositions	 	 = new InvestorPositions;

$openPositions->account_id 					= $idUser;
$openPositions->traderaccount_id 			= $traderpositions->traderaccount_id;
$openPositions->masterOrderNumber 			= $traderpositions->orderNumber;
$openPositions->openTime 					= date("Y-m-d H:i:s", time());
$openPositions->timeNow 					= date("Y-m-d H:i:s", time());
$openPositions->openType 					= $traderpositions->openType;
$openPositions->orderSize 					= $traderpositions->orderSize;
$openPositions->orderSymbol 				= $traderpositions->orderSymbol;
$openPositions->statut 						= 1;
$openPositions->flagOpen 					= 0;
$openPositions->flagClose 					= 0;



$historyPositions->account_id 				= $idUser;			
$historyPositions->traderaccount_id 		= $traderpositions->traderaccount_id;
$historyPositions->masterOrderNumber 		= $traderpositions->orderNumber;
$historyPositions->openTime 				= $traderpositions->openTime;
$historyPositions->openType 				= $traderpositions->openType;
$historyPositions->orderSize 				= $traderpositions->orderSize;
$historyPositions->orderSymbol				= $traderpositions->orderSymbol;
$historyPositions->profit					= $traderpositions->profit;
$historyPositions->profitPips				= $traderpositions->profitPips;


if ($openPositions->save() && $historyPositions->save()) {
$result = 1;
$obj['test']=1;
}


} else {
$result = 0;
$obj['test']=2;
} 

} else {
$result = 0;
$obj['test']=3;	
} 


header('Content-type: application/json');


if ($result){

$obj['response'] = "success";
//$obj['count']	 = $investorPositions	;

} else {
$obj['response'] = "failed";
}

echo json_encode($obj);

 
}




