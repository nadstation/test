<?php require_once("../../../includes/initialize.php");?>
<?php 
$session = new Session(); 

 if(isset($_POST["postid"])){

 header('Content-type: application/json');
 $obj = array();

 $traderFollow = new TraderFollow;

 	
 $traderaccount_id = $db->escape_value($_POST["postid"]);
 $account_id 	   = $db->escape_value($_POST["id"]);
 $traders 	       = TraderFollow::find_all_by_account_id($account_id);

 $exist = 0;

 //$notOpenPosition = array();

 for ($i=0; $i < count($traders) ; $i++) { 
 if ($traders[$i]->traderaccount_id == $traderaccount_id ) {
 $exist = 1; 
 } 
 }

 if ($exist == 1) {
 // We should not do anything but now we will remove the trader id

 $searchId = TraderFollow::find_by_account_id_and_traderaccount_id( $account_id, $traderaccount_id );
 $traderFollow->id = $searchId->id;
 $traderFollow->delete();
 
 
 $obj['exist']	  = $exist;
 $obj['limit']	  = 0;
 $obj['traderId'] = $traderaccount_id;
 $obj['response'] = "success";
 echo json_encode($obj);
 

 } else if ($exist == 0) {

 $numberOfTraders = TraderFollow::count_all_by_account_id($account_id);
 if ($numberOfTraders <= 4) {
 
 $traderFollow->create();

 $traderFollow->account_id 			= $account_id;
 $traderFollow->traderaccount_id 	= $traderaccount_id;

 if ($traderFollow->update()) {

 $obj['exist']	  	= $exist;
 $obj['limit']	  	= 0;
 $obj['traderId'] 	= $traderaccount_id;
 $obj['response'] 	= "success";
 echo json_encode($obj);
 } else {

 $obj['response'] = "failed";
 echo json_encode($obj);

 }

 } else {

 $obj['exist']	  = $exist;
 $obj['limit']	  = 1;
 $obj['response'] = "success";
 echo json_encode($obj);

 }


 }


 	
	
 }
 		
?>
 

