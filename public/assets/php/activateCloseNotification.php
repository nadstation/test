<?php require_once("../../../includes/initialize.php");?>
<?php 

 
 if(isset($_POST["id"])){
 	
 $id 				= $db->escape_value($_POST["id"]);

 $notification  				  = NotificationObj::find_by_id($id);
 $notification->notificationClose  = "1";
 $obj = array();

 header('Content-type: application/json');

 if ($notification->update()) {
 $obj['response'] = "success";
 } else {
 $obj['response'] = "failed";
 }

	
 echo json_encode($obj);

 
 }
?>