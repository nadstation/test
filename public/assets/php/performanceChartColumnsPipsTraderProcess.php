<?php require_once("../../../includes/initialize.php");?>

<?php 
 
 if(isset($_POST["id"])){
 	
 $id = $db->escape_value($_POST["id"]);
 $result_set_closed_position = $db->query("SELECT profitPips, closeTime FROM traderhistory WHERE traderaccount_id = {$id} AND flagClose='1' ORDER BY closeTime ASC");
 header('Content-type: application/json');
 $objFinal = array();
 $obj = array();
 $monthText = ['Jan' => 1,'Feb' => 2,'Mar' => 3,'Apr' => 4,'May' => 5,'Jun' => 6,'Jul' => 7,'Aou' => 8,'Sep' => 9,'Oct' => 10,'Nov' => 11,'Dec' => 12];

 while ($traderpositions = $db->fetch_array($result_set_closed_position)){
 $year			   				   = substr($traderpositions["closeTime"],0,4);
 $month			   				   = substr($traderpositions["closeTime"],5,2);
 foreach ($monthText as $k => $v){
 if ($month == $v) {
 $month = $k;
 }
 }

 $date 					= $month . " " . $year;
 $obj[$date]  	  		+= (double)round($traderpositions["profitPips"],2);
 
 }
 
 foreach ($obj as $key => $value) {
 $objFinal["date"][] 	= $key;
 $objFinal["profit"][] = $value;
 }

 echo json_encode($objFinal);
 
 
 }
?>