<?php 

require_once("../../../includes/initialize.php");
require_once("../../../includes/functions.php"); 
require_once("../../../includes/validation_functions.php");

$id = $session->user_id; 

?>


<?php

	if (isset($_POST['setting'])) {
	
	$currencyArray = array();

	$EURUSD = $db->escape_value($_POST["EURUSD"]);
	$USDJPY = $db->escape_value($_POST["USDJPY"]);
	$GBPUSD = $db->escape_value($_POST["GBPUSD"]);
	$AUDUSD = $db->escape_value($_POST["AUDUSD"]);
	$GBPJPY = $db->escape_value($_POST["GBPJPY"]);
	$USDCAD = $db->escape_value($_POST["USDCAD"]);
	$NZDUSD = $db->escape_value($_POST["NZDUSD"]);
	$EURJPY = $db->escape_value($_POST["EURJPY"]);
	$EURGBP = $db->escape_value($_POST["EURGBP"]);
	$GBPAUD = $db->escape_value($_POST["GBPAUD"]);
	$EURAUD = $db->escape_value($_POST["EURAUD"]);

	$arrayCurrency = array("EURUSD"=> $EURUSD,
						   "USDJPY"=> $USDJPY, 
						   "GBPUSD"=> $GBPUSD,
						   "AUDUSD"=> $AUDUSD, 
						   "GBPJPY"=> $GBPJPY,
						   "USDCAD"=> $USDCAD, 
						   "NZDUSD"=> $NZDUSD,
						   "EURJPY"=> $EURJPY, 
						   "EURGBP"=> $EURGBP, 
						   "GBPAUD"=> $GBPAUD, 
						   "EURAUD"=> $EURAUD);

	foreach ($arrayCurrency as $key => $value) {
	lots_validation($key,$value);
	}

	
		
	if (!empty($errors)) {
		 
	header('Content-type: application/json');
    $obj = array();
	$obj['response'] = "failed";
	$obj['list']= $errors; 
    echo json_encode($obj);
	
	} else {
	
	$currency  = new Currency();
    $currencies = $currency->find_all();

    for ($i=0; $i < count($currencies) ; $i++) { 
    $lot = Lots::find_by_account_id_and_currency_id($id, $currencies[$i]->id);
    $lot->account_id   		= $id;
    $lot->currency_id 		= $currencies[$i]->id;

	foreach ($arrayCurrency as $key => $value) {
   	if ($key == $currencies[$i]->currency){
   	$lot->value = $value;
   	}
    }

    if ($lot->save()){
    $result = 1;
    } else {
    $result = 0;
    }
	}
	
	if ($result) {
		
	header('Content-type: application/json');
    $obj = array();
	$obj['response'] = "success";
    echo json_encode($obj);
	
	} else {
		
	header('Content-type: application/json');
    $obj = array();
	$obj['response'] = "failed";
    echo json_encode($obj);	
		
	}
	
	}
		

		
		
	}	
		
	
	
	
	
	
	
	

	
	

	?>
			
			
			
			
			