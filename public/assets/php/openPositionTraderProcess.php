<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if(isset($_POST["id"]) && isset($_POST["idUser"])){
	
$id     = $db->escape_value($_POST["id"]);
$idUser = $db->escape_value($_POST["idUser"]);

$user              = User::find_by_id($idUser);	
$traderpositions   = Traderpositions::find_all_open_position_by_traderaccount_id($id);

$offset 		   = offsetTime($user->timezone);

for ($i=0; $i < count($traderpositions) ; $i++) { 

$timestamp = strtotime($traderpositions[$i]->openTime);

if ($user->isAutoTimezone) {

$newTimestamp 				   = $timestamp + ($user->detectOffsetTimezone * 3600);
$newTime 					   = date('Y-m-d H:i:s', $newTimestamp);
$traderpositions[$i]->openTime = $newTime;

} else {

$newTimestamp   			   = $timestamp + $offset;
$newTime        			   = date('Y-m-d H:i:s', $newTimestamp);
$traderpositions[$i]->openTime   = $newTime;

}

}

header('Content-type: application/json');
$obj = array();
$obj['data'] = $traderpositions;

echo json_encode($obj);

 
}