<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if(isset($_POST["id"]) && isset($_POST["idUser"])) {
	
$traderaccount_id     = $db->escape_value($_POST["id"]);
$account_id 		  = $db->escape_value($_POST["idUser"]);
$notOpenPosition      = array();
$user              	= User::find_by_id($account_id);	
$offset 		   	= offsetTime($user->timezone);

 $traderpositions   = Traderpositions::find_all_open_position_by_traderaccount_id($traderaccount_id);

if (count($traderpositions) > 0) { 
for ($i=0; $i < count($traderpositions) ; $i++) {

$investorpositions = Investorpositions::find_open_position_by_account_id_and_masterordernumber($account_id, $traderpositions[$i]->orderNumber);

if (!$investorpositions) {
$notOpenPosition[] = $traderpositions[$i];
}
}
}


for ($c=0; $c < count($notOpenPosition)   ; $c++) {

$timestamp = strtotime($notOpenPosition[$c]->openTime);

if ($user->isAutoTimezone) {

$newTimestamp 				   = $timestamp + ($user->detectOffsetTimezone * 3600);
$newTime 					   = date('Y-m-d H:i:s', $newTimestamp);
$notOpenPosition[$c]->openTime = $newTime;

} else {

$newTimestamp   			   = $timestamp + $offset;
$newTime        			   = date('Y-m-d H:i:s', $newTimestamp);
$notOpenPosition[$c]->openTime = $newTime;

}

}

header('Content-type: application/json');
$obj = array();
$obj['data']     = $notOpenPosition;

echo json_encode($obj);

}