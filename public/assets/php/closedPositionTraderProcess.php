<?php require_once("../../../includes/initialize.php");?>
<?php 

 
 if(isset($_POST["id"]) && isset($_POST["idUser"])){
 	
 $id     = $db->escape_value($_POST["id"]);
 $idUser = $db->escape_value($_POST["idUser"]);

 $user              = User::find_by_id($idUser);	
 $traderpositions   = Traderhistory::find_all_closed_position_by_traderaccount_id($id);

 $offset 			= offsetTime($user->timezone);


 for ($i=0; $i < count($traderpositions) ; $i++) { 

 $timestampClose = strtotime($traderpositions[$i]->closeTime);
 $timestampOpen  = strtotime($traderpositions[$i]->openTime);

 if ($user->isAutoTimezone) {	

 $newTimestampClose 	= $timestampClose + ($user->detectOffsetTimezone * 3600);
 $newTimeClose 	   		= date('Y-m-d H:i:s', $newTimestampClose);
 $traderpositions[$i]->closeTime = $newTimeClose;

 $newTimestampOpen  	= $timestampOpen + ($user->detectOffsetTimezone * 3600);
 $newTimeOpen       	= date('Y-m-d H:i:s', $newTimestampOpen);
 $traderpositions[$i]->openTime = $newTimeOpen;
 
 
 } else {

 $newTimestampClose 	= $timestampClose + $offset;
 $newTimeClose 	    	= date('Y-m-d H:i:s', $newTimestampClose);
 $traderpositions[$i]->closeTime = $newTimeClose;

 $newTimestampOpen   	= $timestampOpen + $offset;
 $newTimeOpen        	= date('Y-m-d H:i:s', $newTimestampOpen);
 $traderpositions[$i]->openTime = $newTimeOpen;

 }
 
 }

 header('Content-type: application/json');
 $obj = array();
 $obj['data'] = $traderpositions;

 echo json_encode($obj);

 
 }