<?php require_once("../../../includes/initialize.php");?>
<?php 
$session = new Session(); 

 if(isset($_POST["postid"])){

 header('Content-type: application/json');
 $obj = array();

 $traderCopy = new TraderCopy;
 	
 $traderaccount_id = $db->escape_value($_POST["postid"]);
 $account_id 	   = $db->escape_value($_POST["id"]);
 $traders 	       = TraderCopy::find_all_by_account_id($account_id);

 $exist = 0;

 $notOpenPosition = array();

 for ($i=0; $i < count($traders) ; $i++) { 
 if ($traders[$i]->traderaccount_id == $traderaccount_id ) {
 $exist = 1; 
 } 
 }

 if ($exist == 1) { 
 /***************REMOVE*****************/
 // We should not do anything but now we will remove the trader id

 $searchId = TraderCopy::find_by_account_id_and_traderaccount_id( $account_id, $traderaccount_id );
 $traderCopy->id = $searchId->id;
 $traderCopy->delete();

 $investorpositions = Investorpositions::find_all_open_position_by_account_id_by_traderaccount_id($account_id, $traderaccount_id);

 if (count($investorpositions) > 0)  {
 $obj['open']	  = 1;
 }
 
 $obj['exist']	  = $exist;
 $obj['limit']	  = 0;
 $obj['traderId'] = $traderaccount_id;
 $obj['response'] = "success";
 echo json_encode($obj);
 
 } else if ($exist == 0) { 
 /*************COPY**************/

 $numberOfTraders = TraderCopy::count_all_by_account_id($account_id);
 if ($numberOfTraders <= 4) { // No copy more than 4 Traders
 
 $traderCopy->create();

 $traderCopy->account_id 		= $account_id;
 $traderCopy->traderaccount_id 	= $traderaccount_id;

 if ($traderCopy->update()) {
 // Check if positions already opened.
 // When Copy a trader the position not yet opened ( expired ) doesn't exist right away on the database of the investor position.

 $traderpositions = Traderpositions::find_all_open_position_by_traderaccount_id($traderaccount_id);

 if (count($traderpositions) > 0) { 
 
 for ($i=0; $i < count($traderpositions) ; $i++) {

 //We check with the masterordernumber in the history of the investor if the position has already been traded.

 $existingPositions = Investorhistory::find_closed_position_by_account_id_and_masterordernumber($account_id, $traderpositions[$i]->orderNumber);


 if ($existingPositions) {
 $alreadyTraded = 1;
 } else {


 $investorpositions = Investorpositions::find_all_open_position_by_account_id($account_id);
 
 foreach ($investorpositions as $value ) {
 
 $p = 0;

 if ($investorpositions->masterOrderNumber == $traderpositions[$i]->orderNumber ) { 
 // check position ouverte sur trader et investor si OUI p>1
 // here "traderpositions"  > 0 and "the not open positions yet" = 0
 $p++;	

 }

 }

 }

 }

 if ($alreadyTraded) {
 $obj['open']	  = 0;	//The position has already been traded in the past we don't open the notification to show position.
 } else {


 if ( $p == 0 && count($investorpositions) == count($traderpositions) ) {
 $obj['open']	  = 0;
 } else {
 $obj['open']	  = 1;
 }

 }

 } else {
 $obj['open']	  = 0;
 }

 $obj['exist']	  	= $exist;
 $obj['limit']	  	= 0;
 $obj['traderId'] 	= $traderaccount_id;
 $obj['response'] 	= "success";
 echo json_encode($obj);
 } else {

 $obj['response'] = "failed";
 echo json_encode($obj);

 }

 } else {

 $obj['exist']	  = $exist;
 $obj['limit']	  = 1;
 $obj['response'] = "success";
 echo json_encode($obj);

 }

 }
	
 }
 		
?>
 

