<?php require_once("../../../includes/initialize.php");?>
<?php require_once("../../../includes/functions.php"); ?>
<?php require_once("../../../includes/validation_functions.php"); ?>


<?php 
$id = $session->user_id; 
?>


<?php

	if (isset($_POST['submit'])) {
	// Process the form
	// Often these are from values in $_POST
	$obj = array();

	$username 		 = $db->escape_value($_POST['username']);
	$email 			 = $db->escape_value($_POST['email']);
	$password 		 = $db->escape_value($_POST['password']);
	$passwordAgain   = $db->escape_value($_POST['passwordAgain']);
	$timezone		 = $db->escape_value($_POST['timezone']);
	$daylight		 = $db->escape_value($_POST['daylight']);
	$country		 = $db->escape_value($_POST['country']);
	$isAutoTimezone	 = $db->escape_value($_POST['isAutoTimezone']);

	$hashed_password = $db->escape_value(sha1($password));
	//$hash 			 = $db->escape_value(md5(rand(0,1000)));
	//$required_fields = array("timezone","daylight","country");
	//validate_presences($required_fields);
	
	//$fields_with_max_lengths = array( "email" => 40, "password" => 40, "passwordAgain" => 40);
	//validate_max_lengths($fields_with_max_lengths);
	
	emailExpressionCheck($email);
	
	if (password_match($password,$passwordAgain)) {
	passwordExpressionCheck($password);
	}

	$user = User::find_by_id($id);
	
	if ($user->type == "1") {
	$loginInvestor 	= LoginInvestor::find_by_email($email);
	$emailResult 	= $loginInvestor->email;
	($emailResult != "") ? $isEmail = "1" : $isEmail = "0";
	} else if ($user->type == "2") {
	$searchEmailLoginTwitter 		= LoginTwitter::find_by_email($email);
	$searchUsernameLoginTwitter 	= LoginTwitter::find_by_username($username);
	$emailResult 					= $searchEmailLoginTwitter->email;
	$usernameResult 				= $searchUsernameLoginTwitter->username;
	($emailResult 	 != "") ? $isEmail 	  = "1" : $isEmail 	  = "0";
	($usernameResult != "") ? $isUsername = "1" : $isUsername = "0";
	} else if ($user->type == "3") {
	$searchEmailLoginFacebook		= LoginFacebook::find_by_email($email);
	$searchUsernameLoginFacebook 	= LoginFacebook::find_by_username($username);
	$emailResult 					= $searchEmailLoginFacebook->email;
	$usernameResult 				= $searchUsernameLoginFacebook->username;
	($emailResult 	 != "") ? $isEmail 	  = "1" : $isEmail 	  = "0";
	($usernameResult != "") ? $isUsername = "1" : $isUsername = "0";
	}
	
	if (!empty($errors)) { //if errors
	header('Content-type: application/json');
    $obj['response']= "error";
	$obj['list']= $errors; 
    echo json_encode($obj);
    
	} else if ($isEmail) { 
		
	header('Content-type: application/json');
    $obj['response']= "emailExist";	
    echo json_encode($obj);
	
		
	} else if ($isUsername) {

	header('Content-type: application/json');
    $obj['response']= "usernameExist";	
    echo json_encode($obj);

	}  else {

	if ($user->type == "1") {
	$loginInvestor = LoginInvestor::find_by_id($id);
	} else if ($user->type == "2") {
	$loginTwitter = LoginTwitter::find_by_id($id);  
	} else if ($user->type == "3") {
	$loginFacebook = LoginFacebook::find_by_id($id);  
	}

	
	if ($password == "" && $email == "" && $username == ""){ // only Timezone & Location changed
	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;
	$user->update() ? $obj['response'] = "success" : $obj['response'] = "failed";

	
	} else if ($email == "" && $password == ""){ 			// only Username changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone 	  = $timezone;
	$user->daylight       = $daylight;
	$user->country        = $country;

	if ($user->type == "1") {
	$user->update() ? $obj['response'] = "success" : $obj['response'] = "failed";

	} else if ($user->type == "2") {
	$loginTwitter->username = $username;
	if($loginTwitter->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "username";
	$obj['username'] = $username;
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {
	$loginFacebook->username = $username;
	if($loginFacebook->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "username";
	$obj['username'] = $username;
	} else {
	$obj['response'] = "failed";
	}
	}

	} else if ($email == "" && $username == ""){			// only Password changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;

	if ($user->type == "1") {
	$loginInvestor->hashed_password = $hashed_password;
	if ($loginInvestor->update() && $user->update()) { 
	$obj['response'] = "success";
	$obj['ref'] 	 = "password";
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "2") {
	$loginTwitter->hashed_password = $hashed_password;
	if ($loginTwitter->update() && $user->update()) { 
	$obj['response'] = "success";
	$obj['ref'] 	 = "password";
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {
	$loginFacebook->hashed_password = $hashed_password;
	if ($loginFacebook->update() && $user->update()) { 
	$obj['response'] = "success";
	$obj['ref'] 	 = "password";
	} else {
	$obj['response'] = "failed";
	}
	}


	} else if ($password == "" && $username == ""){			// only Email changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;

	if ($user->type == "1") {

	$loginInvestor->email = $email;
	if ($loginInvestor->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "email";
	$obj['email'] 	 = $email;	
	} else { 
	$obj['response'] = "failed";
	}

	} else if ($user->type == "2") {

	$loginTwitter->email = $email;
	if($loginTwitter->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "email";
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {

	$loginFacebook->email = $email;
	if($loginFacebook->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "email";
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	}


	} else if ($password == "") {						// Username and Email changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;

	if ($user->type == "1") {
	$loginInvestor->email = $email;
	($loginInvestor->update() && $user->update()) ? $obj['response'] = "success" : $obj['response'] = "failed"; 
	} else if ($user->type == "2") {
	$loginTwitter->username = $username;
	$loginTwitter->email 	= $email;
	if($loginTwitter->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "emailUsername";
	$obj['username'] = $username;
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {
	$loginFacebook->username = $username;
	$loginFacebook->email 	= $email;
	if($loginFacebook->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "emailUsername";
	$obj['username'] = $username;
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	}


	} else if ($email == ""){						// Password and Username changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;

	if ($user->type == "1") {
	$loginInvestor->hashed_password = $hashed_password;
	($loginInvestor->update() && $user->update()) ? $obj['response'] = "success" : $obj['response'] = "failed"; 
	} else if ($user->type == "2") {
	$loginTwitter->username = $username;
	$loginTwitter->hashed_password = $hashed_password;
	if($loginTwitter->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "passwordUsername";
	$obj['username'] = $username;
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {
	$loginFacebook->username = $username;
	$loginFacebook->hashed_password = $hashed_password;
	if($loginFacebook->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "passwordUsername";
	$obj['username'] = $username;
	} else {
	$obj['response'] = "failed";
	}
	
	}


	} else if ($username == ""){				 //Password and Email changed

	$user->isAutoTimezone = $isAutoTimezone;
	$user->timezone = $timezone;
	$user->daylight = $daylight;
	$user->country  = $country;

	if ($user->type == "1") {
	$loginInvestor->email = $email;
	$loginInvestor->hashed_password = $hashed_password;
	if ($loginInvestor->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "passwordEmail";
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	} 
	} else if ($user->type == "2") {
	$loginTwitter->email = $email;
	$loginTwitter->hashed_password = $hashed_password;
	if($loginTwitter->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "passwordEmail";
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	} else if ($user->type == "3") {
	$loginFacebook->email = $email;
	$loginFacebook->hashed_password = $hashed_password;
	if($loginFacebook->update() && $user->update()) {
	$obj['response'] = "success";
	$obj['ref'] 	 = "passwordEmail";
	$obj['email'] 	 = $email;
	} else {
	$obj['response'] = "failed";
	}
	}


	} 
		                 
	header('Content-type: application/json');
    echo json_encode($obj);
        	
	} 
	
	}

	

	


	
	?>
			
			
			
			
			