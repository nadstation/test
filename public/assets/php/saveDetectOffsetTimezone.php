<?php require_once("../../../includes/initialize.php");?>
<?php 

 if(isset($_POST["detectOffsetTimezone"]) && isset($_POST["id"])){
 	
 $detectOffsetTimezone = $db->escape_value($_POST["detectOffsetTimezone"]);
 $id 				   = $db->escape_value($_POST["id"]);
 $user             	   = User::find_by_id($id);

 if ($user->isAutoTimezone) {

 $user->detectOffsetTimezone = $detectOffsetTimezone;
 $user->update();
 $obj = array();
 $obj['isOffset'] = "yes";
 $obj['offset']   = $detectOffsetTimezone;

 } else {
 $obj['isOffset'] = "no";
 }
 
 }

 header('Content-type: application/json');
 
 echo json_encode($obj);
 
?>