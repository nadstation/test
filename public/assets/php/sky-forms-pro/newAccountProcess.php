<?php require_once("../../../../includes/initialize.php"); ?>
<?php require_once("../../../../includes/functions.php"); ?>
<?php require_once("../../../../includes/validation_functions.php"); ?>
<?php require_once("../../../../includes/mail/PHPMailerAutoload.php"); ?>



<?php

	if (isset($_POST['submit'])) {
	// Process the form
	
	$username 		 = $db->escape_value($_POST["username"]);
	$type 			 = "1";
	$email 			 = $db->escape_value($_POST["email"]);
	$password 		 = $db->escape_value($_POST['password']);
	$hashed_password = $db->escape_value(sha1($password));
	$hash 			 = $db->escape_value(md5(rand(0,1000)));
	$required_fields = array("username","email","password");
	validate_presences($required_fields);
	
	usernameExpressionCheck($username);
	emailExpressionCheck($email);
	passwordExpressionCheck($password);
	
	
	//$fields_with_max_lengths = array("username" => 40,"email" => 40,"password" => 40);
	//validate_max_lengths($fields_with_max_lengths);
	$userEmail = User::find_by_email($email);
	$userUsername = LoginInvestor::find_by_user($username);

	if (!empty($errors)) { //if errors
	header('Content-type: application/json');
	$obj = array();
    $obj['response']= "error";
	$obj['list']= $errors; 
    echo json_encode($obj);
    
	} else if ($userEmail) { //if email existing
		
	header('Content-type: application/json');
	$obj = array();
    $obj['response']= "emailExist";	
    echo json_encode($obj);
	
	} else if ($userUsername){
	
	header('Content-type: application/json');
	$obj = array();
    $obj['response']= "userExist";	
    echo json_encode($obj);
		
	} else { // we can create the account

	$loginInvestor  = new LoginInvestor();

	$loginInvestor->username 			= $username 		;
	$loginInvestor->type 				= $type 			;
	$loginInvestor->email 				= $email 			;
	$loginInvestor->hashed_password 	= $hashed_password	;
	$loginInvestor->hash 				= $hash             ;

	if ($loginInvestor->save()){
	$result = 1;	
	$loginInvestor->defaultConfig($loginInvestor->id);
	$sup = "You know it!!";
	if (!file_exists('../../../images/' . $username )) {
    mkdir('../../../images/' . $username, 0777, true);
	}

	} else {
	$result = 0;	
	}

	
	
	if ($result) {
		
	$mail = new PHPMailer;
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'nad.benha@gmail.com';                 // SMTP username
	$mail->Password = 'grandeNadstation8!';                           // SMTP password
	//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to
	$mail->setFrom('from@example.com', 'Mailer');
	$mail->addAddress($email, $username);     // Add a recipient
	                              // Set email format to HTML
	$mail->Subject = 'Welcome to JumpInvestor.com';
	$mail->Body    = '
	Thanks for signing up! 
	Your account has been created, you can login with the following credentials after you have activated your account 
	by pressing the url below.
	 
	------------------------
	Username: '.$username.'
	Password: '.$sup.'
	------------------------
	 
	Please click this link to activate your account:
	https://www.tradengo.co/verify.php?email='.$email.'&hash='.$hash; // Our message above including the link';
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
	
	$mail->send();
	
		
                     
	header('Content-type: application/json');
    $obj = array();
    $obj['response'] = "success"; 
    echo json_encode($obj);
        	
	} else {
	//Failure
	//$_SESSION["message"] = "Subject creation failed";
	header('Content-type: application/json');
    $obj = array();
    $obj['response']= "failed"; 
    echo json_encode($obj);
	
	
	}
	
	}
		
	} else {
	//This is probably a GET request
	redirect_to("https://www.tradengo.co");
	}
		
?>



<?php
//if (isset($connetion)) { mysqli_close($connection); }
?>

