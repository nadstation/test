<?php require_once("../../../../includes/initialize.php"); 				?>
<?php require_once("../../../../includes/functions.php"); 				?>
<?php require_once("../../../../includes/validation_functions.php"); 	?>

<?php

	$email = "";

	if (isset($_POST['submit'])) {
	//Process the form
	//Validations
	
	$email = $db->escape_value($_POST["email"]);
	$password = $db->escape_value($_POST["password"]);
	
	$required_fields = array("email","password");
	validate_presences($required_fields);
	
	emailExpressionCheck($email);
	passwordExpressionCheck($password);
	
	
	
	
	if (empty($errors)) { 
	// Attempt Login	
	
	$found_account = User::authenticate($email, $password);	
	
	if ($found_account) {
	//Success
	$session->login($found_account);
	
	header('Content-type: application/json');
    $obj = array();
    $obj['response'] = "success"; 
    echo json_encode($obj);
	
	
	} else {
	//Failure
	//$_SESSION["message"] = "email/password not found."; 
	header('Content-type: application/json');
    $obj = array();
    $obj['response']= "failed"; 
    echo json_encode($obj);
	
	}
		
	
	} else {
	
	header('Content-type: application/json');
	$obj = array();
    $obj['response']= "error";
	$obj['list']= $errors; 
    echo json_encode($obj);
	
	}	
	} else {
	//This is probably a GET request
	redirect_to("http://www.jumpinvestor.com");
	} //end: if (isset($_POST['submit'])) 
		
?>