<?php require_once("../../../../includes/initialize.php"); ?>
<?php require_once("../../../../includes/functions.php"); ?>
<?php require_once("../../../../includes/validation_functions.php"); ?>


<?php

	if (isset($_POST['submit'])) {
	// Process the form
	
	
	// Often these are from values in $_POST
	$name = $db->escape_value($_POST["name"]);
	$email = $db->escape_value($_POST["email"]);
	$message = $db->escape_value($_POST["message"]);
	
	//Validations
	$required_fields = array("name","message");
	validate_presences($required_fields);
	
	
	$fields_with_max_lengths = array("name" => 50,"message" => 500);
	$fields_with_min_lengths = array("name" => 10,"message" => 10);
	
	validate_max_lengths($fields_with_max_lengths);
	validate_min_lengths($fields_with_min_lengths);
	
	emailExpressionCheck($email);
	
	if (!empty($errors)) {
	header('Content-type: application/json');
	$obj = array();
    $obj['response']= "Please fix the following errors:";
	$obj['list']= $errors; 
    echo json_encode($obj);
	} else {
	
	// 2. Perform database query
	$query  = "INSERT INTO messages (";
	$query .= " name, email, message";
	$query .= ") VALUES (";
	$query .= " '{$name}','{$email}','{$message}'";
	$query .= ")";
	
	//$result = mysqli_query($connection, $query);
	$result = $db->query($query);	//Test if there was a query error.
	
	if ($result) {
	//Success
		
	header('Content-type: application/json');
    $obj = array();
    $obj['response']= "success"; 
    echo json_encode($obj);   
	
	} else {
	//Failure
	header('Content-type: application/json');
    $obj = array();
    $obj['response']= "failed"; 
    echo json_encode($obj);
	
	
	}
	
	}
		
	} else {
	//This is probably a GET request
	redirect_to("http://www.jumpinvestor.com");
	//echo "coco";
	}
		
?>



<?php
//if (isset($connetion)) { mysqli_close($connection); }
?>