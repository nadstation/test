<?php require_once("../../../includes/initialize.php");?>
<?php 

 
if(isset($_POST["id"])){
	
$id = $db->escape_value($_POST["id"]);

$user              = User::find_by_id($id);	
$userpositions 	   = Investorhistory::find_all_closed_position_by_account_id($id);

$offset 			= offsetTime($user->timezone);

for ($i=0; $i < count($userpositions) ; $i++) { 

$timestampClose = strtotime($userpositions[$i]->closeTime);
$timestampOpen  = strtotime($userpositions[$i]->openTime);

if ($user->isAutoTimezone) {	

$newTimestampClose = $timestampClose + ($user->detectOffsetTimezone * 3600);
$newTimeClose 	   = date('Y-m-d H:i:s', $newTimestampClose);
$userpositions[$i]->closeTime = $newTimeClose;

$newTimestampOpen  = $timestampOpen + ($user->detectOffsetTimezone * 3600);
$newTimeOpen       = date('Y-m-d H:i:s', $newTimestampOpen);
$userpositions[$i]->openTime = $newTimeOpen;
 
} else {

$newTimestampClose 	= $timestampClose + $offset;
$newTimeClose 	    = date('Y-m-d H:i:s', $newTimestampClose);
$userpositions[$i]->closeTime = $newTimeClose;

$newTimestampOpen   = $timestampOpen + $offset;
$newTimeOpen        = date('Y-m-d H:i:s', $newTimestampOpen);
$userpositions[$i]->openTime = $newTimeOpen;

} 

}

header('Content-type: application/json');
$obj = array();
$obj['data'] = $userpositions;

echo json_encode($obj);

}


