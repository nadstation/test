<?php require_once("../../../includes/initialize.php");?>
<?php 

if(isset($_POST["id"])){
 
$idUser   	= $db->escape_value($_POST["id"]);
$user 	  	= User::find_by_id($idUser); 

$offset = offsetTime($user->timezone);

 
$dat_id 	= array();
$result_set_id_traderaccounts = $db->query("SELECT id FROM traderaccounts WHERE 1"); // 2 differents ID
 
$finalArray = array();
 
while ($id_traders = $db->fetch_array($result_set_id_traderaccounts)){
$dat_id[] 	= (int)$id_traders["id"]; // [1,3]
}			//2
for ($i=0; $i < count($dat_id); $i++) {

$id = $dat_id[$i]; //1 first and then 3 second
$finalArray["id"][] = $dat_id[$i];

$result_set_closed_position = $db->query("SELECT closeTime, profitPips FROM traderhistory WHERE traderaccount_id = {$id} AND flagClose='1' ORDER BY closeTime LIMIT 10");

$dat 	= array();
$objv 	= array();
$obj 	= array();


while ($traderpositions = $db->fetch_array($result_set_closed_position)){
$objv['profit'][] 		= (double)round($traderpositions["profitPips"],2); 
  /******/

$timestamp = strtotime($traderpositions["closeTime"]); 

if ($user->isAutoTimezone) { 
$newTimestamp     = $timestamp + ($user->detectOffsetTimezone * 3600);
$objv['time'][]   = $newTimestamp * 1000;
} else {
$newTimestamp 	  = $timestamp + $offset;
$objv['time'][]   = $newTimestamp * 1000;
}

}

for ($j=0; $j < count($objv['profit']); $j++) {	
$objv['profit'][$j] = $objv['profit'][$j-1] +$objv['profit'][$j];
$dat[] 				= $objv['time'][$j];
$dat[] 				= $objv['profit'][$j];
$obj[]   			= $dat; 
unset($dat);
$dat = array();
}

$finalArray["performance"][] = $obj;

unset($obj);
$obj = array();

unset($objv);
$objv = array();


}

header('Content-type: application/json');

echo json_encode($finalArray);

}
?>