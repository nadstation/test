<?php require_once("../../../includes/initialize.php");?>
<?php require_once("../../../includes/functions.php"); ?>
<?php require_once("../../../includes/validation_functions.php"); ?>
<?php require_once("../../../includes/mail/PHPMailerAutoload.php"); ?>
<?php 

 
 if(isset($_POST["id"]) && isset($_POST["idUser"])){
 	
 $id 	 = $db->escape_value($_POST["id"]);
 $idUser = $db->escape_value($_POST["idUser"]);

 $user   = User::find_by_id($idUser);

 $offset = offsetTime($user->timezone);

 $result_set_closed_position = $db->query("SELECT closeTime, profit FROM traderhistory WHERE traderaccount_id = {$id} AND flagClose='1' ORDER BY closeTime ASC");
 header('Content-type: application/json');
 $dat = array();
 $objv = array();
 $obj = array();

 while ($traderpositions = $db->fetch_array($result_set_closed_position)){
 $objv['profit'][] = (double)round($traderpositions["profit"],2); 
 $timestamp = strtotime($traderpositions["closeTime"]); 

 if ($user->isAutoTimezone) { 
 $newTimestamp = $timestamp + ($user->detectOffsetTimezone * 3600);
 $objv['time'][]   = $newTimestamp * 1000;
 } else {
 $newTimestamp = $timestamp + $offset;
 $objv['time'][]   = $newTimestamp * 1000;
 } 

 }
 
 for ($i=0; $i < count($objv['profit']); $i++) {	
 $objv['profit'][$i] = $objv['profit'][$i-1] +$objv['profit'][$i];
 $dat[] 	= $objv['time'][$i];
 $dat[] 	= $objv['profit'][$i];
 $obj[]   	= $dat; 
 unset($dat);
 $dat = array();
 }
 
 
 
 echo json_encode($obj);
 
 
 }
?>