<?php require_once("../../../includes/initialize.php"); ?>
<?php require_once("../../../includes/functions.php"); ?>
<?php require_once("../../../includes/validation_functions.php"); ?>



<?php

	if (isset($_POST['submit'])) {
	// Process the form

	$obj = array();

	
	$username 		 = $db->escape_value($_POST["username"]);
	$password 		 = $db->escape_value($_POST["password"]);
	$hashed_password = $db->escape_value(sha1($password));
	$hash 			 = $db->escape_value(md5(rand(0,1000)));
	$required_fields = array("username","password");
	validate_presences($required_fields);
	
	usernameExpressionCheck($username);
	passwordExpressionCheck($password);
	
	$traderName 	= LoginTrader::find_by_user($username);

	if (!empty($errors)) { //if errors
	header('Content-type: application/json');
    $obj['response']= "error";
	$obj['list']= $errors; 
    echo json_encode($obj);
    
	} else if ($traderName){
	
	header('Content-type: application/json');
    $obj['response']= "userExist";	
    echo json_encode($obj);
		
	} else { // we can create the account
	
	$loginTrader  = new LoginTrader();

	$loginTrader->username 			= $username 		;
	$loginTrader->hashed_password 	= $hashed_password	;

	if ($loginTrader->save()){
	if (!file_exists('../../imagesTrader/' . $username )) {
    mkdir('../../imagesTrader/' . $username, 0777, true);
	}

	$id 	= $loginTrader->id;
	$photo 	= PhotographTrader::find_by_id($id);

	if(isset($_FILES['file_upload'])){
	
    if ($photo->attach_file($_FILES['file_upload'])) {

	if($photo->save()) {
	
	$newPicture  	  = $photo->filename;
	$newPictureThumb  = $photo->filenamethumb;

	//Success
	$obj['responsePicture'] = "success";
    $obj['link']	 		= "../../imagesTrader/".$username."/". $newPicture;
    $obj['linkThumb']		= "../../imagesTrader/".$username."/". $newPictureThumb;

	} else {
	//Failed
	$obj['responsePicture'] = "failed";
	$obj['pictureList'] = $photo->errors;	
	
	}

	} else {
	//Failed
	$obj['responsePicture'] = "failed";
	$obj['pictureList'] = $photo->errors;
	}

	} else {

	// Failed
    $obj['responsePicture'] = "failed";
	$obj['pictureList'] = "There is no picture attached";

	}

	
	if ($result=1) {	
                     
	header('Content-type: application/json');
    $obj['response'] = "success"; 
    echo json_encode($obj);
        	
	} else {
	//Failure

	header('Content-type: application/json');
    $obj['response']= "failed"; 
    echo json_encode($obj);
	
	
	}
	
	} else {
	//Failure
		
	header('Content-type: application/json');
    $obj['response']= "failed"; 
    echo json_encode($obj);
	
	
	}

	}
		
	}
		
?>




