<?php require_once("../../../includes/initialize.php"); ?>

<?php 

 
 if(isset($_POST["subscriptionId"])){ //account_id
 $obj = array();
 
 $subscriptionId 		= $db->escape_value($_POST["subscriptionId"]); // get account_id of the position 
 $notificationObj 		= NotificationObj::find_by_subscriptionId($subscriptionId);



 $account_id 			= $notificationObj->id;

 if ($notificationObj->notificationSource == "trader") {

 $allTraderaccount_id =	TraderFollow::find_all_by_account_id($account_id);
 $traderaccount_idsArray = array();

 for ($i=0; $i < count($allTraderaccount_id) ; $i++) { 
 $traderaccount_idsArray[] = $allTraderaccount_id[$i]->traderaccount_id; 
 } //[5,6...]

 $lastTraderPosition = Traderhistory::find_the_last_position_by_traderaccount_ids($traderaccount_idsArray);

 $orderType 		 = NotificationTrader::find_by_id($lastTraderPosition->traderaccount_id);

 if ($orderType->notificationType == "open") {

 $lastOpenTraderPosition = Traderpositions::find_the_last_open_position_by_traderaccount_ids($traderaccount_idsArray);
 $lastOpenTraderPosition->openType == "0" ? $openType = "Bought" : $openType = "Sold";


 if ( $lastTraderPosition->traderaccount_id == $lastOpenTraderPosition->traderaccount_id ) {

 $traderAccount_id 		= $lastOpenTraderPosition->traderaccount_id;
 $traderAccount 		= Traderaccount::find_by_id($traderAccount_id);
 $traderName 			= $traderAccount->username;

 $obj['orderSymbol'] 	= $lastOpenTraderPosition->orderSymbol;
 $obj['openTime']		= $lastOpenTraderPosition->openTime;
 $obj['orderSize']		= $lastOpenTraderPosition->orderSize;
 $obj['openType']		= $openType;
 $obj['orderType']		= $orderType->notificationType;
 $obj['traderName'] 	= $traderName;

 }

  
//message = data.traderName + " " + data.openType + " " + data.orderSize + " Lot(s) of " + data.orderSymbol + " , " + " Opening time: " + data.openTime;


 } else if ($orderType->notificationType == "close") {

 $lastClosedTraderPosition = Traderhistory::find_the_last_closed_position_by_traderaccount_ids($traderaccount_idsArray);
 
 if ( $lastTraderPosition->traderaccount_id == $lastClosedTraderPosition->traderaccount_id ) {

 $traderAccount_id 		= $lastClosedTraderPosition->traderaccount_id;
 $traderAccount 		= Traderaccount::find_by_id($traderAccount_id);
 $traderName 			= $traderAccount->username;
 
 $obj['orderSymbol'] 		= $lastClosedTraderPosition->orderSymbol;
 $obj['closeTime']			= $lastClosedTraderPosition->closeTime;
 $obj['orderSize']			= $lastClosedTraderPosition->orderSize;
 $obj['orderType']			= $orderType->notificationType;
 $obj['traderName'] 		= $traderName;

 //message = data.traderName + " closed " + data.orderSize + " Lot(s) of " + data.orderSymbol + " , " + " Closing time: " + data.closeTime;


 }

 }

 } else if ($notificationObj->notificationSource == "investor") {

 if ($notificationObj->notificationType == "open") {

 $lastOpenUserPosition = Investorpositions::find_the_last_open_position_by_account_id($account_id);

 if ( $lastOpenUserPosition->statut == '1' && $lastOpenUserPosition->flagOpen == '1' && $lastOpenUserPosition->flagClose == '0' ) {

 $lastOpenUserPosition->openType == "0" ? $openType = "Bought" : $openType = "Sold";
 $orderType = "open";

 $traderAccount_id 		= $lastOpenUserPosition->traderaccount_id;
 $traderAccount 		= Traderaccount::find_by_id($traderAccount_id);
 $traderName 			= $traderAccount->username;



 $obj['orderSymbol'] 		= $lastOpenUserPosition->orderSymbol;
 $obj['openTime']			= $lastOpenUserPosition->openTime;
 $obj['orderSize']			= $lastOpenUserPosition->orderSize;
 $obj['openType']			= $openType;
 $obj['orderType'] 			= $orderType;
 $obj['traderName'] 		= $traderName;


 } 

 } else if ($notificationObj->notificationType == "close") {

 $lastCloseUserPosition = Investorhistory::find_the_last_closed_position_by_account_id($account_id);


 if ( $lastCloseUserPosition->flagOpen == '1' && $lastCloseUserPosition->flagClose == '1'  ) {

 $orderType = "close";


 $traderAccount_id 		= $lastCloseUserPosition->traderaccount_id;
 $traderAccount 		= Traderaccount::find_by_id($traderAccount_id);
 $traderName 			= $traderAccount->username;


 $obj['orderSymbol'] 		= $lastCloseUserPosition->orderSymbol;
 $obj['closeTime']			= $lastCloseUserPosition->closeTime;
 $obj['orderSize']			= $lastCloseUserPosition->orderSize;
 $obj['orderType']			= $orderType;
 $obj['profit']				= $lastCloseUserPosition->profit;
 $obj['profitPips']			= $lastCloseUserPosition->profitPips;
 $obj['traderName'] 		= $traderName;

 }

 }

 }

 header('Content-type: application/json');
 echo json_encode($obj);

 }
?>