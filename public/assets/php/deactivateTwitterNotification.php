<?php require_once("../../../includes/initialize.php");?>
<?php 

 
 if(isset($_POST["id"])){
 	
 $id 													= $db->escape_value($_POST["id"]);

 $user             										= User::find_by_id($id);

 $loginTwitter        									= LoginTwitter::find_by_id($id);
 $loginTwitter->notificationTw 	  						= 0;
 
 if ($user->type != 2) {
 $loginTwitter->twLogUsername						= "";
 $loginTwitter->twLogToken 							= "";
 $loginTwitter->twLogTokenSecret 					= "";
 }

 $obj = array();

 header('Content-type: application/json');

 if ($loginTwitter->update()) {
 unset($_SESSION['access_token_notification']);
 

 $obj['response'] = "success";
 } else {
 $obj['response'] = "failed";
 }

	
 echo json_encode($obj);

 
 }
?>