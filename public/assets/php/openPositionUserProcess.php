<?php require_once("../../../includes/initialize.php");?>
<?php 

 
 if(isset($_POST["id"])){
 	
 $id = $db->escape_value($_POST["id"]);

 $user              = User::find_by_id($id);	
 $userpositions 	= Investorpositions::find_all_open_position_by_account_id($id);

 $offset 			= offsetTime($user->timezone);

 for ($i=0; $i < count($userpositions) ; $i++) { 

 $timestamp = strtotime($userpositions[$i]->openTime);


 if ($user->isAutoTimezone) {	

 $newTimestamp 				  = $timestamp + ($user->detectOffsetTimezone * 3600);
 $newTime 					  = date('Y-m-d H:i:s', $newTimestamp);
 $userpositions[$i]->openTime = $newTime;
 
 } else {

 $newTimestamp   			  = $timestamp + $offset;
 $newTime        			  = date('Y-m-d H:i:s', $newTimestamp);
 $userpositions[$i]->openTime = $newTime;

 }
 
 }

 header('Content-type: application/json');
 $obj = array();
 $obj['data'] = $userpositions;

 echo json_encode($obj);

 
 }