<?php require_once("../../../includes/initialize.php");?>
<?php 

 if(isset($_POST["id"])){
 	
 $id   = $db->escape_value($_POST["id"]);
 $user = User::find_by_id($id);	

 $offset 			= offsetTime($user->timezone);


 $result_set_closed_position = $db->query("SELECT closeTime, profit FROM investorhistory WHERE account_id = {$id} AND flagOpen='1' AND flagClose='1' AND error = 0 ORDER BY closeTime ASC");
 
 header('Content-type: application/json');
 $dat = array();
 $objv = array();
 $obj = array();

 while ($userpositions = $db->fetch_array($result_set_closed_position)){
 $objv['profit'][] = (double)round($userpositions["profit"],2); 
 $timestamp = strtotime($userpositions["closeTime"]); 

 if ($user->isAutoTimezone) { 
 $newTimestamp 	   = $timestamp + ($user->detectOffsetTimezone * 3600);
 $objv['time'][]   = $newTimestamp * 1000;
 } else {
 $newTimestamp     = $timestamp + $offset;
 $objv['time'][]   = $newTimestamp * 1000;
 }
 

 }
 
 for ($i=0; $i < count($objv['profit']); $i++) {	
 $objv['profit'][$i] = $objv['profit'][$i-1] +$objv['profit'][$i];
 if ($i ==  (count($objv['profit']) - 1)){
 $dat[] 	= $objv['time'][$i];
 $dat[] 	= $objv['profit'][$i];
 
 }
 
 }
 
 echo json_encode($dat);
 
 }
?>