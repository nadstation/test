var TwitterEmail = function () {

    return {
        
        //Contact Form
        initTwitterEmail: function () {
	        // Validation
	        $("#twitterEmail").validate({
	            // Rules for form validation
	            
	            rules:
	            {
	            	lots:
	                {
	                    required: false,
	                    email: false
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: 'assets/php/twitterEmailProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(), //JSON.stringify(mydata),
        				success: function( data, textStatus, jQxhr )
						{
						//data - response from server
						
						if (data.response === 'success'){
						
						} else if (data.response === 'failed') {
						
						}
						}
	               		});
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
