var SettingForm = function () {

    return {
        
        //Contact Form
        initSettingForm: function () {
	        // Validation
	        $("#settings").validate({
	            // Rules for form validation
	            
	            
	            rules:
	            {
	            	lots:
	                {
	                    required: false,
	                    email: false
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                /*name:
	                {
	                    required: 'Please enter your name',
	                },
	                email:
	                {
	                    required: 'Please enter your email address',
	                    email: 'Please enter a VALID email address'
	                },
	                message:
	                {
	                    required: 'Please enter your message'
	                },
	                captcha:
	                {
	                    required: 'Please enter characters',
	                    remote: 'Correct captcha is required'
	                }*/
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: 'assets/php/settingsProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(), //JSON.stringify(mydata),
        				success: function( data, textStatus, jQxhr )
						{
						//data - response from server
						
						if (data.response === 'success'){
						toastr["success"]("Changed Saved!", "Tradengo");
						} else if (data.response === 'failed') {
						var array = data.list.rangeSize;
						if ( typeof data.list.rangeSize != 'undefined' ) {
						if ( data.list.rangeSize.length > 0 ) {

						textRangeSize = "";
						for (i = 0; i < data.list.rangeSize.length; i++) {
    					var textRangeSize = textRangeSize + data.list.rangeSize[i] + "<br>";
    					console.log(textRangeSize);
						}
						toastr["error"](textRangeSize, "Tradengo");
						
						}

						} else if ( typeof data.list.formatLot != 'undefined' ) {

						if ( data.list.formatLot.length > 0 ) {

						textFormatLot = "";
						for (i = 0; i < data.list.formatLot.length; i++) { 
						var textFormatLot = textFormatLot + data.list.formatLot[i] + "<br>";
						console.log(textFormatLot);
						}
						toastr["error"](textFormatLot, "Tradengo");

						}

						}
						
						}
						}
	               		});
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
