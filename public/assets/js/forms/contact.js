var ContactForm = function () {

    return {
        
        //Contact Form
        initContactForm: function () {
	        // Validation
	        $("#sky-form3").validate({
	            // Rules for form validation
	            
	            
	            rules:
	            {
	                name:
	                {
	                    required: true,
	                    minlength: 10,
	                    maxlength: 50
	                },
	                email:
	                {
	                    required: true,
	                    email: true,
	                    minlength: 10,
	                    maxlength: 50
	                },
	                message:
	                {
	                    required: true,
	                    minlength: 10,
	                    maxlength: 500
	                },
	                captcha:
	                {
	                    required: false,
	                    //remote: 'assets/plugins/sky-forms/version-2.0.1/captcha/process.php'
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                name:
	                {
	                    required: 'Please enter your name',
	                },
	                email:
	                {
	                    required: 'Please enter your email address',
	                    email: 'Please enter a VALID email address'
	                },
	                message:
	                {
	                    required: 'Please enter your message'
	                },
	                captcha:
	                {
	                    required: 'Please enter characters',
	                    remote: 'Correct captcha is required'
	                }
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: 'assets/php/sky-forms-pro/contactProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(),
        				success: function( data, textStatus, jQxhr )
						{
						//data - response from server
						if (data.response === 'success'){
                        console.log(data.response);
						$("#sky-form3").addClass('submited');
						} else {
							var datas;
							var name ="";
							var email ="";
							var message ="";
							var errors = {};
							
							if (typeof data.list.name === 'string') {
							errors[0] = data.list.name ;
							} 
							if (typeof data.list.email === 'string') {
							errors[1] = data.list.email ;	
							} 
							if (typeof data.list.message === 'string') {
							errors[2] = data.list.message ;	
							}
							datas = '';
							for (var key in errors) {
        					datas += errors[key]+"\n";
        					}
        					alert(data.response+"\n" + datas);
							
						} 
						
						}
	                   
	                     
					
	                   
	                });
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
