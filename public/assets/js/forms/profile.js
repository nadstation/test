var progressbox     = $('.progress');
var progressbar     = $('.progress-bar');
//var statustxt       = $('#statustxt');
var completed       = '0%';

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

function LoadImage(id,link) {
var img = new Image(),
x = document.getElementById(id);
/*img.onload = function() {
x.src = img.src;
};*/
$( img ).load( function() {
  x.src = img.src;
});
img.src = link;
}



var ProfileForm = function () {

    return {
    	
        
        //Contact Form
        initProfileForm: function () {
	        // Validation
	        $("#MyUploadForm").validate({
	            // Rules for form validation
	            
	            
	            rules:
	            {
	            	email:
	                {
	                    required: false,
	                    email: false
	                },
	                password:
	                {
	                    required: false,
	                    email: false
	                    
	                } 
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: 'assets/php/profileProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				beforeSubmit:  function (){
    				   //check whether browser fully supports all File API
					   if (window.File && window.FileReader && window.FileList && window.Blob)
						{
					
							if( $('#imageInput').val()) //check empty input filed
							{
							
							var fsize = $('#imageInput')[0].files[0].size; //get file size
							var ftype = $('#imageInput')[0].files[0].type; // get file type
							
							//allow only valid image file types 
							switch(ftype)
					        {
					            case 'image/png': case 'image/jpeg': 
					                break;
					            default:
					                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
									return false
					        }
							
							//Allowed file size is less than 1 MB (1048576)
							if(fsize>1048576) 
							{
								$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo using an image editor.");
								return false
							}
							
							//Progress bar
							progressbox.show(); //show progressbar
							progressbar.width(completed); //initial value 0% of progressbar
							//statustxt.html(completed); //set status text
							//statustxt.css('color','#000'); //initial color of status text
					
									
							$('#profileButton').hide(); //hide submit button
							//$('#loading-img').show(); //hide submit button
							$("#output").html("");  
							} else {
        					$("#output").html("There is no picture attached ");
							return true;
							}
						
						}
						else
						{
							//Output error to older unsupported browsers that doesn't support HTML5 File API
							$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
							return false;
						}
					},  // pre-submit callback 
						uploadProgress: function (event, position, total, percentComplete)
					{
						//Progress bar
						progressbar.width(percentComplete + '%') //update progressbar percent complete
						//statustxt.html(percentComplete + '%'); //update status text
						if(percentComplete == 100)
							{
								setTimeout(function(){ 
								progressbox.hide();
								progressbar.width(0 + '%'); 
								}, 1000);
								
								
								//statustxt.css('color','#fff'); //change status text to white after 50%
							}
					},
						contentType: 'multipart/form-data',
        				data: $('#MyUploadForm').serialize(),
        				success:       function( data, textStatus, jQxhr )
						{
							
							
							if (data.responsePicture == "success"){
														
							
    						idLive  	  = "livePicture";
    						idSide  	  = "sidePicture";
    						idJumboPic    = "jumboPicture";
    						var link 	  = data.link;
    						var linkThumb = data.linkThumb;
    						if (data.type == "2"){ // in the case of twitter user
							var res = link.replace("normal", "bigger"); 
    						} else {
    						var res = link;	
    						}
    						//attach pictures
    						LoadImage(idLive,res);
    						LoadImage(idSide,linkThumb);
    						LoadImage(idJumboPic,res);
							//$('#jumboPicture').attr("src",res);


							toastr["success"]("Picture uploaded successfully", "Tradengo");

							setTimeout( function() { $('#profileButton').show();} , 1000 );
							
							
							} else if (data.responsePicture == "failed"){

							$("#output").html(data.pictureList);  
							toastr["error"](data.pictureList, "Tradengo");
								
							}
							
							if (data.responseText == "success") {
							toastr["success"]("General Information saved", "Tradengo");
							} else if (data.responseText == "failed") {
							toastr["error"](data.list.quote, "Tradengo");
							} 
							//else if (data.responseText == "warning") {
							//toastr["warning"]("Information: No change", "Tradengo");
							//} 
							},
        				//resetForm: true,
						error: function ( jQxhr, status, strErr ) {
            			console.log(jQxhr.responseText);
                		console.log(status);
                		console.log(strErr);
						setTimeout( function() { $('#profileButton').show();} , 1000 );

        				}
						
						
	                   
	                     
					
	                   
	                });
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
