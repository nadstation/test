var AdminLoginForm = function () {

    return {
        
        //Contact Form
        initAdminLoginForm: function () {
	        // Validation
	        $("#admin").validate({
	            // Rules for form validation
	            
	            
	            rules:
	            {
	                name:
	                {
	                    required: false,
	                    email: false
	                },
	                email:
	                {
	                  	required: false,
	                    email: false
	                },
	                message:
	                {
	                    required: false,
	                    email: false
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                name:
	                {
	                    
	                },
	                email:
	                {
	                    
	                },
	                message:
	                {
	                    
	                }
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: '../assets/php/adminLoginProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(),
        				success: function( data, textStatus, jQxhr )
						{
						//data - response from server
						if (data.response == 'success'){
						
						window.location.replace("http://www.tradengo.co/admin/administration.php");
                        
						} else if (data.response == 'failed') {
							
						
							
						}
						
						}
	                   
	                     
					
	                   
	                });
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
