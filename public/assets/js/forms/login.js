function removeErrorSignIn() {
$("#form_login").find('input[name="email"]').remove('invalid').nextAll('span').text('');
$("#form_login").find('input[name="password"]').remove('invalid').nextAll('span').text('');
$("#resultLogin").text('');

//$("#form_signup").find('input[name="email"]').removeClass('error-message').next('span').html('').removeClass('is-visible');
//$("#form_signup").find('input[name="password"]').removeClass('error-message').next('span').html('').removeClass('is-visible');

}



var loginForm = function () {

    return {
        
        //Masking
        initLoginForm: function () {
	        // Validation for login form
	        $("#form_login").validate({
	            // Rules for form validation
	            rules:
	            {
	                email:
	                {
	                    required: false,
	                    email: false
	                },
	                password:
	                {
	                    required: false,
	                    email: false
	                    
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                /*email:
	                {
	                    required: 'Please enter your email address',
	                    email: 'Please enter a VALID email address'
	                },
	                password:
	                {
	                    required: 'Please enter your password'
	                }*/
	            }, 
	            
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                	beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                    
	                    url: 'assets/php/sky-forms-pro/loginProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(),
        				success: function( data, textStatus, jQxhr )
						{
							
						if (data.response === 'success'){
						console.log(data.response);
						
						/*$form_login.find('input[name="email"]').removeClass('has-error').next('span').removeClass('is-visible');
						$form_login.find('input[name="password"]').removeClass('has-error').next('a').next('span').removeClass('is-visible');*/

						$("#form_login").find('input[name="email"]').removeClass('invalid');
						$("#form_login").find('input[name="password"]').removeClass('invalid');

						
						window.location.replace("https://www.tradengo.co/admin.php");

						
						} else if (data.response === 'failed'){
						console.log(data.response);
						removeErrorSignIn();

						$("#resultLogin").html("Account / Password incorrect. Please try again").css({"color": "red"});

						
						} else if (data.response === 'error')   {
						console.log(data.response);
						removeErrorSignIn();
						
						
						if (typeof data.list.emailPresence === 'string') {
						
						$("#form_login").find('input[name="email"]').addClass('invalid');
						$("#form_login").find('span[name="email"]').html(data.list.emailPresence).css({"color": "red"});
						
						} else if (typeof data.list.email === 'string') {
						
						$("#form_login").find('input[name="email"]').addClass('invalid');
						$("#form_login").find('span[name="email"]').html(data.list.email).css({"color": "red"});
							
						}
						
						if (typeof data.list.passwordPresence === 'string') {
						
						$("#form_login").find('input[name="password"]').addClass('invalid');
						$("#form_login").find('span[name="password"]').html(data.list.passwordPresence).css({"color": "red"});
						
						} else if (typeof data.list.password === 'string') {
						
						$("#form_login").find('input[name="password"]').addClass('invalid');
						$("#form_login").find('span[name="password"]').html(data.list.password).css({"color": "red"});
							
						}
						
						}
						
						}
												
						
	                	
	                	
	                });               
	            },
	            
	                             
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
	        
	        
        }

    };

}();