var AccountInfoForm = function () {

    return {
        
        //Contact Form
        initAccountInfoForm: function () {
	        // Validation
	        $("#accountInfo").validate({
	            // Rules for form validation
	            
	            
	            rules:
	            {
	            	/*email:
	                {
	                    required: false,
	                    email: false
	                },
	                password:
	                {
	                    required: false,
	                    email: false
	                    
	                }*/
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                /*name:
	                {
	                    required: 'Please enter your name',
	                },
	                email:
	                {
	                    required: 'Please enter your email address',
	                    email: 'Please enter a VALID email address'
	                },
	                message:
	                {
	                    required: 'Please enter your message'
	                },
	                captcha:
	                {
	                    required: 'Please enter characters',
	                    remote: 'Correct captcha is required'
	                }*/
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form, event)
	            {
	            	event.preventDefault();
	                $(form).ajaxSubmit(
	                {
	                    beforeSend: function()
	                    {
	                       // $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                   
	                    url: 'assets/php/accountProcess.php',
	                    dataType: 'json',
        				type: 'post',
        				contentType: 'application/x-www-form-urlencoded',
        				data: $(form).serialize(), //JSON.stringify(mydata),
        				success: function( data, textStatus, jQxhr )
						{
						//data - response from server
						if (data.response === 'success'){
						toastr["success"]("Changed Saved!", "Tradengo");

       					$(document).on("click", "#back", function(){
    					console.log("click simulated");
						});

       					if (isBackButtonVisible) {
    					$("#back").trigger("click");
    					}

    					graphUser 		= 1;
    					graphListTrader = 1;




  						if (data.ref == "") {

  						} else if (data.ref == "email") {
  					  	$("input[name*=email]").val("");
    			  		$("input[name*=email]").attr("class", 'form-control');
  						$("input[name*=email]").attr("placeholder", data.email);

  						} else if (data.ref == "username") {
  			  			$("input[name*=username]").val("");
  			  			$("input[name*=username]").attr("class", 'form-control');
  						$("input[name*=username]").attr("placeholder", data.username);

  						} else if (data.ref == "emailUsername") {
    					$("input[name*=email]").val("");
    					$("input[name*=email]").attr("class", 'form-control');
    					$("input[name*=email]").attr("placeholder", data.email);

    					$("input[name*=username]").val("");
    					$("input[name*=username]").attr("class", 'form-control');
  						$("input[name*=username]").attr("placeholder", data.username);

  						} else if (data.ref == "passwordUsername") {
    					$("input[name*=username]").val("");
    					$("input[name*=username]").attr("class", 'form-control');
  						$("input[name*=username]").attr("placeholder", data.username);

  						$("input[name*=password]").val("");
    					$("input[name*=password]").attr("class", 'form-control');

    					$("input[name*=passwordAgain]").val("");
    					$("input[name*=passwordAgain]").attr("class", 'form-control');

  						} else if (data.ref == "passwordEmail") {
    					$("input[name*=email]").val("");
    					$("input[name*=email]").attr("class", 'form-control');
  						$("input[name*=email]").attr("placeholder", data.email);

  						$("input[name*=password]").val("");
    					$("input[name*=password]").attr("class", 'form-control');

    					$("input[name*=passwordAgain]").val("");
    					$("input[name*=passwordAgain]").attr("class", 'form-control');

  						} else if (data.ref == "password") {

  						$("input[name*=password]").val("");
    					$("input[name*=password]").attr("class", 'form-control');

    					$("input[name*=passwordAgain]").val("");
    					$("input[name*=passwordAgain]").attr("class", 'form-control');

  						}

						} else if (data.response === 'error') {
						
						var datas;
						var email ="";
						var passwordMatch ="";
						var password ="";
						var errors = {};
						
						
						if (typeof data.list.email === 'string') {
						errors[0] = data.list.email ;	
						} 
						if (typeof data.list.passwordMatch === 'string') {
						errors[1] = data.list.passwordMatch ;	
						}	
						if (typeof data.list.password === 'string') {
						errors[2] = data.list.password ;	
						}
						
						datas = '';
						for (var key in errors) {
        				datas += errors[key]+"\n";
        				}
        				toastr["error"](datas, "Tradengo")

        				console.log(datas);

						} else if (data.response === 'emailExist') {
						toastr["warning"]("This email address is already used", "Tradengo")

						} else if (data.response === 'usernameExist') {
						toastr["warning"]("This username is already used", "Tradengo")
						}
					    
						console.log(data);
                        console.log(textStatus);
                        console.log(jQxhr);
                        
						
						},
						error: function ( jQxhr, status, strErr ) {
            			console.log(jQxhr.responseText);
                		console.log(status);
                		console.log(strErr);
        				}
	                });
	               
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();

						
	                    
	                    
