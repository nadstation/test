<?php
//require_once("initialize.php");


function redirect_to($new_location) {
header("Location: " . $new_location);
exit;
}



function password_check($password, $existing_hash){
//existing hash contains format and salt at start
/*$hash = crypt($password, $existing_hash);
if ($hash === $existing_hash){
	return true; 
} else {
	return false ;
}*/

$hashed_password = sha1($password);
if ($hashed_password === $existing_hash){
	return true; 
} else {
	return false ;
}

}


function attempt_login($email, $password){
//2 steps process: 
//find the user
//compare the existing hash ($existing_hash) in the database against the password ($password)
//$user = find_user_by_email($email);
$user = User::find_by_email($email);
if ($user){
	//found admin, now check password
	if (password_check($password, $user->hashed_password)) {
	// password matches	
	return $user;
	} else {
	// password does not match
	return false;
	}
} else {
	//admin not found
	return false;
}
	
}



function formatOffset($offset) {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);

        if ($hour == 0 AND $minutes == 0) {
            $sign = ' ';
        }
        return $sign . str_pad($hour, 2, '0', STR_PAD_LEFT) .':'. str_pad($minutes,2, '0');
}




/*function __autoload($class_name){
		$class_name = strtolower($class_name);
		$path = LIB_PATH.DS."{$class_name}.php";
		if(file_exists($path)){
			require_once($path);
		} else {
			die("The file {$class_name}.php could not be found.");
		}
		
	}*/

function output_message($message=''){
		if (!empty($message)) {
		return "<p class=\"message\">" . $message . "<p/>";
		} else {
		return "";
		}
	}
	

/*function password_encrypt($password) {
	$hash_format = "$2y$10$"; // Tells PHP to use Blowfish with a "cost" of 10
	$salt_length = 22;  // Blowfish salts should be 22-Characters or more.
	$salt = generate_salt($salt_length);
	$format_and_salt = $hash_format . $salt;
	$hash = crypt($password, $format_and_salt);
	return $hash;
}

function generate_salt($length) {
//NOT 100% unique, not 100% random, but good enough for a salt
//MD5 returns 32 characters
$unique_random_string = md5(uniqid(mt_rand(), true));

//Valid characters for a salt are [a-zA-Z0-9./]
$base64_string = base64_encode($unique_random_string);

//But not '+' which is valid in base64 encoding
$modified_base64_string = str_replace('+', '.', $base64_string);

//Truncate string to the correct length
$salt = substr($modified_base64_string, 0,$length );


return $salt;

}*/

















?>