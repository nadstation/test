/*
*
*  Push Notifications codelab
*  Copyright 2015 Google Inc. All rights reserved.
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      https://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License
*
*/

// Version 0.1

//var id;
function json(response) {  
  return response.json()  
}

console.log('Started', self);
self.addEventListener('install', function(event) {
self.skipWaiting();
console.log('Installed', event);
});
self.addEventListener('activate', function(event) {
console.log('Activated', event);
});

/*self.addEventListener('message', function(event){
    var data = JSON.parse(event.data);


    console.log("SW Received Message: id=" + data.id );
    //console.log(data);

    self.id = data.id;
    console.log(self.id);
    
});*/



  self.registration.pushManager.getSubscription().then(subscription => {
  // Do something with subscription.endpoint

  console.log(subscription);
  console.log(subscription.endpoint);

  var str = subscription.endpoint;

  console.log(str);


  var a = str.search("mozilla");
  var b = str.search("android");

  console.log("Value of a:"+a);
  console.log("Value of b:"+b);

  if (a >= 0) {
  self.subscriptionId = str.replace('https://updates.push.services.mozilla.com/wpush/v1/','');
  console.log("a >= 0:"+self.subscriptionId);
  } else if (b >= 0) {
  self.subscriptionId = str.replace('https://android.googleapis.com/gcm/send/','');
  console.log("b >= 0:"+self.subscriptionId);
  }

  console.log("after condition:"+self.subscriptionId);

  });


self.addEventListener('push', function(event) {  
  // Since there is no payload data with the first version  
  // of push messages, we'll grab some data from  
  // an API and use it to populate a notification  
    //console.log(id);

    if (!(self.Notification && self.Notification.permission === 'granted')) {
        console.log("Denied");
        return;
    }

    

    event.waitUntil(  
    fetch('/assets/php/getDataForPushNotification.php',{
    method: 'post',
    headers: {  
      "Content-type": "application/x-www-form-urlencoded"  
    },  
    body: 'subscriptionId=' + self.subscriptionId //+ '&test='+ 'ttt'
    }).then(function(response) {  
      if (response.status !== 200) {  
        console.log(self.subscriptionId);
        // Either show a message to the user explaining the error  
        // or enter a generic message and handle the
        // onnotificationclick event to direct the user to a web page  
        console.log('Looks like there was a problem. Status Code: ' + response.status);  
        throw new Error();  
      } else {
      console.log("== 200:"+self.subscriptionId);
      }



      // Examine the text in the response  
        return response.json().then(function(data) {  

        //console.log(data.test);


        /*if (data.error || !data.notification) {  
          console.log('The API returned an error.', data.error);  
          throw new Error(); 
        }  */

        //console.log(data.orderSymbol);

        var title = "TradeNgo";

        console.log(data);
        

        if ( data.orderType == "open" ) {

        message = data.traderName + " " + data.openType + " " + data.orderSize + " Lot(s) of " + data.orderSymbol + " , " + " Opening time: " + data.openTime;
        
        } else if (data.orderType == "close") {

        message = data.traderName + " closed " + data.orderSize + " Lot(s) of " + data.orderSymbol + " , " + " Closing time: " + data.closeTime;

        }


        var icon = "assets/images/logo.png";  
        var notificationTag = "";

        return self.registration.showNotification(title, {
          body: message,  
          icon: icon,  
          tag: notificationTag  
        });  
      });  
    }).catch(function(err) {  
      console.log('Unable to retrieve data', err);

      var title = 'TradeNgo';
      var message = 'You received a notification ';  
      var icon = "assets/images/logo.png"; 
      var notificationTag = 'notification-error';  
      return self.registration.showNotification(title, {  
          body: message,  
          icon: icon,  
          tag: notificationTag  
        });  
    })  
  );  
});




// TODO

