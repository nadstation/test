<?php require_once("../../includes/initialize.php"); ?>

<?php
$session = new Session(); 

require_once __DIR__ . '/src/Facebook/autoload.php';

$fb = new Facebook\Facebook([
  'app_id' => '1608981019406098',
  'app_secret' => '76afb5ad4ba91ab6b259e22d46d72748',
  'default_graph_version' => 'v2.8',
  ]);

$helper = $fb->getRedirectLoginHelper();


	
try {
	if (isset($_SESSION['facebook_access_token'])) {
		$accessToken = $_SESSION['facebook_access_token']; 
	} else {
  		$accessToken = $helper->getAccessToken();		   
	}
} catch(Facebook\Exceptions\FacebookResponseException $e) {
 	// When Graph returns an error
 	echo 'Graph returned an error: ' . $e->getMessage();
  	exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
 	// When validation fails or other local issues
	echo 'Facebook SDK returned an error: ' . $e->getMessage();
  	exit;
 }

if (isset($accessToken)) {
	if (isset($_SESSION['facebook_access_token'])) {
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']); /******/
	} else {
		// getting short-lived access token
		$_SESSION['facebook_access_token'] = (string) $accessToken;

	  	// OAuth 2.0 client handler
		$oAuth2Client = $fb->getOAuth2Client();

		// Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);

		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;

		// setting default access token to be used in script
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);/*****/
	}

	// getting basic info about user
	try {
		$profile_request = $fb->get('/me');
		$profile = $profile_request->getGraphNode()->asArray();
		//$graphNode = $profile_request->getGraphNode();
		

	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		session_destroy();
		// redirecting user back to app login page
		//header("Location: ./");
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}

	if (isset($_GET["signup"])) { 					/****signup****/
	$isAccountExist = LoginFacebook::find_by_fbId($profile["id"]);
	if ($isAccountExist) {
	//Account alreaddy exist
	redirect_to('https://www.tradengo.co/index.php?signup=1');	
	} else {
	$user = new User;
	if ($user->create()) {
	$id = $user->id;
	$userCreated = User::find_by_id($id);
	$userCreated->type = "3";
	$userCreated->update();

	$loginFacebook = loginFacebook::find_by_id($id);
	$loginFacebook->fbName 					= $profile["name"];
	$loginFacebook->fbId 					= $profile["id"];
	$loginFacebook->fbLogToken 				= $accessToken;
	$dataPicture 							= "https://graph.facebook.com/" . $profile["id"] . "/picture?type=large";
	$loginFacebook->fbProfilePicture		= $dataPicture;
	$loginFacebook->update();

	$loginFacebook->defaultConfig($loginFacebook->id);

	unset($_SESSION['facebook_access_token']);
	$found_account 		 = User::find_by_id($id);

	if ($found_account) {
	$session->logout();
	$session->login($found_account);
	redirect_to('https://www.tradengo.co/admin.php');
	}
	}
	}
	} else if (isset($_GET["signin"])) { 			/****login****/
	$fbId = $profile["id"];
	unset($_SESSION['facebook_access_token']);
	
	if ($loginFacebook = LoginFacebook::find_by_fbId($fbId)) {
	$session->logout();
	$session->login($loginFacebook);
	$loginFacebook->fbLogToken 		  = $accessToken;
	$loginFacebook->update();


	redirect_to('https://www.tradengo.co/admin.php');
	
	} else {
	redirect_to('https://www.tradengo.co/index.php?signin=1');
	}

	} else if (isset($_GET["notification"])) { 		/****notification****/
	
	$id 							  = $session->user_id;	
	$loginFacebook     				  = LoginFacebook::find_by_id($id);
	$loginFacebook->notificationFb 	  = 1;

	$user = new User;

	if ($user->type != 3 ) {
	$loginFacebook->fbLogToken 		  = $accessToken;
	$loginFacebook->fbName	  		  = $profile["name"];
	$loginFacebook->fbId 	  	  	  = $profile["id"];
	} 

	$loginFacebook->update();

	
	redirect_to('https://www.tradengo.co/admin.php');

	
	

	}



	
  	// Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
  	
} else {
	// replace your website URL same as added in the developers.facebook.com/apps e.g. if you used http instead of https and you used non-www version or www version of your website then you must add the same here
	//$loginUrl = $helper->getLoginUrl('https://www.tradengo.co/test6.php', $permissions);
	//echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';
	redirect_to('https://www.tradengo.co/index.php');
}



