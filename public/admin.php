<?php require_once('../includes/initialize.php'); ?>
<?php $max_file_size = 1048576; ?>
<?php $session = new Session(); ?>
<?php 
//phpinfo();
if(!$session->is_logged_in()){
redirect_to("index.php");
} 
?>
<?php $id = $session->user_id; ?>
<script>
var idUser = <?php echo $id; ?> ;
</script>
<?php  

$link             = Userlink::find_by_id($id);
$user             = User::find_by_id($id);

$loginInvestor    = LoginInvestor::find_by_id($id);
$loginTwitter     = LoginTwitter::find_by_id($id);
$loginFacebook    = LoginFacebook::find_by_id($id);
$photo            = Photograph::find_by_id($id);
$info             = Info::find_by_id($id);
$notification     = NotificationObj::find_by_id($id);
$browser          = Browser::find_by_id($id);
$browsername      = $browser->get_browser_name($_SERVER['HTTP_USER_AGENT']);
$browser->browsername = $browsername;
$browser->update();
?>

<?php
$date           = DateTime::createFromFormat("Y-m-d", $user->age);
$arr            = array();

?>

<!DOCTYPE html>
<html class="sb-init">
    <head>
        <title>Admin</title>
        <link rel="manifest" href="manifest.json">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
      <link rel="stylesheet" href="assets/css/bootstrap.min.css" >
      <link rel="stylesheet" href="assets/css/mdb.css" >
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.jqueryui.min.css" >
      <link rel="stylesheet" href="assets/css/dataTables.bootstrap.min.css">
      <link rel="stylesheet" href="assets/css/dataTables.jqueryui.min.css">
      <link rel="stylesheet" href="assets/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/css/plugins/style.css" >
      <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    
    <body >
    <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav admin-side-nav dark-side-nav">
        <!-- Logo -->
        <div class="logo-wrapper">

            <?php if ($user->type == "1") {
                  if ( $photo->filename == "" ) { 
                  $sidePicture = "assets/images/profil.jpg";
                  } else { 
                  $sidePicture = "images/".$loginInvestor->username."/".$photo->filenamethumb;
                  } 
                  } else if ($user->type == "2") {
                  $sidePicture = $loginTwitter->twProfilePicture;
                  } else if ($user->type == "3") { 
                  $sidePicture = $loginFacebook->fbProfilePicture;
                  } ?>
            
            <img id="sidePicture" style="height: 90px; width: 100px;" src=<?php echo $sidePicture; ?> class="img-responsive img-circle">
            <div class="rgba-stylish-strong">
            <?php if ($user->type == 1) {
                  $sideName = $loginInvestor->username;
                  $secondLine = $loginInvestor->email;
                  } else if ($user->type == 2) {
                  $sideName = "@" . $loginTwitter->twLogUsername;
                  $twEmail = $secondLine;
                  if ($twEmail == ''){
                  $secondLine = "Email via 'User Account'";
                  } else {
                  $secondLine = $loginInvestor->email;
                  }
                  
                  } else if ($user->type == 3) { 
                  $sideName = $loginFacebook->fbName;
                  $secondLine = $loginInvestor->email;
                  } ?>

                <p class="user white-text"><?php echo $sideName; ?>
                    <br> <?php echo $secondLine; ?>
                </p>


            </div>
        </div>
        <!--/. Logo -->

        <!-- Side navigation links -->
        <ul class="collapsible collapsible-accordion">
            <li><a href="#user" class="waves-effect waves-light"><i class="fa fa-user fa-lg"></i> User Account</a></li>
            <li><a href="#profile" class="waves-effect waves-light"><i class="fa fa-check-square fa-lg"></i> Profile Information</a></li>
            <li><a href="#link" class="waves-effect waves-light"><i class="fa fa-power-off fa-lg"></i> Account Link (live)</a></li>
            <li><a href="#setting" class="waves-effect waves-light"><i class="fa fa-cog fa-lg"></i> Setting</a></li>
            <li><a href="#notification" class="waves-effect waves-light"><i class="fa fa-exclamation-circle fa-lg"></i></i> Notification</a></li>
            <li><a href="#social" class="waves-effect waves-light"><i class="fa fa-globe fa-lg"></i> Social links</a></li>
            <li><a href="logout.php" class="waves-effect waves-light"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>

        </ul>
        <!--/. Side navigation links -->

    </ul>
    <!--/. Sidebar navigation -->
        
    <!--Navbar-->
    <nav class="double-navbar navbar navbar-fixed-top z-depth-1" role="navigation">
        <div class="container-fluid" style="position:relative;">
            <div class="navbar-header pull-left">
                <!-- SideNav slide-out button -->
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
                <!--/. SideNav slide-out button -->
            </div>

            <a style="position: absolute; left:100px; display: none;" class="btn-floating btn-large waves-effect waves-light orange traderList" id="back"><i class="fa fa-arrow-left left"></i></a>
         

            <div style="position: absolute; left:600px;">
            <div class="navbar-brand" ><img alt="TradeNgo" src="assets/images/logo.png" style="width: 50px;"></div>
            <div class="navbar-brand" style="padding-left: 0px;padding-top: 18px"><span style="color:#000">Trade</span><span style="color:#ff7200">Ngo</span><span style="color:#000">.co</span></div>
            </div>


            <!-- Navbar Icons -->
            <ul class="list-inline pull-right text-center">

                <!--<li><img alt="TradeNgo" src="assets/images/logo.png" style="width: 60px; margin-right: 230px;"></li>-->
            <li><a href="#activity" class="waves-effect waves-light" ><i class="fa fa-rss"></i><br><span class="label-navbar-activity">Live Activity Feed</span></a></li>
            <li><a href="#dashboard" class="waves-effect waves-light" ><i class="fa fa-tachometer"></i><br><span class="label-navbar-dashboard">Dashboard</span></a></li>
            <li><a href="#system" class="waves-effect waves-light" ><i class="fa fa-list-ul"></i><br><span class="label-navbar-trader">Trader</span></a></li>
                <!--<li><a href="#" class="waves-effect waves-light" ><i class="fa fa-envelope"></i><br><span>Contact</span></a></li>
                <li><a href="logout.php" class="waves-effect waves-light" ><i class="fa fa-sign-out" ></i><br><span>Logout</span></a></li>-->
                

            </ul>
            <!--/. Navbar Icons -->
        </div>
    </nav>
    <!--/.Navbar-->
        
    <div id="sb-site">

    <!-- Tab panes -->
    
    <div class="container" >
  
    <div class="tab-content">

    <div id="activity" class="tab-pane fade in active" >

    
    <!-- Modal -->
    <div class="modal fade" id="infoNotif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">TradeNgo Notification</h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                Your are using another web browser from the previous one! <br>
                Which browser would you like to receive your notification ?
                </div>
                <!--Footer-->
                <div class="">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" >Previous one</button>
                    <button type="button" class="btn btn-primary" id="resendNotification" >This one</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- /.Live preview-->

        
    <div class="container-fluid">
    
    <ul class="collection">
    <?php 
    //$investorPositionsHistory  = $db->query("SELECT * FROM investorhistory 
    //WHERE flagOpen  ='1' AND flagClose = '1' OR flagManualClose = '1' ORDER BY closeTime DESC LIMIT 30");
    $investorHistory = new InvestorHistory;
    $positions = $investorHistory->find_all_the_last_30_closed_position();
    for ($i=0; $i < count($positions) ; $i++) { 
    //while($investorPositions     = $db->fetch_array($investorPositionsHistory)){
    $account_id                  = $positions[$i]->account_id;
    $userTypeActivity            = User::find_by_id($account_id);
    

    if ($userTypeActivity->type  == 1) {
    $userActivity                = LoginInvestor::find_by_id($account_id);
    $photoActivity               = Photograph::find_by_id($account_id);
    $source                      = "images/".$userActivity->username."/".$photoActivity->filenamethumb;
    } else if ($userTypeActivity->type == 2) {
    $userActivity                = LoginTwitter::find_by_id($account_id);
    $source                      = $userActivity->twProfilePicture;
    } else if ($userTypeActivity->type == 3) {
    $userActivity                = loginFacebook::find_by_id($account_id);
    $source                      = $userActivity->fbProfilePicture;
    }

    $photoActivity               = Photograph::find_by_id($account_id);
    $closeTimeActivity           = $positions[$i]->closeTime;
    $closePriceActivity          = $positions[$i]->closePrice;
    $orderSymbolActivity         = $positions[$i]->orderSymbol;
    $profitActivity              = $positions[$i]->profitPips;
    $orderNumberActivity         = $positions[$i]->orderNumber; 
    ?>


    
    <li class="collection-item avatar">
        <div id="orderNumberActivity" style="display: none;"><?php echo $orderNumberActivity; ?></div>
        <?php
        $arr[] = $orderNumberActivity;
        ?>
        
        <img id="activityPicture" src="<?php echo $source; ?>" alt="" class="circle" style="height:42px" class="circle"/>
        

        <span class="title">
        <?php echo $userActivity->username; ?></span>
        <p>Closed a <?php echo $orderSymbolActivity; ?> position at <?php echo $closePriceActivity; ?> <?php echo " " . $profitActivity > 0  ? 'Winning' : 'Losing'; ?> <?php echo $profitActivity; ?> Pips
        </p>
        <a class="secondary-content"><i class="fa fa-star fa-lg"></i></a>
    </li>

    <?php 
    }
    //} 
    ?>

    </ul>
    </div>

    </div>
    
    
    
    
  
    <div id="dashboard" class="tab-pane fade" style="height: 1700px;">
    
    
    <!--new dashboard-->

    <div class="user-detail" >

    

    <div class="account-details">

    <div class="leftside">
    </div>

    <div class= "avatar-frame">
    <?php if ($user->type == "1") {
          if ( $photo->filename == "" ) { 
          $jumboPicture = "assets/images/profil.jpg";
          } else { 
          $jumboPicture = "images/".$loginInvestor->username."/".$photo->filename;
          } 
          } else if ($user->type == "2") {
          $jumboPicture = $loginTwitter->twProfilePicture;
          $search = '_normal' ;
          $jumboPicture = str_replace($search, '', $jumboPicture) ;
          } else if ($user->type == "3") { 
          $jumboPicture = $loginFacebook->fbProfilePicture;
    } ?>

    <img id = "jumboPicture" src=<?php echo $jumboPicture; ?> class="img-responsive img-circle">
    </div>

    
    <div class= "user-name">
    <p>Nadstation</p>
    </div>

    <div class="account-selector">
    </div>

    </div><!--end of account-details -->

    <div class="stats-dashboard">
    <div class="equity">
    <p>EQUITY</p>
    </div>
    <div class="label-equity">
    <p>&euro;2000</p>
    </div>

    <div class="balance">
    <p>BALANCE</p>
    </div>

    <div class="label-balance">
    <p>&euro;2000</p>
    </div>

    <div class="PNL">
    <p>PNL</p>
    </div>
    
    <div class="label-PNL">
    <p>&euro;2000</p>
    </div>

    <div class="rightside">
    </div>

    </div>

    </div><!--end of user-detail -->


    <!--******-->




    <br><br>
    
    
    <div style="height: 460px; width: 920px; ">

    <ul class="nav nav-tabs">
    <li id="performance" class="active"><a href="#PnLPerformance" aria-controls="PnLPerformance" data-toggle="tab"><i class="fa fa-money fa-lg"></i> Profit</a></li>
    <li id="performance" ><a href="#pipsPerformance" aria-controls="pipsPerformance" data-toggle="tab"><i class="fa fa-pied-piper fa-lg"></i> Profit Pips</a></li>
    </ul>
  <br/>

  <div class="panel panel-default">
  <div class="panel-body z-depth-2">

    <div class="tab-content">
  <div role="tabpanel" class="tab-pane fade in active" id="PnLPerformance">
  <div id="performancePnl" style="height: 300px;width: 860px; "></div>  
    </div>
  <div role="tabpanel" class="tab-pane fade" id="pipsPerformance">
  <div id="performancePips" style="height: 300px;width: 860px; "></div>
    </div>
    </div>

  </div>
  </div>

    </div>


    <div style="height: 650px; width: 920px; ">

    <ul class="nav nav-tabs">
    <li id="positions" class="active"><a href="#tradingHistory" aria-controls="tradingHistory" data-toggle="tab"><i class="fa fa-history fa-lg"></i> Trading History</a></li>
    <li id="positions" ><a href="#openPositions" aria-controls="openPositions" data-toggle="tab"><i class="fa fa-ellipsis-h fa-lg"></i> Open Positions</a></li>
    </ul>
  <br/>

  <div class="panel panel-default">
  <div class="panel-body z-depth-2">

    <div class="tab-content">
  <div role="tabpanel" class="tab-pane fade in active" id="tradingHistory" style="width: 880px;">
  <table id="historic" class="dataTable display compact" cellspacing="0" width="100%"  >
      <thead>
          <tr>
              <th>Order Number</th>
              <th>Open Time</th>
              <th>Order Type</th>
              <th>Order Size</th>
              <th>Order Symbol</th>
              <th>Open Price</th>
              <th>Close Time</th>
              <th>Close Price</th>
              <th>Profit</th>
              <th>Profit Pips</th>
              <th>Trader</th>
        </tr>
      </thead>
  </table>  
  </div>
  
  <div role="tabpanel" class="tab-pane fade" id="openPositions" style="width: 880px;">
  <table id="open" class="dataTable display compact" cellspacing="0" width="100%" >
      <thead>
          <tr>
              <th>Order Number</th>
              <th>Open Time</th>
              <th>Order Type</th>
              <th>Order Size</th>
              <th>Order Symbol</th>
              <th>Open Price</th>
              <th>Profit</th>
              <th>Profit Pips</th>
              <th>Trader</th>
        </tr>
      </thead>
  </table>
  </div>

    </div>
  </div>
  </div>

  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>

  </div>

    </div><!-- end dashboard div -->
  
  
  
    <div id="system" class="tab-pane fade" style="height: 1600px">

    <div id="loading">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-red">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-yellow">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>

      <div class="spinner-layer spinner-green">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
        </div>
      </div>
    </div>  
    </div>
    
    <div id="traderPerformanceInfo" style="display:none;">



    <div class="top-trader">

    <div class="top-left-trader">

    <div class="picture-trader">
    <img class="img-circle pictureTrader" src="" alt="...">
    </div>

    <div class="trader-name">
    <p class="username"></p>
    </div>

    </div>



    <div class="top-right-trader">

    <div class="strategy">
    <div class="strategy-label">
    <div class="inline"><i class="fa fa-lightbulb-o fa-2x" ></i></div><div class="inline"><p>STRATEGY</p></div>
    </div>
    </div>

    <div class="description">
    <div class="description-text">
    <p>Dear followers. Our system operates at increased market volatility. At such times, the market for us is more predictable and can enter into a transaction with little risk. We do not chase for thousands pips. We do not open more number orders. The main task for us is a stable profit every month with minimal drawdown. Our trading philosophy - to make a one good trade, then another one good trade and so on.</p>
    </div>
    </div>

    <!--Button-->

    
    <div class="copy-button-trader">
    <button id="belowButtonCopy" type="button" class="btn-floating btn-large waves-effect waves-light green ">
    <i class="signTraderCopy fa fa-plus fa-lg"></i>
    </button>
    <div >
    <span ><p class="belowButtonCopy">Copy</p></span>
    </div>
    </div>
    

    
    <div class="follow-button-trader">
    <button id="belowButtonFollow" type="button" class="btn-floating btn-large waves-effect waves-light blue ">
    <i class="eyeTraderFollow fa fa-eye fa-lg"></i>
    </button>
    <div >
    <span ><p class="belowButtonFollow">Follow</p></span>
    </div>
    </div>


    <!--end-->



    </div>

    </div>

    <br><br>

    <div class="mid-trader">


    <div class="mid-left-trader">

    <label style="padding-right: 7px">ACCOUNT BALANCE :</label>
    <label><span class="label label-primary accountBalance"><div id="accountBalance"></div><span id="accountBalance"></span></span></label></br>
    <label style="padding-right: 22px">ACCOUNT EQUITY :</label>
    <label><span class="label label-primary accountEquity"><div id="accountEquity"></div><span id="accountEquity"></span></span></label></br>

    </div>

    <div class="mid-right-trader">
    
    <ul class="nav nav-tabs">
    <li id="performanceTrader" class="active"><a href="#PnLPerformanceTrader" aria-controls="PnLPerformanceTrader" data-toggle="tab"><i class="fa fa-money fa-lg"></i> Profit</a></li>
    <li id="performanceTrader" ><a href="#pipsPerformanceTrader" aria-controls="pipsPerformanceTrader" data-toggle="tab"><i class="fa fa-pied-piper fa-lg"></i> Profit Pips</a></li>
    <li id="performanceTrader" ><a href="#columnsPipsPerformanceTrader" aria-controls="columnsPipsPerformanceTrader" data-toggle="tab"><i class="fa fa-bar-chart fa-lg"></i> Performance</a></li>
    <li id="performanceTrader" ><a href="#piePipsPerformanceTrader" aria-controls="piePipsPerformanceTrader" data-toggle="tab"><i class="fa fa-pie-chart fa-lg"></i> Trading</a></li>
    </ul>
    <br/>

    <div class="panel panel-default">
    <div class="panel-body z-depth-2">
  
    <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="PnLPerformanceTrader">
    <div id="TraderPerformancePnl" style="height: 300px;width: 780px;"></div>   
    </div>
    <div role="tabpanel" class="tab-pane fade" id="pipsPerformanceTrader">
    <div id="TraderPerformancePips" style="height: 300px;width: 780px;"></div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="columnsPipsPerformanceTrader">
    <div id="TraderPerformanceColumnsPips" style="height: 300px;width: 780px;"></div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="piePipsPerformanceTrader">
    <div id="TraderPerformancePiePips" style="height: 300px;width:780px;"></div>
    </div>
    </div>

    </div>
    </div>

    <ul class="nav nav-tabs">
    <li id="positions" class="active"><a href="#tradingHistoryTrader" aria-controls="tradingHistoryTrader" data-toggle="tab"><i class="fa fa-history fa-lg"></i> Trading History</a></li>
    <li id="positions" ><a href="#openPositionsTrader" aria-controls="openPositionsTrader" data-toggle="tab"><i class="fa fa-ellipsis-h fa-lg"></i> Open Positions</a></li>
    </ul>
    <br/>

    <div class="panel panel-default">
    <div class="panel-body z-depth-2">
  
    <div class="tab-content">
  
    <div role="tabpanel" class="tab-pane fade in active" id="tradingHistoryTrader" style="width: 780px;">
    <table id="historicTrader" class="display compact" cellspacing="0" width="100%" >
        <thead>
            <tr>
                <th>Open Time</th>
                <th>Order Type</th>
                <th>Order Size</th>
                <th>Order Symbol</th>
                <th>Open Price</th>
                <th>Close Time</th>
                <th>Close Price</th>
                <th>Profit</th>
                <th>Profit Pips</th>
            </tr>
        </thead>
    </table>    
    </div>
    
    <div role="tabpanel" class="tab-pane fade" id="openPositionsTrader" style="width: 800px;">
    <table id="openTrader" class="display compact" cellspacing="0" width="100%" >
        <thead>
            <tr>
                <th>Open Time</th>
                <th>Order Type</th>
                <th>Order Size</th>
                <th>Order Symbol</th>
                <th>Open Price</th>
                <th>Profit</th>
                <th>Profit Pips</th>
            </tr>
        </thead>
    </table>
    </div>

    </div>

    </div>
    </div>

    

    </div>

    </div>

    <br />
    

    

  

    
    
    <br />
    <br />
    <br />
    <br />
    
    
    

    </div>
    
    
    <!-- ******************************************************************** -->
    
    
    
    <div id="traderPerformanceList" style="display:block;">
    <!-- Default panel contents -->
    
    <!-- Modal -->
    <div class="modal fade" id="infoOnlyOpenTableTrader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" >
        <div class="modal-dialog" role="document" >
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Positions Open</h4>
                </div>
                <!--Body-->
                <div id="onlyOpenTableTrader" class="modal-body">

                
               

                </div>
                <!--Footer-->
                <div class="">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 40px; width: 80px" >OK</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="infoOnlyCloseTableTrader" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static" >
        <div class="modal-dialog" role="document" >
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Positions Open</h4>
                </div>
                <!--Body-->
                <div id="onlyCloseTableTrader" class="modal-body">


               

                </div>
                <!--Footer-->
                <div class="">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: 40px; width: 80px" >OK</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- /.Live preview-->

    <!--<div class="panel panel-warning z-depth-2">-->
    <!--<div class="panel-heading">Performance Table</div>-->
    <!--<div class="panel-body">
    
    <p>You can follow five trader at a time.</p>
    
    </div>-->
    
    <!-- Table -->
    <!--<div class="table-responsive">-->
    <table class="table">

    <thead>
    <tr height="10px">
    <td>
    </td>
    <td > 
    </td>
    <td width="75px"><p>Trader</p>
    </td>
    <td ><p>Analytics</p>
    </td>
    <td ><p>Pips</p>
    </td>
    <td width="100px"><p>Trades</p>
    </td>
    <td width="170px"><p>AverPips</p>
    </td>
    <td><p>Win %</p>
    </td>
    </tr>
    </thead>
    
    <?php 
    //$result_set_id_traderaccounts = $db->query("SELECT id,username FROM traderaccounts WHERE active = 1");
    $allTraders = new TraderAccount;
    $positions  = $allTraders->find_all_traders_active_id_and_username();

    for ($j=0; $j < count($positions) ; $j++) { 
    //while($id_traders = $db->fetch_array($result_set_id_traderaccounts)){
    $i                  = $positions[$j]->id;
    //$i = (int)$id_traders["id"]; // [1,3] ?>    
    

    
    <tbody class="trader-list">
    <tr  style="height:60px">
    <td width ="20px" rowspan="2">
    <div style="position:relative;">
    <div class="bulletTrader" style="position:absolute; top:55px">
    <p><?php echo $j+1 ; ?></p>
    </div>
    </div>
    </td>
    <td width ="110px" rowspan="2">
    <div style="position:relative;">
    <div id="pictureProfilTrader" style="position:absolute; top:30px">
    <?php $photoTrader = PhotographTrader::find_by_id($i); 
          $loginTrader = LoginTrader::find_by_id($i);

          if ($photoTrader->filename == "") { ?>
    <img src="assets/images/profil.jpg" alt="" height="91px" width="91px" class="img-circle"/>
    <?php } else { ?>
    <img src="<?php echo "/imagesTrader/".$loginTrader->username."/".$photoTrader->filename; ?>" alt="" height="91px" width="91px" class="img-circle"/>
    <?php } ?>

    </div>
    </div>
    </td>
    
    <td width ="150px" rowspan="2">
    <div style="position:relative;">
    <div id="userTrader" style="position:absolute; top:20px"> 
    <?php echo "<p>". $positions[$j]->username ."</p>"; ?>
    </div>
    </div>
    <br />
    <div style="position:relative; top:25px; width:150px; height: 50px">
    <div id="descriptionTrader" style="position:absolute" >
    <p>
    "This is a text example!! here you will find the text description of the trading system"
    </p>
    </div>
    </div>
    </td>
     
    <td width="230px" rowspan="2">
    <div style="position:relative">
    <?php echo "<div id=\"performancePipsTraderList" . $i . "\" style=\"width: 155px; height: 115px; position:absolute; top:15px\"></div>"; ?>
    </div>
    </td>

    <td class="stat">
    <div style="position:relative;">
    <div style="position:absolute;top:20px">
    <?php 
          $totalPips          = Traderhistory::count_all_pips_by_traderaccount_id($i); 
          $totalPositions     = Traderhistory::count_all($i); 
          $winPercent         = Traderhistory::calcul_winning_percentages_by_traderaccount_id($i);
          $averagePips        = Traderhistory::calcul_average_Pips($i);

          $copiers            = traderCopy::find_all_copiers_by_traderaccount_id($i);
          $copierArray        = array();
          for ($b=0; $b < count($copiers) ; $b++) {
          $copierArray[]      = $copiers[$b]->account_id;
          }

          $allAccountBalance  = Info::find_all_account_Balance_by_account_id_array($copierArray);
          for ($y=0; $y < count($allAccountBalance) ; $y++) {
          $balanceArray[] = $allAccountBalance[$y]->accountBalance;
          }
          unset($copierArray); 
      
          if($totalPips) { 
          
          echo "<p>" . $totalPips . "</p>"; 
          } else {
          echo "<p>" . "0" . "</p>";
          } 
    ?>
    </div>
    </div>
    </td>
    
    
    <td class="stat">
    <div style="position:relative;">
    <div style="position:absolute;top:20px">
    <?php if($totalPositions) 
       { 
        echo "<p>" . $totalPositions . "</p>"; 
       } else {
        echo "<p>" . "0" . "</p>";
       }  
    ?>
    </div>
    </div>
    </td>
    
    
    <td class="stat">
    <div style="position:relative;">
    <div style="position:absolute;top:20px">
    <?php if($averagePips){
        echo "<p>" . $averagePips . "</p>";
    } else {
        echo "<p>" . "0" . "<p>";
    } ?>
    </div>
    </div>
    </td>
    
    
    <td class="stat">
    <div style="position:relative;">
    <div style="position:absolute;top:20px">
    <?php if ($winPercent){
    echo "<p>" . $winPercent . " %" . "</p>";    
    } else {
    echo "<p>" . "0 %" . "</p>";     
    }
    ?>
    </div>
    </div>
    </td>
    
    
    </tr>

    <tr style="height:100px">

    <td colspan="2" rowspan="1" >
    <p class="titleAmountCopy">AMOUNT COPYING</p>
    <?php if(count($balanceArray)>0){
    echo '<p class="valueAmountCopy">' . "$ " . array_sum($balanceArray) . "</p>";
    } else {
    echo '<p class="valueAmountCopy">' . "$ " . "0" . "</p>"; 
    }
    unset($balanceArray);
    ?> 
    </td>
    <td colspan="2" rowspan="1">

    <div style="position:relative;">
    


    <?php 

    $traders = Tradercopy::find_all_by_account_id($id);

    for ($k=0; $k < count($traders) ; $k++) { 
    if ($traders[$k]->traderaccount_id == $i) {
    $signClass = "fa-minus";  
    $traderNumber  = $i;
    $color     = "red";

    $assoc_array_copy_class[] = array("data"=>$signClass, "traderNumber"=>$traderNumber, "color"=>$color);

    } 
    }


    $followers = Traderfollow::find_all_by_account_id($id);

    for ($k=0; $k < count($followers) ; $k++) { 
    if ($followers[$k]->traderaccount_id == $i) {
    $eyeClass = "fa-eye-slash";  
    $traderNumber  = $i;
    $color     = "red";

    $assoc_array_follow_class[] = array("data"=>$eyeClass, "traderNumber"=>$traderNumber, "color"=>$color);

    } 
    }

     

    ?>
    
    <div style="position:absolute;left:0px">
    <div class="inline">
    <button id="copyTrader" data-postid=<?php echo "'" . $i . "'"; ?> type="button" class="btn-floating btn-small waves-effect waves-light green copyTrader">
    <i class="signTrader fa fa-plus fa-lg"></i>
    </button>

    <div style="position:absolute;left:16px;top:60px">
    <span myid=<?php echo $i; ?> ><p class="belowButton">Copy</p></span>
    </div>
    </div>
    </div>

    <div style="position:absolute;left:70px">
    <div class="inline">
    <button id="followTrader" data-postid=<?php echo "'" . $i . "'"; ?> type="button" class="btn-floating btn-small waves-effect waves-light blue followTrader">
    <i class="eyeTrader fa fa-eye fa-lg"></i>
    </button>

    <div style="position:absolute;left:14px;top:60px">
    <span myidf=<?php echo $i; ?> ><p class="belowButton">Follow</p></span>
    </div>
    </div>
    </div>

    <div style="position:absolute;left:140px">

    <div class="inline">

    <button data-postid=<?php echo $i; ?> type="button" class="btn-floating btn-small waves-effect waves-light orange moreInfo">
    <i class="fa fa-info fa-lg"></i>
    </button>

    <div style="position:absolute;left:6px;top:60px">
    <p class="belowButton">More Info</p>
    </div>

    </div>

    </div>

    </div>

    <?php
    }
    ?>
   

    </td>
    
    </tr>



    </tbody>
    
    </table>


    <!--</div>-->
    <!--</div>-->
    
    </div>
    
    </div>

    <!-- ******************************************************************** -->

    
    
    <div id="user" class="tab-pane fade" >
    <div class="row">
   
    <form action="assets/php/account-info.php" method="post" id="accountInfo" >   
   
    <div class="col-sm-4">
    
    <div class="panel panel-warning z-depth-2" style="height: 450px">
    <div class="panel-heading">User Account Info</div>
    <div class="panel-body">

    <div class="form-group">
    <label for="username">Username</label><br />

    <?php if ($user->type == "1") { ?>
    <input type="text" class="form-control" name="username" id="username" readonly="readonly" placeholder=<?php echo $loginInvestor->username; ?> >
    <?php } else if ($user->type == "2") { ?>
    <input type="text" class="form-control" name="username" id="username" placeholder=<?php echo $loginTwitter->username; ?> >
    <?php } else if ($user->type == "3") { ?> 
    <input type="text" class="form-control" name="username" id="username" placeholder=<?php echo $loginFacebook->username; ?> >
    <?php } ?>



    </div>
    <div class="form-group">
    <?php if ($user->type == "1") { ?>
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" value="" name="email" placeholder=<?php echo $loginInvestor->email; ?> >
    <?php } else if ($user->type == "2") { ?>
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" value="" name="email" placeholder=<?php echo $loginTwitter->email; ?> >
    <?php } else if ($user->type == "3") { ?>
    <label for="email">Email</label>
    <input type="text" class="form-control" id="email" value="" name="email" placeholder=<?php echo $loginFacebook->email; ?> >
    <?php } ?>
    
    </div>
    </div>  
    </div>

    
    </div>
  
    <div class="col-sm-4">
    
    <div class="panel panel-warning z-depth-2" style="height: 450px">
    <div class="panel-heading">Change/Set a Password</div>
    <div class="panel-body">

    <div class="form-group">
    <label for="password">New Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password">
    </div>
    <div class="form-group">
    <label for="password">Password Again</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="passwordAgain">
    </div>
    </div>
    </div>

  
    </div>
   
    <div class="col-sm-4">
    
    <div class="panel panel-warning z-depth-2" style="height: 450px">
    <div class="panel-heading">Timezone and Location</div>
    <div class="panel-body">

    <div class="form-group">
    <input type="hidden" name="isAutoTimezone" value="0"  />
    <input type="checkbox" id="isAutoTimezone" name="isAutoTimezone" value="1" <?php echo $user->isAutoTimezone == 1 ? "checked" : ""; ?> />
    <label for="isAutoTimezone">Set timezone automatically</label>

    </div>
    
    <div class="form-group">

    <label >Timezone</label>
    
    <select id="timezone" name="timezone" >
    <?php
    $utc = new DateTimeZone('UTC');
    $dt = new DateTime('now', $utc);
        
    foreach(DateTimeZone::listIdentifiers() as $tz) {
    $current_tz = new DateTimeZone($tz);
    $offset =  $current_tz->getOffset($dt);
    //$transition =  $current_tz->getTransitions($dt->getTimestamp(), $dt->getTimestamp());
    //$abbr = $transition[0]['abbr'];
    echo '<option value="' .$tz. '"';
    if ($user->timezone == $tz ){
        echo " selected";
    }
    //echo ">" .$tz. ' [' .$abbr. ' '. formatOffset($offset). ']</option>';
    echo ">" .$tz. ' [' . 'UTC' . ' '. formatOffset($offset). ']</option>';

    }
    ?>
    </select>
    
    </div>
    
    <div class="form-group">
    <label >Daylight Saving</label>
    <select id="daylight" name="daylight">
    <?php
    echo '<option value="0"';
    if($user->daylight == "0" ){
    echo " selected";   
    }
    echo ">DST Off</option>";
    echo '<option value="1"';
    if($user->daylight == "1" ){
    echo " selected";   
    }
    echo ">DST On</option>";
    ?>
    </select>
    </div>
    
    <div class="form-group">
    <label >Country</label>
    <select name="country" >
    <?php
    $countries = array(
     'AF' => 'Afghanistan',
     'AX' => 'Åland Islands',
     'AL' => 'Albania',
     'DZ' => 'Algeria',
     'AS' => 'American Samoa',
     'AD' => 'Andorra',
     'AO' => 'Angola',
     'AI' => 'Anguilla',
     'AQ' => 'Antarctica',
     'AG' => 'Antigua and Barbuda',
     'AR' => 'Argentina',
     'AM' => 'Armenia',
     'AW' => 'Aruba',
     'AU' => 'Australia',
     'AT' => 'Austria',
     'AZ' => 'Azerbaijan',
     'BS' => 'Bahamas',
     'BH' => 'Bahrain',
     'BD' => 'Bangladesh',
     'BB' => 'Barbados',
     'BY' => 'Belarus',
     'BE' => 'Belgium',
     'BZ' => 'Belize',
     'BJ' => 'Benin',
     'BM' => 'Bermuda',
     'BT' => 'Bhutan',
     'BO' => 'Bolivia, Plurinational State of',
     'BQ' => 'Bonaire, Sint Eustatius and Saba',
     'BA' => 'Bosnia and Herzegovina',
     'BW' => 'Botswana',
     'BV' => 'Bouvet Island',
     'BR' => 'Brazil',
     'IO' => 'British Indian Ocean Territory',
     'BN' => 'Brunei Darussalam',
     'BG' => 'Bulgaria',
     'BF' => 'Burkina Faso',
     'BI' => 'Burundi',
     'KH' => 'Cambodia',
     'CM' => 'Cameroon',
     'CA' => 'Canada',
     'CV' => 'Cape Verde',
     'KY' => 'Cayman Islands',
     'CF' => 'Central African Republic',
     'TD' => 'Chad',
     'CL' => 'Chile',
     'CN' => 'China',
     'CX' => 'Christmas Island',
     'CC' => 'Cocos (Keeling) Islands',
     'CO' => 'Colombia',
     'KM' => 'Comoros',
     'CG' => 'Congo',
     'CD' => 'Congo, the Democratic Republic of the',
     'CK' => 'Cook Islands',
     'CR' => 'Costa Rica',
     'CI' => "Côte d'Ivoire",
     'HR' => 'Croatia',
     'CU' => 'Cuba',
     'CW' => 'Curaçao',
     'CY' => 'Cyprus',
     'CZ' => 'Czech Republic',
     'DK' => 'Denmark',
     'DJ' => 'Djibouti',
     'DM' => 'Dominica',
     'DO' => 'Dominican Republic'
    );
    foreach ($countries as $code => $name) {
    echo "<option value=" . $code;
    /****/
    if ( $user->country == $code ) {
        echo " selected";
    }
    else {
        echo "";
    }   
    /****/
        echo " >" . $name . "</option>";
    }
    ?>
    </select>
    </div>
    </div>

    <div class="form-group">
    <button type="submit" name="submit" class="btn btn-default" style="height: 40px; width: 80px;" >Save</button>
    </div>

    </div>
  
    </div><!-- end of div col -->
    
    </div><!-- end of row -->
    
    </form>
    </div><!-- end of tab-pane -->
    
    <div id="profile" class="tab-pane fade" >   
    
    <div class="row">   
   
    <form method="post" id="MyUploadForm" onSubmit="return false" >
  <div class="col-sm-4">
    
  

  <div class="panel panel-warning z-depth-2" style="height: 570px;">
  <div class="panel-heading" >General Information</div>
  <div class="panel-body" >

    <div class="form-group">
    <label >Gender</label>
    <div class="form-group">
    <!--<label class="radio-inline">-->
    <?php echo "<input type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio1\" value=\"Male\"";
    if ( $user->gender == "Male" ) {
        echo "checked=\"checked\"";
        }
      echo "/>"; 
  ?>
  <label for="inlineRadio1">Male</label>
    
    <?php echo "<input type=\"radio\" name=\"inlineRadioOptions\" id=\"inlineRadio2\" value=\"Female\"";
    if ( $user->gender == "Female" ) {
        echo "checked=\"checked\"";
    }
    echo "/>"; 
  ?>
  <label for="inlineRadio2">Female</label>
    
  <br/>
    
    <label >Age ( Birthday )</label>
    <div class="form-inline">
    <select name="day" >
    <?php for ($i=1; $i < 32; $i++) {
        echo "<option value={$i}";
        if ( $date->format("d") == $i ) {
        echo " selected";
        } else {
        echo "";
        }
        echo ">{$i}</option>" ;   
    } ?> 
    
    </select>
    <select name="month" >
    <?php
    $months = array(
     '01' => 'January',
     '02' => 'February',
     '03' => 'March',
     '04' => 'April',
     '05' => 'May',
     '06' => 'June',
     '07' => 'July',
     '08' => 'August',
     '09' => 'September',
     '10' => 'October',
     '11' => 'November',
     '12' => 'December'
     );

    foreach ($months as $number => $name) {
    echo "<option value=" . $number;
    /****/
    if ( $date->format("m") == $number ) {
        echo " selected";
    }
    else {
        echo "";
    }   
    /****/
        echo " >" . $name . "</option>";
    }?>
    </select>
    <select name="year" >
    <?php for ($i=1900; $i <= date("Y", time()); $i++) {
        echo "<option value={$i}";
        if ( $date->format("Y") == $i ) {
        echo " selected";
        } else {
        echo "";
        }
        echo ">{$i}</option>" ;   
    } ?> 
    
    </select>
    </div>

    
    <label>Trading Since</label>
    <select name="dateStart">
    <?php for ($i=1950; $i <= date("Y", time()) ; $i++) {
        echo "<option value={$i}";
        if ( $user->dateStart == $i ) {
        echo " selected";
        } else {
        echo "";
        }
        echo">{$i}</option>" ;
           
       } ?>         
    </select>

    <br />
    <label>Favorite Quote</label>
    <textarea maxlength="200" class="materialize-textarea" rows="3" name="quote" placeholder=<?php echo $user->quote; ?>><?php echo $user->quote; ?></textarea>
    </div>  
    </div>

    </div>  
    </div>

    </div><!-- end of col -->
    
    <div class="col-sm-4">
    

    <div class="panel panel-warning z-depth-2" style="height: 570px;">
    <div class="panel-heading">Profile Picture</div>
    <div class="panel-body" >

  <div class="form-group">

    <label>Image Type allowed: Jpeg, Png</label>

    
    <div class="file-field input-field">
    <button type="button" class="btn btn-default waves-effect waves-light" style="height: 50px; width: 70px;  ">
    <span>File</span>
    <input type="hidden" name="MAX_FILE_SIZE" value=<?php echo "'" . $max_file_size . "'"; ?> />
    <input id="imageInput" name="file_upload" type="file" />
    </button>

    <button id = "delete" type="button" class="btn btn-default waves-effect waves-light" style="height: 50px; width: 150px;  ">
    <span>Delete Picture</span>
    </button> 


    </div>
    <br/>
    <br/>
    <br/>
    <br/>
    <!--<img src="images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>-->
    </div>

    <?php if ( $user->type == "1" )     { ?>
    <?php if ( $photo->filename == "" ) { ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src="assets/images/profil.jpg" alt="..." id="livePicture"/>
    <?php } else { ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src="<?php 
    echo "images/".$loginInvestor->username."/".$photo->filename; 
    ?>" alt="..." id="livePicture"/>
    <?php } ?>

    <?php } else if ( $user->type == "2" ) { ?>
    <?php        
    if ( $loginTwitter->twProfilePicture == "" ) { ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src="assets/images/profil.jpg" alt="..." id="livePicture"/>
    <?php } else if ( $loginTwitter->twProfilePicture != "" ) {
    $normal                      = array("_normal");
    $bigger                      = array("");
    $sourceSmall                 = $loginTwitter->twProfilePicture;
    $source                      = str_replace($normal, $bigger, $sourceSmall);
    ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src=<?php echo $source ?> alt="..." id="livePicture"/>
    <?php } ?>

    <?php } else if ( $user->type == "3" ) {   
    if ( $loginFacebook->fbProfilePicture == "" ) { ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src="assets/images/profil.jpg" alt="..." id="livePicture"/>
    <?php } else if ( $loginFacebook->fbProfilePicture != "" ) { 
    $source = $loginFacebook->fbProfilePicture; ?>
    <img class="img-circle" style="height:200px;width:200px; margin-left: 10px; margin-bottom: 20px" src=<?php echo $source ?> alt="..." id="livePicture"/>
    <?php } ?> 
    
    <?php } ?>

    <div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"  >
  </div>
</div>


  

    <div class="form-group">    
    <button id="profileButton" type="profile" name="profile" class="btn btn-default" style="height: 40px; width: 80px;" >Submit</button>       
    </div>
    <div id="output">
    </div>

    </div>
    </div>
    
    
    </div> <!-- end of col -->
    </form>
    
    </div> <!-- end of row -->

    </div> <!-- end of tab-pane -->
    
    
    <div id="link" class="tab-pane fade" >
    <div class="row">   
   <!-- form -->
    <div class="col-md-4">
    

    <div class="panel panel-warning z-depth-2">
    <div class="panel-heading">TradeNgo Account Information</div>
    <div class="panel-body">

    
    <label style="padding-right: 30px">Last Connexion:</label><label><span class="label label-info"><?php echo $link->lastConnexion;?></label><br />
    <label style="padding-right: 11px">Type Of Connexion:</label><label><span class="label label-info"><?php echo "MT4 Bridge";?></label><br />
    <label style="padding-right: 34px">Bridge Version:</label><label><span class="label label-info"><?php echo "Ver " . $link->bridgeVersion;?></label><br />
    <label style="padding-right: 40px">Update Statut:</label><label><span class="label label-info"><?php if ($link->updateVersion == 1) {echo "Requested";} else if ($link->updateVersion == 0) { echo "Not Requested";}?></label><br />
    <label style="padding-right: 8px">Investor Connexion:</label><label><span id="bridgeConnexion" ><i class="fa fa-spinner fa-spin"></i></span></label><br />
    <label style="padding-right: 17px">Trader Connexion:</label><label><span id="systemConnexion" ><i class="fa fa-spinner fa-spin"></i></span></label><br />
    <label style="padding-right: 23px">Activation Statut:</label><label><span id="active" ><i class="fa fa-spinner fa-spin"></i></span></label><br />
    
    </div>
    </div> 

    </div> 
   
    <div class="col-md-4">
    
    <div class="panel panel-warning z-depth-2">
    <div class="panel-heading">MT4 Broker Information</div>
    <div class="panel-body">

    <label style="padding-right: 46px">Broker Name:</label><label><span class="label label-info"><?php echo $info->companyName;?></span></label><br />
    <label style="padding-right: 37px">Account Name:</label><label><span class="label label-info"><?php echo $info->accountName;?></span></label><br />
    <label style="padding-right: 47px">Server Name:</label><label><span class="label label-info"><?php echo $info->serverAccount;?></span></label><br />
    <label style="padding-right: 43px">Account Type:</label><label><span class="label label-info"><?php echo $info->accountType;?></span></label><br />
    <label style="padding-right: 67px">Leverage:</label><label><span class="label label-info"><?php echo $info->leverage;?></span></label><br />
    <label style="padding-right: 38px">Trade Allowed:</label><label><span id="tradeAllowed"><i class="fa fa-spinner fa-spin"></i></span></label><br />
    <label style="padding-right: 10px">Account Connexion:</label><label><span id="accountConnexion" ><?php echo "";?><i class="fa fa-spinner fa-spin"></i></span></label><br />


    </div>
    </div>
    
    </div>




    </div><!-- end of row --> 
    
      <div class="media">

    <div class="media-left media-middle">
    <a href="bridge/Setup.exe">
    <i class="fa fa-download fa-5x"></i>
    </a>
    </div>
    <div class="media-body">
    <h4 class="media-heading">TradeNgo Bridge Version Beta 0.0.1 For Windows</h4>
    TradeNgo connects directly to your MetaTrader 4 (MT4) via a two-way connection allowing for a live feed instantly syncing to your TradeNgo profile. It automatically publishes your trading activity with preferences set up by you and updates the data for your historical analysis. To set up this two-way connection, You need only to download a TradeNgo Bridge to apply to your MT4. It works just like an Expert Advisor (EA) and does not cost a thing.
    </div>
    
    <br />
    
    <div class="media-left media-middle">
    <a target="_blank" href="https://www.metatrader4.com">
    <i class="fa fa-download fa-5x"></i>
    </a>
    </div>
    <div class="media-body">
    <h4 class="media-heading">MT4 platform For Windows</h4>
    Designed for traders looking for an edge in their trading, the MT4 platform offers a rich and user-friendly interface in a highly customizable trading environment to help improve your trading performance. Accessing your portfolio has never been easier. Enhanced charting functionality and sophisticated order management tools help you to control your positions quickly and efficiently.
    </div>
    
    <br />
    
    <div class="media-left media-middle">
    <a  href="bridge/jump.dmg">
    <i class="fa fa-download fa-5x"></i>
    </a>
    </div>
    <div class="media-body">
    <h4 class="media-heading">MT4 platform + TradeNgo Bridge For MAC</h4>
    While MT4 was not originally designed for Mac, TradeNgo provide you a MT4 Plateform that works as a native OSX application, meaning there is no need to use other software to run the platform. This Mac OSX platform has the full MT4 functionality, and the Expert Advisors TradeNgo Bridge is already installed and ready to use on your Mac.
    </div>
    
    
    
      </div>
    
    
    
    </div><!-- end of tab-pane -->
    
    <div id="setting" class="tab-pane fade" >
   
    <form method="post" id="settings" onSubmit="return false" >
    <div class="form-group">

    <div class="panel panel-warning z-depth-2">
    <div class="panel-heading">Lot(s)</div>
    <div class="panel-body">


    <p>How many lots do you want executed in your account, every time the system recommends a signal.</p>
    <br>
    
    
    <div class="form-group">

    <?php

    $currency  = new Currency();
    $currencies = $currency->find_all();

    for ($i=0; $i < count($currencies) ; $i++) { 
    $lot = Lots::find_by_account_id_and_currency_id($id, $currencies[$i]->id);
    echo "<p style=\"float:left;\">".$currencies[$i]->currency.":</p><input name=".$currencies[$i]->currency." type='text' class='form-control' id='lots' placeholder='". $lot->value ."' value='". $lot->value ."' style='width:100px'><br>";
    }

    ?>
    
    
    </div>
    <div class="form-group">
    <button type="submit" name="setting" class="btn btn-default" value="setting" style="height: 40px; width: 80px;" >Save</button>
    </div>
    </div>
    </div>
    </div>

    </form>

    </div><!-- end of tab-pane -->
    
    
    <div id="notification" class="tab-pane fade" >
    
    <br> 
    <p>Web Push Notification (Beta Version: only for Chrome and Firefox)</p> <br>
    <p>Receive realtime web push notifications on opening and closing positions.</p>
    <br> 

    <!-- Switch -->
    
    
  <div class="switch">
    <label>
      Off
      <?php 
      if ($notification->subscriptionId == "" ) { ?>
      <input id="subNotifButton" type="checkbox">
      <?php
      } else {
      ?>
      <input id="subNotifButton" type="checkbox" checked>
      <?php
      }
      ?>
      
      <span class="lever"></span>
      On
    </label>
  </div>

    <br> 
    <p>Receive realtime notification via email (not available for the moment)</p>
    <br> 

  <div class="switch">
    <label>
      Off
      <input id="subNotifEmailButton" type="checkbox">
      <span class="lever"></span>
      On
    </label>
  </div>

    <br> 
    <p>notification on opening positions</p>
    <br> 

  <div class="switch">
    <label>
      Off
      <?php 
      if ($notification->notificationOpen == "0" ) { ?>
      <input id="subNotifOpenButton" type="checkbox">
      <?php
      } else {
      ?>
      <input id="subNotifOpenButton" type="checkbox" checked>
      <?php
      }
      ?>
      
      <span class="lever"></span>
      On
    </label>
  </div>

    <br> 
    <p>notification on closing positions</p>
    <br> 

   <div class="switch">
    <label>
      Off
      <?php 
      if ($notification->notificationClose == "0" ) { ?>
      <input id="subNotifCloseButton" type="checkbox">
      <?php
      } else {
      ?>
      <input id="subNotifCloseButton" type="checkbox" checked>
      <?php
      }
      ?>
      
      <span class="lever"></span>
      On
    </label>
  </div>


    </div>

    <div id="social" class="tab-pane fade" >
    <p>Share your activity to the World with Twitter</p>
    <br>
    <div class="switch">
    <label>
      Off
      <?php 
      if  ( ($loginTwitter->notificationTw == 1 ) ) { 
        $attribut = "checked";
        } else {
        $attribut = " ";  
        }
      ?>
      <input id="twitterNotification" type="checkbox" <?php echo $attribut; ?> >
      
      <span class="lever"></span>
      On
    </label>
    </div>
    <br>
    <p>Share your activity to the World with Facebook</p>
    <br>
    <div class="switch">
    <label>
      Off
      <?php 
      if  ( ($loginFacebook->notificationFb == 1 ) ) { 
        $attribut = "checked";
        } else {
        $attribut = " ";  
        }
      ?>
      <input id="facebookNotification" type="checkbox" <?php echo $attribut; ?> >
      
      <span class="lever"></span>
      On
    </label>
    </div>
    </div>

      </div><!-- end of tab-content -->
    
    
    </div><!-- end of container -->


    </div><!-- end of sb-site -->

    <footer class="page-footer center-on-small-only">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="white-text">TradeNgo</h5>
                        <p class="grey-text text-lighten-4">Here you can use rows and columns here to organize your footer content.</p>
                    </div>
                    <div class="col-md-6">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
                            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
                            <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
                            <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright text-center rgba-black-light">
                <div class="container-fluid">
                    <i class="fa fa-copyright"></i> 2017 Copyright: <a href="https://www.TradeNgo.co"> TradeNgo.co </a>
                </div>
            </div>
    </footer>    

    <script src="assets/js/main.js"></script>
    <script src="assets/js/plugins/jquery.min.js"></script>
    <script src="assets/js/plugins/bootstrap.min.js"></script>
    <script src="assets/js/plugins/mdb.js"></script>            
    <script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
    <script src="assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
    <script src="assets/js/plugins/jquery.numeric.js"></script>
    <script src="assets/js/forms/accountInfo.js"></script>
    <script src="assets/js/forms/setting.js"></script>
    <script src="assets/js/forms/profile.js"></script>
    <script src="assets/js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/plugins/jquery.dataTables.min.js"></script>
    <script src="https://code.highcharts.com/stock/highstock.js"></script>


    <script>

    $('#belowButtonFollow').click(function(){

    var moreInfoIdTrader = $('#belowButtonFollow').attr( "postid" );
    console.log(moreInfoIdTrader);

    $.ajax({
    type:"POST",
    url:"assets/php/followProcess.php",
    dataType : 'json',
    data: {postid :moreInfoIdTrader, id : idUser},
    success: function(data){
    if (data.response == "success"){
    if (data.exist == '1'){

    $("[myidf="+data.traderId+"]").html('<p class="belowButton">Follow</p>');
    $(".belowButtonFollow").html("Follow");
    toastr["success"]("Changed Saved!", "TradeNgo");

    } else if (data.exist == '0') {

    if (data.limit == '1') {
    toastr["warning"]("Follow is limited to 5 Traders only!", "TradeNgo");
    } else {
    toastr["success"]("Changed Saved!", "TradeNgo");
    $("[myidf="+data.traderId+"]").html('<p class="belowButton">Unfollow</p>');
    $(".belowButtonFollow").html("Unfollow");

    }  

    }

    $(".followTrader[data-postid="+data.traderId+"] > .eyeTrader").toggleClass('fa-eye fa-eye-slash');
    $(".followTrader[data-postid="+data.traderId+"]").toggleClass('blue red');

    $(".eyeTraderFollow").toggleClass('fa-eye fa-eye-slash');
    $("#belowButtonFollow").toggleClass('blue red');
    
    } else if (data.response == "failed") {
    toastr["error"]("Something went wrong", "TradeNgo");
    }

    }
    });
    });

    </script>

    <script>


    $('#belowButtonCopy').click(function(){

    if ($(".eyeTraderFollow").hasClass("fa-eye") && $(".signTraderCopy").hasClass("fa-plus") ) {
    $("#belowButtonFollow").trigger( "click" );
    } else {
    console.log("no");
    }

    if ($(".eyeTraderFollow").hasClass("fa-eye-slash") && $(".signTraderCopy").hasClass("fa-minus") ) {
    $("#belowButtonFollow").trigger( "click" );
    } else {
    console.log("no");
    }


    var moreInfoIdTrader = $('#belowButtonCopy').attr( "postid" );
    console.log(moreInfoIdTrader);

    $.ajax({
    type:"POST",
    url:"assets/php/copyProcess.php",
    dataType : 'json',
    data: {postid :moreInfoIdTrader, id : idUser},
    success: function(data){
    
    if (data.response == "success"){
    if (data.exist == '1'){


    $("[myid="+data.traderId+"]").html('<p class="belowButton">Copy</p>');
    $(".belowButtonCopy").html("Copy");

    toastr["success"]("Changed Saved!", "TradeNgo");

    if ( data.open ) {

    $('#infoOnlyCloseTableTrader').modal('show');
    $("#onlyCloseTableTrader").html('<p>There are positions opened on your account from this Trader<br>Which position would you close ?</p><br><i class="fa fa-cog" aria-hidden="true"></i><table id="onlyCloseTrader" class="dataTable display compact" cellspacing="0" width="100%" ><thead><tr><th>Select</th><th>Order Number</th><th>Open Time</th><th>Order Type</th><th>Order Size</th><th>Order Symbol</th><th>Open Price</th><th>Profit</th><th>Profit Pips</th></tr></thead></table>');


    onlyCloseTableTrader(data.traderId,idUser);

    }

    } else if (data.exist == '0') {

    if (data.limit == '1') {
    toastr["warning"]("Copy is limited to 5 Traders only!", "TradeNgo");
    } else {
    toastr["success"]("Changed Saved!", "TradeNgo");
    $("[myid="+data.traderId+"]").html('<p class="belowButton">Remove</p>');
    $(".belowButtonCopy").html("Remove");

    if ( data.open ) {
    //modal on and table on
    $('#infoOnlyOpenTableTrader').modal('show');
    $("#onlyOpenTableTrader").html('<p>Your are copying a trader with already opened positions. <br>Which position would you copy in your account ?</p><br><i class="fa fa-cog" aria-hidden="true"></i><table id="onlyOpenTrader" class="dataTable display compact" cellspacing="0" width="100%" ><thead><tr><th>Select</th><th>Order Number</th><th>Open Time</th><th>Order Type</th><th>Order Size</th><th>Order Symbol</th><th>Open Price</th><th>Profit</th><th>Profit Pips</th></tr></thead></table>');

    onlyOpenTableTrader(data.traderId,idUser);

    }

    }  

    }

    $(".copyTrader[data-postid="+data.traderId+"] > .signTrader").toggleClass('fa-plus fa-minus');
    $(".copyTrader[data-postid="+data.traderId+"]").toggleClass('green red');

    $(".signTraderCopy").toggleClass('fa-plus fa-minus');
    $("#belowButtonCopy").toggleClass('green red');



    
    } else if (data.response == "failed") {
    toastr["error"]("Something went wrong", "TradeNgo");
    }

    }
    });
    });
    

    

    </script>



    <script>

      if ('serviceWorker' in navigator) {
      console.log('Service Worker is supported');
      navigator.serviceWorker.register('sw.js').then(function() {
        return navigator.serviceWorker.ready;
      }).then(function(reg) {
      console.log('Service Worker is ready :^)', reg);
      reg.pushManager.subscribe({userVisibleOnly: true}).then(function(sub) {
      // let's check if we can detect if it is google here then 
      // if it is google we compare only the right information like playerId with the one in the database.
      // we send an alert if it is not the same 
      console.log('endpoint:', sub.endpoint);
      //reg.active.postMessage(JSON.stringify({id: idUser}));
      //console.log("id before the message sent: " + idUser);
      //console.log("Posted message");

      <?php 


      //$browser = new Browser;
      $browsername = $browser->browsername; ?>
      <?php if ($browsername == "Chrome") { ?>
      subscriptionStartId = sub.endpoint.replace('https://android.googleapis.com/gcm/send/','');
      <?php 
      } else if ($browsername == "Firefox") {
      ?>
      subscriptionStartId = sub.endpoint.replace('https://updates.push.services.mozilla.com/wpush/v1/','');
      <?php
      }
      
      ?>

      //subscriptionStartId = sub.endpoint.replace('https://android.googleapis.com/gcm/send/','');
      
      <?php $subId = $notification->subscriptionId; 
      $assoc_array_subId[] = array("subscriptionId"=>$subId); ?>

      var words = <?php echo json_encode($assoc_array_subId); ?>;

      $.each(words, function(key, value) {
        //console.log('id : ' + value.id + ' username :' + value.tradername );
      console.log("Value of database: "+value.subscriptionId);
      console.log("Value of browser: "+subscriptionStartId);
      if (value.subscriptionId == "" && $("#subNotifButton").is(':unchecked') ){

      } else {
      if (value.subscriptionId != subscriptionStartId) {
      //alert("you changed your browser configuration! You won't receive the notification on this web browser.") ;
      console.log("bingo");
      $('#infoNotif').modal('show');
      }

      }

      });

      });
      }).catch(function(error) {
      console.log('Service Worker error :^(', error);
      });
      }

    </script>

    <script>
    $("#twitterNotification").click( function(){
    if( $(this).is(':checked') ) {
    <?php unset($_SESSION['access_token_notification']); ?> 
    window.location.href = "https://www.tradengo.co/twapp/index.php?notification="+ idUser;
    } else {
    
    deactivateTwitterNotification();
    }
    });
    </script>

    <script>
    $("#facebookNotification").click( function(){
    if( $(this).is(':checked') ) {
    <?php //unset($_SESSION['access_token_notification']); ?> 
    window.location.href = "https://www.tradengo.co/fbapp/notification.php";
    } else {
    
    deactivateFacebookNotification();
    }
    });
    </script>

    <script>
    $("#subNotifEmailButton").click( function(){
    if( $(this).is(':checked') ) {
    (false)   ? console.log("!")   : $(this).prop('checked',false);
    } else {
    (false) ? console.log("!") : $(this).prop('checked',true);
    }
    });
    </script>

    <script >
    $("#subNotifOpenButton").click( function(){
    if( $(this).is(':checked')) {
    (activateOpenNotification());
    } else {
    (deactivateOpenNotification());
    }
    });
    </script>

    <script >
    $("#subNotifCloseButton").click( function(){
    if( $(this).is(':checked')) {
    (activateCloseNotification());
    } else {
    (deactivateCloseNotification());
    }
    });
    </script>

    <script >
    var isPushEnabled = false;
    $("#subNotifButton").click( function(){
    if( $(this).is(':checked') && !isPushEnabled  ) {
    (subscribe()) ? console.log("subscribed") : $("#subNotifButton").prop('checked',true);;
    } else {
    (unsubscribe()) ? console.log("unsubscribed") : $("#subNotifButton").prop('checked',false);;
    }
    });
    </script>

    <script >
    $("#resendNotification").click( function(){
    if( $("#subNotifButton").is(':checked') ) {
    if (subscribe()) {
    console.log("subscribed!"); 
    $("#infoNotif").modal('hide');
    } else {
    $("#subNotifButton").attr('checked', false);

    } 
    }
    });
    </script>

    <script>
  function deactivateTwitterNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/deactivateTwitterNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        return true;    
        } else if (data.response == "failed") {
        return false;
        }
        }
       });
  }
  </script>

  <script>
  function deactivateFacebookNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/deactivateFacebookNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        return true;    
        } else if (data.response == "failed") {
        return false;
        }
        }
       });
  }
  </script>

    <script>
  function activateOpenNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/activateOpenNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifOpenButton").prop('checked',true);
        return true;    
        } else if (data.response == "failed") {
        $("#subNotifOpenButton").prop('checked',false);  
        return false;
        }
        }
       });
  }
  </script>

  <script>
  function deactivateOpenNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/deactivateOpenNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifOpenButton").prop('checked',false);
        return true;    
        } else if (data.response == "failed") {
        $("#subNotifOpenButton").prop('checked',true);
        return false;
        }
        }
       });
  }
  </script>

  <script>
  function activateCloseNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/activateCloseNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifCloseButton").prop('checked',true);
        return true;    
        } else if (data.response == "failed") {
        $("#subNotifCloseButton").prop('checked',false);
        return false;
        }
        }
       });
  }
  </script>

  <script>
  function deactivateCloseNotification(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/deactivateCloseNotification.php",
        dataType : 'json',
        data: {id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifCloseButton").prop('checked',false);
        return true;    
        } else if (data.response == "failed") {
        $("#subNotifCloseButton").prop('checked',true);
        return false;
        }
        }
       });
  }
  </script>

   
    <script>
    function subscribe() {
  // Disable the button so it can't be changed while
  // we process the permission request
  var pushButton = document.querySelector('#subNotifButton');
  pushButton.disabled = true;

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {
    serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})
      .then(function(subscription) {
        // The subscription was successful
        isPushEnabled = true;
        //pushButton.textContent = 'Disable Push Messages';
        pushButton.disabled = false;

        // TODO: Send the subscription subscription.endpoint
        // to your server and save it to send a push message
        // at a later date
        // let's check if we can detect if it is google here then 
        // if it is google we keep only the right information like playerId

        navigator.sayswho= (function(){
        var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
        })();

        var browser = navigator.sayswho;

        if (browser.search("Chrome") != -1) {

        console.log(subscription.endpoint);
        console.log("we past the Chrome test");
        subscriptionId = subscription.endpoint.replace('https://android.googleapis.com/gcm/send/','');

        //console.log(subscriptionId);

        if (sendSubscriptionIdToServer(subscriptionId)) {
        //return true;
        } else {
        return false; 
        } 
        } else if (browser.search("Firefox") != -1) { 

        console.log(subscription.endpoint);

        console.log("we past the Firefox test");
        subscriptionId = subscription.endpoint.replace('https://updates.push.services.mozilla.com/wpush/v1/','');

        //console.log(subscriptionId);

        if (sendSubscriptionIdToServer(subscriptionId)) {
        //return true;
        } else {
        console.log("here!!!");
        return false; 
        } 
        

        }
      })
      .catch(function(e) {
        if (Notification.permission === 'denied') {
          // The user denied the notification permission which
          // means we failed to subscribe and the user will need
          // to manually change the notification permission to
          // subscribe to push messages
          //window.Demo.debug.log('Permission for Notifications was denied');
          console.log('Permission for Notifications was denied');
          pushButton.disabled = true;
        } else {
          // A problem occurred with the subscription, this can
          // often be down to an issue or lack of the gcm_sender_id
          // and / or gcm_user_visible_only
          //window.Demo.debug.log('Unable to subscribe to push.', e);
          console.log('Unable to subscribe to push.', e);
          pushButton.disabled = false;
          //pushButton.textContent = 'Enable Push Messages';
        }
    });
  });
  return true;
}

function unsubscribe() {  
  var pushButton = document.querySelector('#subNotifButton');  
  pushButton.disabled = true;

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {  
    // To unsubscribe from push messaging, you need get the  
    // subscription object, which you can call unsubscribe() on.  
    serviceWorkerRegistration.pushManager.getSubscription()
      .then(function(pushSubscription) {  
        // Check we have a subscription to unsubscribe  
        if (!pushSubscription) {  
          // No subscription object, so set the state  
          // to allow the user to subscribe to push  
          isPushEnabled = false;  
          pushButton.disabled = false;  
          //pushButton.textContent = 'Enable Push Messages';  
          return;  
        }  

        // TODO: Make a request to your server to remove
        // the users data from your data store so you
        // don't attempt to send them push messages anymore
        var unsubscribe;
        if (unsubscriptionToServer()) {
        unsubscribe = true; 
        } else {
        unsubscribe = false;
        }


        // We have a subscription, so call unsubscribe on it  
        pushSubscription.unsubscribe()
          .then(function(successful) {  
            pushButton.disabled = false;  
            //pushButton.textContent = 'Enable Push Messages';  
            isPushEnabled = false; 
            if (unsubscribe) {
            //return true;   
            } else {
            return false;
            }  
          }).catch(function(e) {  
            // We failed to unsubscribe, this can lead to  
            // an unusual state, so may be best to remove   
            // the users data from your data store and   
            // inform the user that you have done so

            console.log('Unsubscription error: ', e);  
            pushButton.disabled = false;
            //pushButton.textContent = 'Enable Push Messages'; 
          });  


      }).catch(function(e) {  
        console.error('Error thrown while unsubscribing from push messaging.', e);  
      }); 
  }); 
  return true;
  
}

    </script>

  <script>
  function sendSubscriptionIdToServer(subscription) {

  $.ajax({
        type:"POST",
        url:"assets/php/subscriptionIdToServerProcess.php",
        dataType : 'json',
        data: {subscriptionId : subscription, id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifButton").prop('checked',true);
        return true;    
        } else if (data.response == "failed") {
        $("#subNotifButton").prop('checked',false);
        return false;
        }
        }
       });
  }
  </script>

  <script>
  function unsubscriptionToServer() {

  $.ajax({
        type:"POST",
        url:"assets/php/unsubscriptionProcess.php",
        dataType : 'json',
        data: { id : idUser },
        success: function(data){
        if(data.response == "success"){
        $("#subNotifButton").prop('checked',false);
        return true;
        } else if (data.response == "failed") {
        $("#subNotifButton").prop('checked',true);
        return false;
        }
        }
       });
  }
  </script>

  <script>

    $('#delete').click(function(){  
    $.ajax({
        type:"POST",
        url:"assets/php/deletePictureProcess.php",
        dataType : 'json',
        data: {id : idUser},
        success: function(data){
        if(data.response == "success"){
              <?php if ($user->type == 1) { ?>
              var pictureName = "assets/images/profil.jpg";

              $('#livePicture').fadeOut( "slow", function() {
              $('#livePicture').fadeIn( "slow");
              $('#livePicture').attr("src",pictureName);
              $('#sidePicture').attr("src",pictureName);
              $('#jumboPicture').attr("src",pictureName);
              });


              <?php } else if ($user->type == 2) { ?>
              eggPicture = data.link;

              $('#livePicture').fadeOut( "slow", function() {
              $('#livePicture').fadeIn( "slow");
              $('#livePicture').attr("src",eggPicture);
              $('#sidePicture').attr("src",eggPicture);
              $('#jumboPicture').attr("src",eggPicture);
              });

              <?php } ?>
              
        
        toastr["success"]("Picture Deleted!", "TradeNgo")
        } else if (data.response == "failed") {
         
        toastr["error"]("Something went wrong", "TradeNgo")
        }
        }
    });
    
    });
    </script>

  <script>

  function livefeed() {
        $.ajax({
        url: 'assets/php/livefeed.php',
        type: 'POST',
        data: { id : 10 },
        async: true,
        dataType: "json",
        success: function (data) {
      var myArray = new Array();
      $( "div #orderNumberActivity" ).each(function( index ) {
      orderNumber = $( this ).text();
      myArray.push(orderNumber);  
      });
      for (var x = 0; x < data.length; x++) {
      //console.log(data[x].username);
      if(jQuery.inArray(data[x].orderNumber, myArray) == -1) {
      if ( data[x].filename == '' ) {
      pictureName = "assets/images/profil.jpg"; 
      } else {
      pictureName = "images/"+data[x].username+"/"+data[x].filename;
      }
      //console.log("myArray");
      $(".collection").prepend($("<li class=\"collection-item avatar\"><div id=\"orderNumberActivity\" style=\"display: none;\">"+ data[x].orderNumber +"</div><img src="+pictureName+" class=\"circle\"/><span class=\"title\">"+data[x].username+"</span><p>Closed a "+data[x].orderSymbol+" position at "+data[x].closePrice+" "+data[x].result+" "+data[x].profitPips+" Pips</p><a class=\"secondary-content\"><i class=\"fa fa-star fa-lg\"></i></a></li>").addClass('animated lightSpeedIn'));
      }
      }
      setTimeout(function(){ 
        $(".collection-item").removeClass('animated lightSpeedIn'); 
      }, 3000);
      myArray = [];
      myArray.length = 0;
      }
      });
      };
  </script>

  <script>

   $(document).ready(function () {
   $('select').material_select();
   });

  </script>



    <script>
    $('.copyTrader').click(function(){
    var postid = $(this).data('postid');  

    if ($(".followTrader[data-postid="+postid+"] > .eyeTrader").hasClass("fa-eye") && $(".copyTrader[data-postid="+postid+"] > .signTrader").hasClass("fa-plus") ) {
    $(".followTrader[data-postid="+postid+"]").trigger( "click" );
    } else {
    console.log("no");
    }

    if ( $(".followTrader[data-postid="+postid+"] > .eyeTrader").hasClass("fa-eye-slash") && $(".copyTrader[data-postid="+postid+"] > .signTrader").hasClass("fa-minus") ) {
    $(".followTrader[data-postid="+postid+"]").trigger( "click" );
    } else {
    console.log("no");
    }

    $.ajax({
    type:"POST",
    url:"assets/php/copyProcess.php",
    dataType : 'json',
    data: {postid :postid, id : idUser},
    success: function(data){
    
    if (data.response == "success"){
    if (data.exist == '1'){


    $("[myid="+data.traderId+"]").html('<p class="belowButton">Copy</p>');
    toastr["success"]("Changed Saved!", "TradeNgo");



    if ( data.open ) {



    $('#infoOnlyCloseTableTrader').modal('show');
    $("#onlyCloseTableTrader").html('<p>There are positions opened on your account from this Trader<br>Which position would you close ?</p><br><i class="fa fa-cog" aria-hidden="true"></i><table id="onlyCloseTrader" class="dataTable display compact" cellspacing="0" width="100%" ><thead><tr><th>Select</th><th>Order Number</th><th>Open Time</th><th>Order Type</th><th>Order Size</th><th>Order Symbol</th><th>Open Price</th><th>Profit</th><th>Profit Pips</th></tr></thead></table>');


    onlyCloseTableTrader(data.traderId,idUser);

    }

    } else if (data.exist == '0') {

    if (data.limit == '1') {
    toastr["warning"]("Copy is limited to 5 Traders only!", "TradeNgo");
    } else {
    toastr["success"]("Changed Saved!", "TradeNgo");
    $("[myid="+data.traderId+"]").html('<p class="belowButton">Remove</p>');

    if ( data.open ) {
    //modal on and table on
    $('#infoOnlyOpenTableTrader').modal('show');
    $("#onlyOpenTableTrader").html('<p>Your are copying a trader with already opened positions. <br>Which position would you copy in your account ?</p><br><i class="fa fa-cog" aria-hidden="true"></i><table id="onlyOpenTrader" class="dataTable display compact" cellspacing="0" width="100%" ><thead><tr><th>Select</th><th>Order Number</th><th>Open Time</th><th>Order Type</th><th>Order Size</th><th>Order Symbol</th><th>Open Price</th><th>Profit</th><th>Profit Pips</th></tr></thead></table>');

    onlyOpenTableTrader(data.traderId,idUser);

    }

    }  

    }

    $(".copyTrader[data-postid="+data.traderId+"] > .signTrader").toggleClass('fa-plus fa-minus');
    $(".copyTrader[data-postid="+data.traderId+"]").toggleClass('green red');
    
    } else if (data.response == "failed") {
    toastr["error"]("Something went wrong", "TradeNgo");
    }

    }
    });
    });
    </script>



    <script>

    $('.followTrader').click(function(){
    var postid = $(this).data('postid');  
    $.ajax({
    type:"POST",
    url:"assets/php/followProcess.php",
    dataType : 'json',
    data: {postid :postid, id : idUser},
    success: function(data){
    if (data.response == "success"){
    if (data.exist == '1'){

    $("[myidf="+data.traderId+"]").html('<p class="belowButton">Follow</p>');
    toastr["success"]("Changed Saved!", "TradeNgo");

    } else if (data.exist == '0') {

    if (data.limit == '1') {
    toastr["warning"]("Follow is limited to 5 Traders only!", "TradeNgo");
    } else {
    toastr["success"]("Changed Saved!", "TradeNgo");
    $("[myidf="+data.traderId+"]").html('<p class="belowButton">Unfollow</p>');

    }  

    }

    $(".followTrader[data-postid="+data.traderId+"] > .eyeTrader").toggleClass('fa-eye fa-eye-slash');
    $(".followTrader[data-postid="+data.traderId+"]").toggleClass('blue red');
    
    } else if (data.response == "failed") {
    toastr["error"]("Something went wrong", "TradeNgo");
    }

    }
    });
    });

    </script>

    <script>
    var words = <?php echo json_encode($assoc_array_copy_class); ?>;
    $.each(words, function(key, value) {
    

    if (typeof value.traderNumber === 'undefined' || value.traderNumber === null) {
    // variable is undefined or null we don't need to do anything

    } else {
    $(".copyTrader[data-postid="+value.traderNumber+"] > .signTrader").toggleClass("fa-plus " + value.data);
    $(".copyTrader[data-postid="+value.traderNumber+"]").toggleClass("green " + value.color);
    
    $("[myid="+value.traderNumber+"]").html('<p class="belowButton">Remove</p>');
    console.log("class: "        + value.data);
    console.log("traderNumber :" + value.traderNumber);
    console.log("color :"        + value.color);
    }
    
    });

    </script>

     <script>
    var words = <?php echo json_encode($assoc_array_follow_class); ?>;
    $.each(words, function(key, value) {
    

    if (typeof value.traderNumber === 'undefined' || value.traderNumber === null) {
    // variable is undefined or null we don't need to do anything

    } else {
    $(".followTrader[data-postid="+value.traderNumber+"] > .eyeTrader").toggleClass("fa-eye " + value.data);
    $(".followTrader[data-postid="+value.traderNumber+"]").toggleClass("blue " + value.color);
    
    $("[myidf="+value.traderNumber+"]").html('<p class="belowButton">Unfollow</p>');
    console.log("class: "        + value.data);
    console.log("traderNumber :" + value.traderNumber);
    console.log("color :"        + value.color);
    }
    });

    </script>
    
    <script>
    function chartingPiePipsTrader(data,idTrader){
        //console.info(data);
        traderPerformancePiePips = $('#TraderPerformancePiePips').highcharts({
        chart : {
                
                 plotBackgroundColor: null,
                 plotBorderWidth: null,
                 plotShadow: false,
                 type: 'pie'
            
        },
        title: {
            text: 'Trading'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme)
                    }
                }
            }
        },
        subtitle: {
            text: 'Trading Performance'
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        series: [{
            //type: 'spline',
            name : 'Currency',
            showInLegend: false,
            data: data
        }]
        });
        };
    
    function chartingColumnsPipsTrader(data,idTrader){
        //console.info(data);
        traderPerformanceColumnsPips = $('#TraderPerformanceColumnsPips').highcharts({
        chart : {
                
                type: 'column'
            
        },
        title: {
            text: 'Monthly Performance'
        },
        subtitle: {
            text: 'Trading Performance'
        },
        xAxis: {
            categories: data.date,
            crosshair: true
        },
        yAxis: {
            title: {
            text: 'Pips'
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                animation: false
            }
        },
        series: [{
            //type: 'spline',
            name : 'Pips',
            showInLegend: false,
            data: data.profit
            //data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        }]
        });
        };
    
    
    function chartingPipsTrader(data,idTrader){
        //console.info(data);
        traderPerformancePips = $('#TraderPerformancePips').highcharts('StockChart',{
        chart : {
            events : {
                load : function () {
                   window.seriesPipsTrader = this.series[0];                 
                   window.refreshIntervalPipsId = setInterval(function () {
                   refreshPipsTrader(function(output){
                   //console.log("output Currency: " + output);
                   //shift = series.data.length > 1;
                   var myXPips = seriesPipsTrader.xData[window.seriesPipsTrader.data.length - 1];
                   //console.log("myX:" + myXPips );
                   var myYPips = seriesPipsTrader.yData[window.seriesPipsTrader.data.length - 1];
                   //console.log("myY:" + myYPips );
                   if (myXPips != output[0] && myYPips != output[0] ) {
                   seriesPipsTrader.addPoint(output, true, false);  
                   }
                   output = [];
                   },idTrader);
                   }, 10000  );
                   }
                }
            },
 
        title: {
            text: 'Growth in Pips'
        },
        scrollbar: {
                enabled: false
                },
                rangeSelector: {
                allButtonsEnabled: true,
                enabled: true,
                inputEnabled: false,
                buttons: 
                [       
                  {
                      type: 'month',
                      count: 1,
                      text: '1M'
                  },
                  {
                      type: 'month',
                      count: 3,
                      text: '3M'
                  },
                  {
                      type: 'month',
                      count: 6,
                      text: '6M'
                  },
                  {
                      type: 'month',
                      count: 12,
                      text: '12M'
                  },
                  {
                      type: 'all',
                      text: 'All'
                  }
                ],
                selected: 0
                },
                navigator: {
                enabled: true,
                height: 20
                },
        subtitle: {
            text: 'Trading Performance'
        },
        xAxis: {
            type: 'datetime',
            //tickInterval: 3600 * 1000,
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
            text: 'Pips'
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                animation: false
            }
        },
        series: [{
            type: 'spline',
            name : 'Profit (Pips) / Date',
            showInLegend: false,
            data: data
        }]
        });
        };
    
    
    
    function chartingCurrencyTrader(data,idTrader,accountCurrency){
        //console.info(data);
        traderPerformancePnl = $('#TraderPerformancePnl').highcharts('StockChart',{
        chart : {
            events : {
                load : function () {

                   window.seriesCurrencyTrader = this.series[0];   
                   refreshIntervalCurrencyId = setInterval(function () {          
                   refreshCurrencyTrader(function(output){
                   //console.log("output Currency: " + output);
                   //shift = series.data.length > 1;
                   var myX = seriesCurrencyTrader.xData[window.seriesCurrencyTrader.data.length - 1];
                   //console.log("myX:" + myX );
                   var myY = seriesCurrencyTrader.yData[window.seriesCurrencyTrader.data.length - 1];
                   //console.log("myY:" + myY );
                   if (myX != output[0] && myY != output[0] ) {
                   seriesCurrencyTrader.addPoint(output, true, false);  
                   }
                   output = [];
                   },idTrader);
                   }, 10000  );
                }   
                
            }
        },
        title: {
            text: 'Growth in ' + accountCurrency
        },
        scrollbar: {
                enabled: false
                },
                rangeSelector: {

                
                allButtonsEnabled: true,
                enabled: true,
                inputEnabled: false,

                buttons: 
                [
                 
                  {
                      type: 'month',
                      count: 1,
                      text: '1M'
                  },
                  {
                      type: 'month',
                      count: 3,
                      text: '3M'
                  },
                  {
                      type: 'month',
                      count: 6,
                      text: '6M'
                  },
                  {
                      type: 'month',
                      count: 12,
                      text: '12M'
                  },
                  {
                      type: 'all',
                      text: 'All'
                  }
                  

                ],
                selected: 0
                },
                navigator: {
                enabled: true,
                height: 20
                },
        subtitle: {
            text: 'Trading Performance'
        },
        xAxis: {
            type: 'datetime',
            //tickInterval: 3600 * 1000,
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
            text: accountCurrency
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                animation: false
            }
        },
        series: [{
            type: 'spline',
            name : 'Profit ('+ accountCurrency +') / Date',
            showInLegend: false,
            data: data
        }]
        });
        };
        
        function chartCurrencyTrader(idTrader,accountCurrency) {
        $.ajax({
        url: 'assets/php/performanceChartFundTraderProcess.php',
        type: 'POST',
        data: { id : idTrader, idUser : idUser },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingCurrencyTrader(data,idTrader,accountCurrency);
        }
        });
        }
        
        function refreshCurrencyTrader(handleData,idTrader) {
        $.ajax({
        url: 'assets/php/performanceChartLastFundTraderProcess.php',
        type: 'POST',
        data: { id : idTrader, idUser : idUser },
        dataType: "json",
        success: function (data) {
        handleData(data); 
        }
        });
        }
        
        function chartPipsTrader(idTrader) {
        $.ajax({
        url: 'assets/php/performanceChartPipsTraderProcess.php',
        type: 'POST',
        data: { id : idTrader, idUser : idUser },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingPipsTrader(data,idTrader);
        }
        });
        }
        
        function refreshPipsTrader(handleData,idTrader) {
        $.ajax({
        url: 'assets/php/performanceChartLastPipsTraderProcess.php',
        type: 'POST',
        data: { id : idTrader, idUser : idUser },
        dataType: "json",
        success: function (data) {
        handleData(data); 
        }
        });
        }
        
        function chartColumnsPipsTrader(idTrader) {
        $.ajax({
        url: 'assets/php/performanceChartColumnsPipsTraderProcess.php',
        type: 'POST',
        data: { id : idTrader },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingColumnsPipsTrader(data,idTrader);
        }
        });
        }
        
        function chartPiePipsTrader(idTrader) {
        $.ajax({
        url: 'assets/php/performanceChartPiePipsTraderProcess.php',
        type: 'POST',
        data: { id : idTrader },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingPiePipsTrader(data,idTrader);
        }
        });
        } 
        
    $('.moreInfo').click(function(){

    isBackButtonVisible = 1;  
    testMoreinfoPage = true;    
    window.idTrader = $(this).data('postid'); 
    //console.log("clicked!!!" + idTrader );
    $('#traderPerformanceList').css( "display", "none" );
    $('#loading').css("display", "block");
    //$('#traderPerformanceInfo').fadeIn("slow", function(){ 
    $.ajax({
        type:"POST",
        url:"assets/php/moreInfoTraderProcess.php",
        dataType : 'json',
        data: {idTrader :idTrader, idUser : idUser},
        success: function(data){
        if(data.response == "success"){ //***** if SUCCESS CALL *****//   
        accountCurrency = data.accountCurrency; 
        chartCurrencyTrader(idTrader,accountCurrency);
        chartPipsTrader(idTrader);
        chartColumnsPipsTrader(idTrader);
        chartPiePipsTrader(idTrader);
        historicTableTrader = $('#historicTrader').DataTable( {
        "language": {
        "emptyTable": "History empty"
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"                  : {
            "url"  : "assets/php/closedPositionTraderProcess.php",
            "type" : "POST",
            "data" : { id : idTrader, idUser : idUser },
        },
        "aoColumns": [
            { "mData" : "openTime"          },
            { "mData" : "openType"          },
            { "mData" : "orderSize"         },
            { "mData" : "orderSymbol"       },
            { "mData" : "openPrice"         },
            { "mData" : "closeTime"         },
            { "mData" : "closePrice"        },
            { "mData" : "profit"            },
            { "mData" : "profitPips"        }
        ],
        "rowCallback": function( row, data, index ) {
            
        if ( data["openType"] == "0" ) {
        $('td:eq(1)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(1)', row).html( 'SELL' );  
        }
        var $row = $(row);
      $row.css({"background-color":"#a5cafa"})  ;
        if ( (data["profit"]) <= 0 ) {
      $('td:eq(7)', row).css({"background-color":"#F7819F"});
      $('td:eq(8)', row).css({"background-color":"#F7819F"});
        } else if ( (data["profit"]) > 0 ) {
      $('td:eq(7)', row).css({"background-color":"#81F79F"});
      $('td:eq(8)', row).css({"background-color":"#81F79F"});
        };    
        return $row;
        }
        });

        openTableTrader = $('#openTrader').DataTable( {
        "language": {
        "emptyTable": "There is no position open"
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"                  : {
            "url"  : "assets/php/openPositionTraderProcess.php",
            "type" : "POST",
            "data" : { id : idTrader, idUser : idUser },
        },
        "aoColumns": [
            { "mData" : "openTime"          },
            { "mData" : "openType"          },
            { "mData" : "orderSize"         },
            { "mData" : "orderSymbol"       },
            { "mData" : "openPrice"         },
            { "mData" : "profit"            },
            { "mData" : "profitPips"        }
        ],
        "rowCallback": function( row, data, index ) {
            
        if ( data["openType"] == "0" ) {
        $('td:eq(1)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(1)', row).html( 'SELL' );  
        }
        
        var $row = $(row);
      
      $row.css({"background-color":"#a5cafa"})  ;
        if ( (data["profit"]) <= 0 ) {
      $('td:eq(5)', row).css({"background-color":"#F7819F"});
      $('td:eq(6)', row).css({"background-color":"#F7819F"});
        } else if ( (data["profit"]) > 0 ) {
      $('td:eq(5)', row).css({"background-color":"#81F79F"});
      $('td:eq(6)', row).css({"background-color":"#81F79F"});
        }; 
        return $row;
        }
        });
        window.reloadHistory = function reloadHistoricTableTrader () {
        historicTableTrader.ajax.reload(null, false); // user paging is not reset on reload
        }
        if (traderInfoHistoricTableTrader == 0){
        traderInfoHistoricTableTrader = setInterval(reloadHistory, 15000 );
        }
        window.reloadOpen = function reloadOpenTableTrader () {
        openTableTrader.ajax.reload(null, false); // user paging is not reset on reload
        }
        if (traderInfoOpenTableTrader == 0){
        traderInfoOpenTableTrader = setInterval(reloadOpen, 10000 );
        }
        jQuery(".accountBalance").html(data.accountBalance);
        jQuery(".accountEquity").html(data.accountEquity);
        jQuery(".username").html(data.username);
        jQuery(".pictureTrader").attr("src",data.pictureTrader);
        jQuery("#belowButtonCopy").attr({ postid: idTrader }); 
        jQuery("#belowButtonFollow").attr({ postid: idTrader }); 

        if (data.isTraderCopied) {
        jQuery(".belowButtonCopy").html("Remove");
        jQuery(".signTraderCopy").removeClass('fa-plus');
        jQuery("#belowButtonCopy").removeClass('green');
        jQuery(".signTraderCopy").addClass('fa-minus');
        jQuery("#belowButtonCopy").addClass('red');
        } else {
        jQuery(".belowButtonCopy").html("Copy");
        jQuery(".signTraderCopy").removeClass('fa-minus');
        jQuery("#belowButtonCopy").removeClass('red');
        jQuery(".signTraderCopy").addClass('fa-plus');
        jQuery("#belowButtonCopy").addClass('green');

        }

        if (data.isTraderFollowed) {
        jQuery(".belowButtonFollow").html("Unfollow");
        jQuery(".eyeTraderFollow").removeClass('fa-eye');
        jQuery("#belowButtonFollow").removeClass('blue');
        jQuery(".eyeTraderFollow").addClass('fa-eye-slash');
        jQuery("#belowButtonFollow").addClass('red');
        } else {
        jQuery(".belowButtonFollow").html("Follow");
        jQuery(".eyeTraderFollow").removeClass('fa-eye-slash');
        jQuery("#belowButtonFollow").removeClass('red');
        jQuery(".eyeTraderFollow").addClass('fa-eye');
        jQuery("#belowButtonFollow").addClass('blue');
        }


        console.log("isTraderCopied: " + data.isTraderCopied);
        console.log("isTraderFollowed " + data.isTraderFollowed);
        

        traderPerformancePiePips = $('#TraderPerformancePiePips').highcharts();
        traderPerformanceColumnsPips = $('#TraderPerformanceColumnsPips').highcharts();
        traderPerformancePips = $('#TraderPerformancePips').highcharts();
        traderPerformancePnl = $('#TraderPerformancePnl').highcharts();




        var myVar = setInterval(function(){ 

        if (typeof traderPerformancePiePips === 'undefined' || typeof traderPerformanceColumnsPips === 'undefined' || typeof traderPerformancePips === 'undefined' || typeof traderPerformancePnl === 'undefined')  {
        console.log("we stay here!");

        $('#loading').css("display", "block");
        } else {
        console.log("we are out!");
        $('#loading').css("display", "none");
        jQuery("#back").css("display","block");
        $('#traderPerformanceInfo').css( "display", "block" );
        clearInterval(myVar);

        }

        }, 50);


      
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        } else if (data.response == "failed") {
        
        }
        
        },
        complete: function(){
        $('#loading-image').hide();
        }
    }); 
   
    
    });

    </script>
    
    <script>
    $(document).ajaxStart(function() {
    $("#loading-image").show();
    });

    $(document).ajaxStop(function() {
    $("#loading-image").hide();
    });
    </script>
    
    <script>
    $('#back').click(function(){
        
    traderPerformancePiePips      = $('#TraderPerformancePiePips').highcharts();
    traderPerformanceColumnsPips  = $('#TraderPerformanceColumnsPips').highcharts();
    traderPerformancePips         = $('#TraderPerformancePips').highcharts();
    traderPerformancePnl          = $('#TraderPerformancePnl').highcharts();
    
    if (traderPerformancePiePips && traderPerformanceColumnsPips && traderPerformancePips && traderPerformancePnl && $.fn.DataTable.isDataTable( '#historicTrader' ) && $.fn.DataTable.isDataTable( '#openTrader' ) ) {
        
    historicTableTrader.destroy();
    //$('#historicTrader').empty();
    historicTableTrader = null;
    //console.log("tabledatas destroyed!");
    
    openTableTrader.destroy();
    //$('#openTrader').empty();
    openTableTrader = null;
    //console.log("tabledatas destroyed!");
    
    traderPerformancePiePips.destroy();
    traderPerformancePiePips = false;
    //console.log("chart destroyed!");


    traderPerformanceColumnsPips.destroy();
    traderPerformanceColumnsPips = false;
    //console.log("chart destroyed!");
    
    traderPerformancePips.destroy();
    traderPerformancePips = false;
    //console.log("chart destroyed!");
    
    traderPerformancePnl.destroy();
    traderPerformancePnl = false;
    //console.log("chart destroyed!");
    
    }

    
    if (historicTableTrader == null  && openTableTrader == null && traderPerformancePiePips == false && traderPerformanceColumnsPips == false && traderPerformancePips == false && traderPerformancePnl == false){
    testMoreinfoPage = false;   
    clearInterval(traderInfoHistoricTableTrader);
    traderInfoHistoricTableTrader = 0;
    clearInterval(traderInfoOpenTableTrader);
    traderInfoOpenTableTrader = 0;
    clearInterval(traderInfoCurrencyRefresh);
    traderInfoCurrencyRefresh = 0;
    clearInterval(traderInfoPipsRefresh);
    traderInfoPipsRefresh = 0;
    
    clearInterval(refreshIntervalPipsId);
    clearInterval(refreshIntervalCurrencyId);
    
    
    $('#traderPerformanceInfo').fadeIn("slow",function(){
    $(this).css( "display", "none" );
    });

    $('#traderPerformanceList').fadeIn("slow", function(){

    $(this).css( "display", "block" );
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    jQuery("#back").css("display","none");

    });
    } 
    });
    
    </script>
    
    <script>    
    
    
    function renderChartsList(c,data) {  
    
    for (var i in c) { 
                
                new Highcharts.StockChart({
                chart: {
                    renderTo: 'performancePipsTraderList' + c[i],
                    type: 'areaspline',
                    //borderColor: 'grey',
                    plotBorderWidth: 1,
                    borderWidth: 0,
                    backgroundColor: 'white',
                    //margin: [0, 0, 0, 0],
                    spacing: [1,1,1,1]

                },
                plotBorderColor: '#CCCCCC',
                scrollbar: {
                  enabled: false
                },

                rangeSelector: {

                
                allButtonsEnabled: true,
                enabled: false,
                inputEnabled: false,

                buttons: 
                [
                 
                  {
                      type: 'month',
                      count: 1,
                      text: '1M'
                  },
                  {
                      type: 'month',
                      count: 3,
                      text: '3M'
                  },
                  {
                      type: 'month',
                      count: 6,
                      text: '6M'
                  },
                  {
                      type: 'month',
                      count: 12,
                      text: '12M'
                  },
                  {
                      type: 'all',
                      text: 'All'
                  }
                  

                ],
                selected: 0
                },

                
                title: {
                text: ''
                },
                navigator: {
                enabled: false,
                height: 20
                },
                credits: {
                enabled: false
                },
                xAxis: {
                    type: 'datetime',
                    //tickInterval: 3600 * 1000,
                    title: {
                        text: ''
                    },
                    labels: {
                    enabled: false
                    },
                    visible: false,
                    tickColor: '#999',
                    //lineWidth:3,
                    //tickInterval: 1,
                    gridLineWidth: 1 // The default value, no need to change it
                },
                yAxis: {
                    title: {
                    text: ''
                    },
                    labels: {
                    enabled: false
                    },
                    visible: false,
                    //lineWidth:3,
                    //tickInterval: 1,
                    gridLineWidth: 1 // The default value, no need to change it
                },
                legend: {
                    enabled: false
                },
                
                plotOptions: {
                    series: {
                        color: '#ff7200',
                        animation: false,
                        //enableMouseTracking: true,
                        fillOpacity: 0.5,
                        marker: {
                            enabled: false
                        }//,
                        //borderWidth: 0 // < set this option
                        }
                },
                series: [{
                    type: 'areaspline',
                    data: data.performance[i],
                    fillColor: {
                    linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
                    stops: [
                    [0, '#FFFFFF'],
                    [1, '#ff7200']
                  ]
                }
                   
                }]
                
            });
          }
    }
    
    function chartingPipsTraderList() {
        $.ajax({
        url: 'assets/php/performanceChartPipsTraderListProcess.php',
        type: 'POST',
        data: { id : idUser },
        async: true,
        dataType: "json",
        success: function (data) {
        charts = data.id;
        // and also we give the value count of the array charts
        renderChartsList(charts,data);      
        }
        });
        }
    
    chartingPipsTraderList();
    
    //t = setInterval(chartingPipsTrader,3000);
    
   
    
    </script>
    
    <script>
    
    
        
        function chartingPips (data){
        //console.info(data);
        $('#performancePips').highcharts('StockChart',{
        chart : {
            events : {
                load : function () {
                   seriesPips = this.series[0];               
                   refreshPipsVar = function () {
                   refreshPips(function(output){
                   //console.log("output Pips: " + output);
                   //console.log(seriesPips.data.length);
                   var myX = seriesPips.xData[seriesPips.data.length - 1];
                   var myY = seriesPips.yData[seriesPips.data.length - 1];
                   if (myX != output[0] && myY != output[0] ) {
                   seriesPips.addPoint(output, true, false);    
                   }
                   output = [];
                   });
                   }
                }
            }
        },
        title: {
            text: 'Growth in Pips'
        },
                scrollbar: {
                enabled: false
                },
                rangeSelector: {

                
                allButtonsEnabled: true,
                enabled: true,
                inputEnabled: false,

                buttons: 
                [
                 
                  {
                      type: 'month',
                      count: 1,
                      text: '1M'
                  },
                  {
                      type: 'month',
                      count: 3,
                      text: '3M'
                  },
                  {
                      type: 'month',
                      count: 6,
                      text: '6M'
                  },
                  {
                      type: 'month',
                      count: 12,
                      text: '12M'
                  },
                  {
                      type: 'all',
                      text: 'All'
                  }
                  

                ],
                selected: 0
                },
                navigator: {
                enabled: true,
                height: 20
                },
        subtitle: {
            text: 'Your trading Performance'
        },
        xAxis: {
            type: 'datetime',
            //tickInterval: 3600 * 1000,
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
            text: 'Pips'
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                animation: false
            }
        },
        series: [{
            type: 'spline',
            name : 'Profit (pips) / Date',
            showInLegend: false,
            data: data
        }]
        });
        };
        
        function chartPips() {
        $.ajax({
        url: 'assets/php/performanceChartPipsProcess.php',
        type: 'POST',
        data: { id : idUser },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingPips(data);
        }
        });
        }
        
        function refreshPips(handleData) {
        $.ajax({
        url: 'assets/php/performanceChartLastPipsProcess.php',
        type: 'POST',
        data: { id : idUser },
        dataType: "json",
        success: function (data) {
        handleData(data); 
        }
        });
        }
        
        chartPips();
        
    
    
    </script>
    
    <script>
        
    
        
        function chartingCurrency (data){
        
        $('#performancePnl').highcharts('StockChart',{
        chart : {
            events : {
                load : function () {

                   seriesCurrency = this.series[0];  
                   refreshCurrencyVar = function () {     
                   refreshCurrency(function(output){
                   //console.log("output Currency: " + output);
                   //shift = series.data.length > 1;
                   var myX = seriesCurrency.xData[seriesCurrency.data.length - 1];
                   //console.log("myX:" + myX );
                   var myY = seriesCurrency.yData[seriesCurrency.data.length - 1];
                   //console.log("myY:" + myY );
                   if (myX != output[0] && myY != output[0] ) {
                   seriesCurrency.addPoint(output, true, false);    
                   }
                   output = [];
                   });
                   }
                  
                }
            }
        },
        title: {
            text: 'Growth in <?php echo $info->accountCurrency; ?>'
        },
                scrollbar: {
                enabled: false
                },
                rangeSelector: {

                
                allButtonsEnabled: true,
                enabled: true,
                inputEnabled: false,

                buttons: 
                [
                 
                  {
                      type: 'month',
                      count: 1,
                      text: '1M'
                  },
                  {
                      type: 'month',
                      count: 3,
                      text: '3M'
                  },
                  {
                      type: 'month',
                      count: 6,
                      text: '6M'
                  },
                  {
                      type: 'month',
                      count: 12,
                      text: '12M'
                  },
                  {
                      type: 'all',
                      text: 'All'
                  }
                  

                ],
                selected: 0
                },
                navigator: {
                enabled: true,
                height: 20
                },
        subtitle: {
            text: 'Your trading Performance'
        },
        xAxis: {
            type: 'datetime',
            //tickInterval: 3600 * 1000,
            title: {
                text: ''
            }
        },
        yAxis: {
            title: {
            text: '<?php echo $info->accountCurrency; ?>'
            }
        },
        legend: {
            enabled: false
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                animation: false
            }
        },
        series: [{
            type: 'spline',
            name : 'Profit (<?php echo $info->accountCurrency; ?>) / Date',
            showInLegend: false,
            data: data
        }]
        });
        };
        
        function chartCurrency() {
        $.ajax({
        url: 'assets/php/performanceChartFundProcess.php',
        type: 'POST',
        data: { id : idUser },
        async: true,
        dataType: "json",
        success: function (data) {
        chartingCurrency(data);
        }
        });
        }
        
        function refreshCurrency(handleData) {
        $.ajax({
        url: 'assets/php/performanceChartLastFundProcess.php',
        type: 'POST',
        data: { id : idUser },
        dataType: "json",
        success: function (data) {
        handleData(data); 
        }
        });
        }
        
        chartCurrency();
        
    </script>
    
    <script>
        $(document).ready(function(){

        

        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
            
        refresh_div();  
        //var bridge = window.setInterval(refresh_div,3000); Important to keep as commented
        var bridge = 0;
        var dashboardOpenTable = 0;
        var dashboardHistoricTable = 0;
        var dashboardCurrencyRefresh = 0;
        var dashboardPipsRefresh = 0;
        var dashboardHistoricTableTrader = 0;

        //var live = 0;
        live = setInterval(livefeed,3000);
        
        window.traderInfoHistoricTableTrader = 0;
        window.traderInfoOpenTableTrader = 0;
        window.traderInfoCurrencyRefresh = 0;
        window.traderInfoPipsRefresh = 0;
        
        window.refreshIntervalPipsId;
        window.refreshIntervalCurrencyId
        
        

        
        
        $('li a[href="#activity"]').click(function(){

            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".animated .lightSpeedIn").removeClass();
        $(".fa-rss").css("color", "#ff7200");
        $(".fa-tachometer").css("color", "#000");
        $(".fa-list-ul").css("color", "#000");
        $(".label-navbar-trader").css("color", "#000");
        $(".label-navbar-dashboard").css("color", "#000");
        $(".label-navbar-activity").css("color", "#ff7200");


        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        console.log("before:" + window.refreshIntervalPipsId);
        //window.test = clearInterval(window.refreshIntervalPipsId);
        console.log("after:" + window.refreshIntervalPipsId);
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);


        if (live == 0){
        livefeed();
        live              = setInterval(livefeed, 5000 );
        }
        
        }); 
        
        
        $('li a[href="#user"]').click(function(){
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();

        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        
        }); 
        
        $('li a[href="#profile"]').click(function(){
            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();
        
        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        
        }); 
        
        $('li a[href="#link"]').click(function(){
            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();
            
        if (bridge == 0){
        refresh_div();   
        bridge    = setInterval(refresh_div,10000); 
        }
        
        
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        

        
        }); 
        
        $('li a[href="#setting"]').click(function(){
            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();
        

        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        }); 
        
        $('li a[href="#notification"]').click(function(){
            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();
        
        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        
        }); 
        
        $('li a[href="#social"]').click(function(){
            
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".collapsible li").removeClass();
        
        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0;
        clearInterval(dashboardHistoricTableTrader);
        dashboardHistoricTableTrader = 0;

        clearInterval(live);
        live=0;
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        
        
        }); 
        
        $('li a[href="#dashboard"]').click(function(){

        $(".fa-rss").css("color", "#000");
        $(".fa-tachometer").css("color", "#ff7200");
        $(".fa-list-ul").css("color", "#000");
        $(".label-navbar-trader").css("color", "#000");
        $(".label-navbar-dashboard").css("color", "#ff7200");
        $(".label-navbar-activity").css("color", "#000");

        


        if (graphUser == 1) {
        graphUser = 0;
        chartPips();
        chartCurrency();
        }

        



        clearInterval(bridge);
        bridge=0;

        clearInterval(live);
        live=0;
        
        
        if (dashboardOpenTable == 0){
        reloadOpenTable();
        dashboardOpenTable              = setInterval(reloadOpenTable, 10000 );
        }
        if (dashboardHistoricTable == 0) {
        reloadHistoricTable();
        dashboardHistoricTable          = setInterval(reloadHistoricTable, 15000 ); 
        }
        
        if (dashboardCurrencyRefresh == 0 && dashboardPipsRefresh == 0) {
        if (typeof refreshCurrencyVar === 'undefined' || typeof refreshPipsVar === 'undefined' ){
        console.log("not loaded yet!");
        } else {
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        refreshCurrencyVar();
        refreshPipsVar();
        dashboardCurrencyRefresh        = setInterval(refreshCurrencyVar, 10000);
        dashboardPipsRefresh            = setInterval(refreshPipsVar, 10000);
        
        }
        }
        
        
        clearInterval(traderInfoHistoricTableTrader);
        traderInfoHistoricTableTrader=0;
        clearInterval(traderInfoOpenTableTrader);
        traderInfoOpenTableTrader = 0;
        
        clearInterval(window.refreshIntervalPipsId);
        clearInterval(window.refreshIntervalCurrencyId);
        
        }); 
        
        $('li a[href="#system"]').click(function(){

        if (graphListTrader) {
        chartingPipsTraderList();
        graphListTrader = 0;
        console.log("chartingPipsTraderList");
        }

        
        
        $(this).tab('show');
        $('html, body').animate({ scrollTop: 0 }, 'fast');
        $(".pull-right li").removeClass();
        $(".fa-rss").css("color", "#000");
        $(".fa-tachometer").css("color", "#000");
        $(".fa-list-ul").css("color", "#ff7200");
        $(".label-navbar-trader").css("color", "#ff7200");
        $(".label-navbar-dashboard").css("color", "#000");
        $(".label-navbar-activity").css("color", "#000");
        
        clearInterval(bridge);
        bridge=0;
        clearInterval(dashboardOpenTable);
        dashboardOpenTable=0;
        clearInterval(dashboardHistoricTable);
        dashboardHistoricTable=0;
        clearInterval(dashboardCurrencyRefresh);
        dashboardCurrencyRefresh=0;
        clearInterval(dashboardPipsRefresh);
        dashboardPipsRefresh=0; 

        clearInterval(live);
        live=0;
        
        //console.log("readback:" + window.refreshIntervalPipsId);
        
        setInterval(window.refreshIntervalPipsId, 10000);
        setInterval(window.refreshIntervalCurrencyId, 10000);
        
        
        if (window.testMoreinfoPage == true) {
            
            
        
        if (traderInfoHistoricTableTrader == 0){
        window.reloadHistory();
        traderInfoHistoricTableTrader = setInterval(window.reloadHistory, 15000 );
        //console.log("call!!!");
        }
        
        if (traderInfoOpenTableTrader == 0){
        window.reloadOpen(); 
        traderInfoOpenTableTrader = setInterval(window.reloadOpen, 10000 );
        //console.log("call!!!");
        }
        /*window.refreshIntervalPipsId();
        window.refreshIntervalCurrencyId();
        setInterval(window.refreshIntervalPipsId, 10000);
        setInterval(window.refreshIntervalCurrencyId, 10000);***********************************************/
            
        }
        
        
        
        }); 
            
       
        });
    </script>
    
    <script>
    
    
    </script>

    <script>
        jQuery(document).ready(function() {

        isBackButtonVisible = 0;
        graphUser           = 0;
        graphListTrader     = 0;


        AccountInfoForm.initAccountInfoForm();
        ProfileForm.initProfileForm();
        SettingForm.initSettingForm();

        // Initialize collapse button
        $(".button-collapse").sideNav();
        // Initialize collapsible 
        $('.collapsible').collapsible();
        });
    </script>

    <script>

    function onlyCloseTableTrader(idTrader,idUser){
    var onlyCloseTableTraders = $('#onlyCloseTrader').DataTable( {
        "language": {
        "emptyTable": "Loading ... "
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"                  : {
            "url"  : "assets/php/onlyClosePositionTraderProcess.php",
            "type" : "POST",
            "data" : { id : idTrader, idUser : idUser },
        },
        "aoColumns": [
            { "mData" :  null               , // can be null or undefined
      "defaultContent": "<button type=\"button\" class=\"btn btn-primary onlyClose\" style=\"height: 40px; width: 80px\"; >Close</button>"},
            { "mData" : "orderNumber" },
            { "mData" : "openTime"          },
            { "mData" : "openType"          },
            { "mData" : "orderSize"         },
            { "mData" : "orderSymbol"       },
            { "mData" : "openPrice"         },
            { "mData" : "profit"            },
            { "mData" : "profitPips"        }
        ],
        "rowCallback": function( row, data, index ) {
            
        if ( data["openType"] == "0" ) {
        $('td:eq(3)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(3)', row).html( 'SELL' );  
        }
        
        var $row = $(row);
      
      $row.css({"background-color":"#a5cafa"})  ;
        if ( (data["profit"]) <= 0 ) {
      $('td:eq(7)', row).css({"background-color":"#F7819F"});
      $('td:eq(8)', row).css({"background-color":"#F7819F"});
        } else if ( (data["profit"]) > 0 ) {
      $('td:eq(7)', row).css({"background-color":"#81F79F"});
      $('td:eq(8)', row).css({"background-color":"#81F79F"});
        }; 

        return $row;
        }

        });

        var onlyCloseTable = function reloadOnlyCloseTrader () {
        onlyCloseTableTraders.ajax.reload(null, false); // user paging is not reset on reload
        }

        var onlyCloseTimer = setInterval(onlyCloseTable, 5000 );


    $('#onlyCloseTrader tbody').on( 'click', 'button.btn-primary', function (e) {
        $(".onlyClose").prop('disabled', true);
        //$('#positionLoading').css("display", "block");
        clearInterval(onlyCloseTimer);
        var data = onlyCloseTableTraders.row( $(this).parents('tr') ).data();
        onlyCloseTableTraders
        .row( $(this).parents('tr') )
        .remove()
        .draw(); 
        $.ajax({
        type:"POST",
        url:"assets/php/onlyClosePositionUserProcess.php",
        dataType : 'json',
        data: { idTrader : idTrader, idUser : idUser, masterordernumber : data["masterOrderNumber"] },
        success: function(data){
        if(data.response == "success"){
        $(".onlyClose").prop('disabled', false);
        onlyCloseTimer = setInterval(onlyCloseTable, 5000 );
        
        //if ( data.count == 1 ) {
        
        //$("#infoOnlyCloseTableTrader").modal('hide');
        //window.clearInterval(onlyCloseTimer);
        //}

      
        } else if (data.response == "failed") {
        
        }
        }
        });

        });

        

        $('#onlyCloseTrader').on('xhr.dt', function ( e, settings, json, xhr ) {
        if (json.data.length == 0 ){
        $('#infoOnlyCloseTableTrader').modal('hide');
        }
        });

        $('#infoOnlyCloseTableTrader').on('hidden.bs.modal', function (e) {
        console.log('ok ok');
        window.clearInterval(onlyCloseTimer);
        });
        
        };

        

    </script>

    <script>

    

    function onlyOpenTableTrader(idTrader,idUser){
    var onlyOpenTableTraders = $('#onlyOpenTrader').DataTable( {
        "language": {
        "emptyTable": "Loading... "
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"                  : {
            "url"  : "assets/php/onlyOpenPositionTraderProcess.php",
            "type" : "POST",
            "data" : { id : idTrader, idUser : idUser },
        },
        "aoColumns": [
            { "mData" :  null               , // can be null or undefined
      "defaultContent": "<button type=\"button\" class=\"btn btn-primary onlyOpen\" style=\"height: 40px; width: 80px\" >Open</button>"},
            { "mData" : "orderNumber"       },
            { "mData" : "openTime"          },
            { "mData" : "openType"          },
            { "mData" : "orderSize"         },
            { "mData" : "orderSymbol"       },
            { "mData" : "openPrice"         },
            { "mData" : "profit"            },
            { "mData" : "profitPips"        }
        ],
        "rowCallback": function( row, data, index ) {
            
        if ( data["openType"] == "0" ) {
        $('td:eq(3)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(3)', row).html( 'SELL' );  
        }
        
        var $row = $(row);
      
      $row.css({"background-color":"#a5cafa"})  ;
        if ( (data["profit"]) <= 0 ) {
      $('td:eq(7)', row).css({"background-color":"#F7819F"});
      $('td:eq(8)', row).css({"background-color":"#F7819F"});
        } else if ( (data["profit"]) > 0 ) {
      $('td:eq(7)', row).css({"background-color":"#81F79F"});
      $('td:eq(8)', row).css({"background-color":"#81F79F"});
        }; 

        return $row;
        }

        });

        var onlyOpenTable = function reloadOnlyOpenTrader () {
        onlyOpenTableTraders.ajax.reload(null, false); // user paging is not reset on reload
        }

        var onlyOpenTimer =  setInterval(onlyOpenTable, 5000 );


        $('#onlyOpenTrader tbody').on( 'click', 'button.btn-primary', function (e) {
        $(".onlyOpen").prop('disabled', true);

        //$('#onlyOpenTrader').DataTable().settings.xhr.abort();
        
        //onlyOpenTableTraders.ajax.abort();

        window.clearInterval(onlyOpenTimer);  
        
        var data = onlyOpenTableTraders.row( $(this).parents('tr') ).data();
        onlyOpenTableTraders
        .row( $(this).parents('tr') )
        .remove()
        .draw();     
        $.ajax({
        type:"POST",
        url:"assets/php/onlyOpenPositionUserProcess.php",
        dataType : 'json',
        data: { idTrader : idTrader, idUser : idUser, ordernumber : data["orderNumber"] },
        success: function(data){
        
        
        
        if(data.response == "success"){
        //The row can disseapear here
        $(".onlyOpen").prop('disabled', false);
        onlyOpenTimer =  setInterval(onlyOpenTable, 5000 );
        
        
        } else if (data.response == "failed") {

        
        }

        


        }
        });

        });


        $('#onlyOpenTrader').on('xhr.dt', function ( e, settings, json, xhr ) {
        if (json.data.length == 0) {
        $('#infoOnlyOpenTableTrader').modal('hide');
        }
        
        });

        $('#infoOnlyOpenTableTrader').on('hidden.bs.modal', function (e) {
        console.log('no no');
        window.clearInterval(onlyOpenTimer);
        });
        
      
        };

        


    </script>

    <script>

    

    

    </script>
    
    <script>
        
        tableHistoric = $('#historic').DataTable( {
        "language": {
        "emptyTable": "History empty"
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"                  : {
            "url"  : "assets/php/closedPositionUserProcess.php",
            "type" : "POST",
            "data" : { id : idUser },
        },
        "aoColumns": [
            { "mData" : "orderNumber"       },
            { "mData" : "openTime"          },
            { "mData" : "openType"          },
            { "mData" : "orderSize"         },
            { "mData" : "orderSymbol"       },
            { "mData" : "openPrice"         },
            { "mData" : "closeTime"         },
            { "mData" : "closePrice"        },
            { "mData" : "profit"            },
            { "mData" : "profitPips"        },
            { "mData" : "traderaccount_id"  } 
        ],
        "rowCallback": function( row, data, index ) {
        
        
        <?php 
        $traders = $db->query("SELECT id, username FROM traderaccounts");
        $assoc_array_traders = array();
        while($row = $db->fetch_array($traders)){
        $i = (int)$row["id"]; // [1,3]
        $tradername = $row["username"];
        $assoc_array_traders[] = array("id"=>$i, "tradername"=>$tradername); 
        }
        ?>
        
        var words = <?php echo json_encode($assoc_array_traders); ?>;

        $.each(words, function(key, value) {
        if (value.id == data["traderaccount_id"]) {
        $('td:eq(10)', row).html(value.tradername); 
        }
        }); 
        
                                        
        
        if ( data["openType"] == "0" ) {
        $('td:eq(2)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(2)', row).html( 'SELL' );  
        }
        
        var $row = $(row);
        $row.css({"background-color":"#a5cafa"});
        if ( (data["profit"]) <= 0 ) {
        $('td:eq(8)', row).css({"background-color":"#F7819F"});
        $('td:eq(9)', row).css({"background-color":"#F7819F"});

        } else if ( (data["profit"]) > 0 ) {
        $('td:eq(8)', row).css({"background-color":"#81F79F"});
        $('td:eq(9)', row).css({"background-color":"#81F79F"});

        };
        if  ( (data["error"] == 1 ) ) {
        $('td:eq(8)', row).css({"background-color":"#819FF7"});
        $('td:eq(9)', row).css({"background-color":"#819FF7"});

        }   
        return $row;
        }
        });
            
        function reloadHistoricTable () {
        tableHistoric.ajax.reload(null, false); // user paging is not reset on reload
        }
        
        
    </script>
    <script>
        
        var tableOpen = $('#open').DataTable( {
        "language": {
        "emptyTable": "There is no position open"
        },
        "bProcessing"           : false,
        "bserverSide"           : true,
        "paging"                : true,
        "pageLength"            : 10,
        "lengthChange"          : false,
        "iDisplayLength"        : 10,
        "iDisplayStart"         : 0,
        "ordering"              : false,
        "info"                  : false,
        "searching"             : false,
        "ajax"              : {
            "url": "assets/php/openPositionUserProcess.php",
            "type": "POST",
            "data" : { id : idUser }
        },
        "columns": [
            { "data" : "orderNumber"       },
            { "data" : "openTime"          },
            { "data" : "openType"          },
            { "data" : "orderSize"         },
            { "data" : "orderSymbol"       },
            { "data" : "openPrice"         },
            { "data" : "profit"            },
            { "data" : "profitPips"        },  
            { "data" : "traderaccount_id"  } 
        ],
        "rowCallback": function( row, data, index ) {
        

        <?php 
        $traders = $db->query("SELECT id, username FROM traderaccounts");
        $assoc_array_traders = array();
        while($row = $db->fetch_array($traders)){
        $i = (int)$row["id"]; // [1,3]
        $tradername = $row["username"];
        $assoc_array_traders[] = array("id"=>$i, "tradername"=>$tradername); 
        }
        ?>
        
        var words = <?php echo json_encode($assoc_array_traders); ?>;
        $.each(words, function(key, value) {
        if (value.id == data["traderaccount_id"]) {
        $('td:eq(8)', row).html(value.tradername);  
        }
        }); 





        if ( data["openType"] == "0" ) {
        $('td:eq(2)', row).html( 'BUY' );
        } else if ( data["openType"] == "1" ) {
        $('td:eq(2)', row).html( 'SELL' );  
        }
        var $row = $(row);
      $row.css({"background-color":"#a5cafa"})  ;
        if ( (data["profit"]) <= 0 ) {
        $('td:eq(6)', row).css({"background-color":"#F7819F"});
        $('td:eq(7)', row).css({"background-color":"#F7819F"});
        } else if ( (data["profit"]) > 0 ) {
        $('td:eq(6)', row).css({"background-color":"#81F79F"});
        $('td:eq(7)', row).css({"background-color":"#81F79F"});

        };
        return $row;
        }
        } );
        
        
        
        function reloadOpenTable () {
        tableOpen.ajax.reload(null, false);
        }
        
        
        </script>
        <script>
       

        function refresh_div() {
        jQuery.ajax({
        url:'assets/php/bridgeConnexionProcess.php',
        dataType: 'json',
        type:'POST',
        data: { id : idUser },
        success:function(results) { 
        if(results.responseBridge == "failed"){
        jQuery("#bridgeConnexion").html('<i class="fa fa-times fa-lg" style="color: #FF0000;" ></i>');
        jQuery("#accountConnexion").html('<i class="fa fa-times fa-lg" style="color: #FF0000;"></i>');
        } else if(results.responseBridge == "success") {
        
        jQuery("#accountBalance").html(results.accountBalance);
        jQuery("#accountEquity").html(results.accountEquity);
        jQuery("#accountCurrency").html(results.accountCurrency);

        jQuery("#bridgeConnexion").html('<i class="fa fa-check fa-lg" style= "color: #00C851;" ></i>');
        if(results.responseMt4 == "failed"){
        jQuery("#accountConnexion").html('<i class="fa fa-times fa-lg" style= "color: #ff4444; "></i>');
        } else if(results.responseMt4 == "success") {
        jQuery("#accountConnexion").html('<i class="fa fa-check fa-lg" style= "color: #00C851;" ></i>');
        }    
        }
    
          if(results.responseSystem == "failed"){
        jQuery("#systemConnexion").html('<i class="fa fa-times fa-lg" style= "color: #ff4444;" ></i>');
        } else if(results.responseSystem == "success") {
        jQuery("#systemConnexion").html('<i class="fa fa-check fa-lg" style= "color: #00C851;" ></i>');
        }
           
        if(results.active == "failed"){
        jQuery("#active").html('<i class="fa fa-times fa-lg" style= "color: #ff4444;" ></i>');
        } else if(results.active == "success") {
        jQuery("#active").html('<i class="fa fa-check fa-lg" style= "color: #00C851;" ></i>');
        }
          
        if(results.tradeAllowed == "failed"){
        jQuery("#tradeAllowed").html('<i class="fa fa-times fa-lg" style= "color: #ff4444;" ></i>');
        } else if(results.tradeAllowed == "success") {
        jQuery("#tradeAllowed").html('<i class="fa fa-check fa-lg" style= "color: #00C851;" ></i>');

        }
           
        }
        });
        }

    </script>      
    <script>
         $("#lots").numeric(".");

    </script>
    <script>
    function LoadImage(id,link) {
    var img = new Image(),
    x = document.getElementById(id);
    img.onload = function() {
    x.src = img.src;
    };
    img.src = link;
    }
    </script>
    <script>
   /*var update_pizza = function () {
    if ($("#isAutoTimezone").is(":checked")) {
      $("#timezone").prop("disabled", false);
      //$('#daylight').prop('disabled', false);
      console.log('1');
    } else {
      $("#timezone").prop("disabled", "disabled");
      console.log('2');
    }
    };
   $(update_pizza);
   $("#isAutoTimezone").change(update_pizza);*/

    </script>

    <script>
// Get offset time
var rightNow = new Date();
var jan1 = new Date(rightNow.getFullYear(), 0, 1, 0, 0, 0, 0);
var temp = jan1.toGMTString();
var jan2 = new Date(temp.substring(0, temp.lastIndexOf(" ")-1));
var std_time_offset = (jan1 - jan2) / (1000 * 60 * 60);
console.log("timezone :"+std_time_offset);

fetch('/assets/php/saveDetectOffsetTimezone.php',{
    method: 'post',
    headers: {  
      "Content-type": "application/x-www-form-urlencoded"  
    },  
    body: 'detectOffsetTimezone=' + std_time_offset +'&id=' + idUser,
    }).then(function(response) {  
      if (response.status !== 200) {  
        console.log(std_time_offset);
        // Either show a message to the user explaining the error  
        // or enter a generic message and handle the
        // onnotificationclick event to direct the user to a web page  
         console.log('Looks like there was a problem. Status Code: ' + response.status);  
         throw new Error();  
        } 
      return response.json().then(function(data) { 
      }); 
  });
</script>
<script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
    }
</script>
<script>
    $( "tbody:odd" ).css( "background-color", "#f5f5f5" );
    $( "tbody:even" ).css( "background-color", "#ebebeb" );
    $( "thead" ).css( "background-color", "#f5f5f5" );
    //ebebeb
</script>



    
</body>  

  

</html>
