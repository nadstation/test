<?php require_once('../../includes/initialize.php'); ?>
<?php $session = new Session(); ?>
<?php 
if(!$session->is_logged_in()){
redirect_to("index.php");
} 
?>
<?php $id = $session->user_id; ?>
<?php $max_file_size = 1048576; ?>
<?php $photo         = PhotographTrader::find_by_id($id); ?>
<!DOCTYPE html>
<html >
	<head>
	    <link rel="stylesheet" href="../assets/css/bootstrap.min.css" >
      <link rel="stylesheet" href="../assets/css/mdb.css" >
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/dataTables.jqueryui.min.css" >
      <link rel="stylesheet" href="../assets/css/dataTables.bootstrap.min.css">
      <link rel="stylesheet" href="../assets/css/dataTables.jqueryui.min.css">
      <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="../assets/css/plugins/style.css" >
	</head>
	<body>

<h3>Welcome to the TradeNgo's Admin</h3>

<br />

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Add a trading system 
</button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sign Up a Trading System</h4>
      </div>
      <div class="modal-body">
        
    <form id="newTraderAccount">

    <div class="form-group">
    <label for="exampleInputEmail1">Trader Name</label>
    <input type="text" name="username" class="form-control" id="exampleInputEmail1" >
    </div>

    <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
    </div>

    <div>Trader Picture</div>


    <label>Image Type allowed: Jpeg, Png</label>

    <br><br>    


    <?php if ( $photo->filename == "" ) { ?>
    <img style="height:200px;width:200px;" src="assets/images/profil.jpg" alt="..." id="livePicture"/>
    <?php } else { ?>
    <img style="height:200px;width:200px;" src="<?php echo "../imagesTrader/".$loginTrader->username."/".$photo->filename; ?>" alt="..." id="livePicture"/>
    <?php } ?>

    <br>
    <br>

    <div class="file-field input-field">
    <button type="button" class="btn btn-default waves-effect waves-light" style="height: 50px; width: 70px;  ">
    <span>File</span>
    <input type="hidden" name="MAX_FILE_SIZE" value=<?php echo "'" . $max_file_size . "'"; ?> />
    <input id="imageInput" name="file_upload" type="file" />
    </button>
    </div>

    <br>
    <br>

    <div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100">
    </div>
    </div>

    
    <div id="output">
    </div>

    <br>


    <button type="submit" name="submit" class="btn btn-default">Submit</button>

    </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>	
  <script src="../assets/js/plugins/mdb.js"></script>            
	<script src="../assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js"></script>
	<script src="../assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js"></script>
	<script src="../assets/js/forms/newTraderAccount.js"></script>
	<script>
        jQuery(document).ready(function() {
            newTraderAccount.initNewTraderAccount();
        });
  </script>


	</body>
</html>
